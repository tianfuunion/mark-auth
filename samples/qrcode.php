<?php
declare (strict_types=1);

if (is_file(__DIR__ . '/../autoload.php')) {
    require_once __DIR__ . '/../autoload.php';
}
if (is_file(__DIR__ . '/../vendor/autoload.php')) {
    require_once __DIR__ . '/../vendor/autoload.php';
}

$encode = \mark\auth\tool\QrCode::encode(session_id(), array('text' => '二维码数据测试'));
var_dump($encode);

$decode = \mark\auth\tool\QrCode::decode(session_id(), $encode['url'] ?? '');
var_dump($decode);

echo PHP_EOL;