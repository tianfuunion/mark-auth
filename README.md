# Mark Authorize SDK For PHP

[![Latest Stable Version](https://poser.pugx.org/tianfuunion/mark-auth/v/stable)](https://packagist.org/packages/tianfuunion/mark-auth)
[![Build Status](https://travis-ci.org/tianfuunion/mark-auth.svg?branch=master)](https://travis-ci.org/tianfuunion/mark-auth)
[![Coverage Status](https://coveralls.io/repos/github/tianfuunion/mark-auth/badge.svg?branch=master)](https://coveralls.io/github/tianfuunion/mark-auth?branch=master)

## 概述

{标记权限管理（Mark Authorize）是 天府联盟 对外提供专业的身份认证和授权服务。用户可以通过调用API，在任何应用、任何时间、任何地点上传和下载数据，也可以通过用户Web控制台对数据进行简单的管理。详情请看 [https://auth.tianfu.ink](https://auth.tianfu.ink)}

## 软件架构

软件架构说明

## 运行环境

- PHP 7.4+
- cURL extension

## 安装方法

如果您通过composer管理您的项目依赖，可以在你的项目根目录运行：

        $ composer require tianfuunion/mark-auth

或者在你的`composer.json`中声明对 Mark Auth SDK For PHP 的依赖：

        "require": {
            "tianfuunion/mark-auth": "^2.0"
        }

然后通过`composer install`安装依赖。composer安装完成后，在您的PHP代码中引入依赖即可：

        require_once __DIR__ . '/vendor/autoload.php';

## 使用说明

在 路由设置(route.php)中添加代码：

        'middleware' => [\app\AuthMiddleware::class];

## 版权信息

Mark Authorize SDK For PHP遵循MulanPSL-2.0开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有 南阳市天府网络科技有限公司

Copyright © 2017-2024 by TianFuUnion (tianfuunion.cn) All rights reserved。

TianFuUnion® 商标和著作权所有者为南阳市天府网络科技有限公司。

- [MulanPSL-2.0](http://license.coscl.org.cn/MulanPSL2/)
- 更多细节参阅 [LICENSE.txt](https://gitee.com/tianfuunion/mark-auth/blob/master/LICENSE.txt)

## 联系我们

- [天府联盟官方网站：www.tianfuunion.cn](https://www.tianfuunion.cn)
- [天府联盟授权中心：auth.tianfu.ink](https://auth.tianfu.ink)
- [天府联盟反馈邮箱：report@tianfuunion.cn](mailto:report@tianfuunion.cn)