<?php
declare (strict_types=1);

namespace mark\auth\entity;

/**
 * Class UserInfo
 *
 * @package mark\auth\entity
 */
final class UserInfo {

    /**
     * 性别列表
     *
     * @return array
     */
    public static function sexs(): array {
        return array(0 => array('id' => 0, 'title' => '保密', 'name' => 'ladyboy', 'icon' => 'icon-sexual'),
            1 => array('id' => 1, 'title' => '男', 'name' => 'male', 'icon' => 'icon-male'),
            2 => array('id' => 2, 'title' => '女', 'name' => 'female', 'icon' => 'icon-female'));
    }

    /**
     * 性别信息
     *
     * @param int $sexid
     *
     * @return array
     */
    public static function sex($sexid = -1): array {
        $sexs = self::sexs();

        return $sexs[array_key_exists($sexid, $sexs) ? $sexid : 0];
    }
}