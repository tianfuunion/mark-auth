<?php
declare (strict_types=1);

namespace mark\auth\entity;

/**
 * Class CorporateInfo
 *
 * @package mark\auth\entity
 */
final class CorporateInfo {
    /**
     * 组织规模
     *
     * @var string[]
     */
    public static $scales = array(
        1 => '1-19',
        2 => '20-49',
        3 => '50-99',
        4 => '100-199',
        5 => '200-499',
        6 => '500-999',
        7 => '1000-1999',
        8 => '>2000'
    );

    /**
     * 组织类型
     *
     * @var array[]
     */
    public static $subject_types = array(
        1 => array('id' => 1, 'title' => '个体户', 'name' => 'individual', 'describe' => '营业执照上的主体类型一般为个体户、个体工商户、个体经营'),
        2 => array('id' => 2, 'title' => '企业', 'name' => 'enterprise', 'describe' => '营业执照上的主体类型一般为有限公司、有限责任公司'),
        3 => array('id' => 3, 'title' => '党政、机关及事业单位', 'name' => 'institutions', 'describe' => '包括国内各级、各类政府机构、事业单位等。如：公安、党团、司法、交通、旅游、工商税务、市政、医疗、教育、学校等机构'),
        4 => array('id' => 4, 'title' => '其他组织', 'name' => 'others', 'describe' => '不属于企业、政府/事业单位的组织机构，如社会团体、民办非企业、基金会。要求机构已办理组织机构代码证'));

    /**
     * 企业状态列表
     *
     * @var array[]
     */
    public static $states = array(
        0 => array('id' => 0, 'title' => '未激活', 'name' => 'inactive', 'icon' => 'icon-notice', 'style' => 'btn-info', 'describe' => ''),
        1 => array('id' => 1, 'title' => '未认证', 'name' => 'not_certified', 'icon' => 'icon-question-gray', 'style' => 'btn-default', 'describe' => ''),
        2 => array('id' => 2, 'title' => '申请中', 'name' => 'applying', 'icon' => 'icon-waiting', 'style' => 'btn-secondary', 'describe' => ''),
        3 => array('id' => 3, 'title' => '审核中', 'name' => 'audited', 'icon' => 'icon-info', 'style' => 'btn-warning', 'describe' => ''),
        4 => array('id' => 4, 'title' => '已注销', 'name' => 'cancelled', 'icon' => 'icon-forbidden', 'style' => 'btn-error', 'describe' => ''),
        5 => array('id' => 5, 'title' => '驳回申请', 'name' => 'rejected', 'icon' => 'icon-protect-error', 'style' => 'btn-danger', 'describe' => ''),
        6 => array('id' => 6, 'title' => '认证成功', 'name' => 'successful', 'icon' => 'icon-success', 'style' => 'btn-success', 'describe' => ''),
        7 => array('id' => 7, 'title' => '预览中', 'name' => 'previewed', 'icon' => 'icon-question', 'style' => 'btn-info', 'describe' => ''),
        8 => array('id' => 8, 'title' => '已过期', 'name' => 'expired', 'icon' => 'icon-reduce', 'style' => 'btn-orange', 'describe' => ''),
        9 => array('id' => 9, 'title' => '维护中', 'name' => 'maintained', 'icon' => 'icon-danger', 'style' => 'btn-danger', 'describe' => ''));

    /**
     * 获取企业状态
     *
     * @param $state
     *
     * @return array
     */
    public static function getState($state): array {
        if (array_key_exists($state, self::$states)) {
            return self::$states[$state] ?? array();
        }

        return self::$states[array_key_exists($state, self::$states) ? $state : 11];
    }
}