<?php
declare (strict_types=1);

namespace mark\auth\entity;

/**
 * Class UnionInfo
 *
 * @package mark\auth\entity
 * @method static setNickName(string $nickname)
 * @method static setSex(string $sex)
 */
final class UnionInfo {

    /**
     * @var string $openid 用户的唯一标识
     */
    private $openid;

    /**
     * @var string $unionid 只有在用户将公众号绑定到微信开放平台账号后，才会出现该字段。详见： 获取用户个人信息（UnionID 机制）
     */
    private $unionid;

    /**
     * @var string $corpid 用户所属企业的corpid
     */
    private $corpid;

    /**
     * @var string $scope 授权作用域
     */
    private $scope;

    /**
     * @var string $nickname // 用户昵称
     */
    private $nickname;
    /**
     * @var string $sex 用户的性别，值为 1 时是男性，值为 2 时是女性，值为 0 时是未知
     */
    private $sex;
    /**
     * @var string $province 用户个人资料填写的省份
     */
    private $province;
    /**
     * @var  string $city 普通用户个人资料填写的城市
     */
    private $city;
    /**
     * @var string $country 国家，如中国为 CN
     */
    private $country;
    /**
     * @var string $headimgurl 用户头像，最后一个数值代表正方形头像大小（有 0、46、64、96、132 数值可选，0 代表 640*640 正方形头像），用户没有头像时该项为空。
     * 若用户更换头像，原有头像 URL 将失效。
     */
    private $headimgurl;
    /**
     * @var string $privilege 用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
     */
    private $privilege;

}