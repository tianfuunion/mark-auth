<?php
declare (strict_types=1);

namespace mark\auth\entity;

/**
 * Class AuthInfo
 *
 * @package mark\auth\entity
 */
final class AuthInfo {
    /**
     * @var string
     */
    public static $modifier = 'modifier';

    public static $default = 'default';
    public static $public = 'public';
    public static $proteced = 'proteced';
    public static $platform = 'platform';

    public static $enterprise = 'enterprise';
    /**
     * @deprecated
     * @var string
     */
    public static $corporate = 'corporate';
    public static $organize = 'organize';
    public static $merchant = 'merchant';
    public static $store = 'store';
    /**
     * @deprecated
     * @var string
     */
    public static $employee = 'employee';

    public static $property = 'property';
    public static $project = 'project';
    public static $community = 'community';

    public static $private = 'private';
    public static $final = 'final';
    public static $static = 'static';
    public static $abstract = 'abstract';
    public static $system = 'system';
    public static $disable = 'disable';

    public static $scope = 'scope';

    public static $auth_phone = 'auth_phone';
    public static $auth_email = 'auth_email';

    public static $auth_base = 'auth_base';
    public static $auth_user = 'auth_user';
    public static $auth_union = 'auth_union';
    public static $auth_friend = 'auth_friend';
    public static $auth_corp = 'auth_corp';
    public static $auth_organize = 'auth_organize';
    public static $auth_merchant = 'auth_merchant';
    public static $auth_store = 'auth_store';
    public static $auth_employee = 'auth_employee';
    public static $auth_property = 'auth_property';
    public static $auth_community = 'auth_community';
    public static $auth_project = 'auth_project';
    public static $auth_staff = 'auth_staff';

    /**
     * 授权作用域集
     *
     * @var array
     */
    public static $scopes = array(
        'default' => array('title' => '默认', 'name' => 'default', 'scope' => 'auth_base', 'describe' => '用户访问，只用登录即可'),
        'public' => array('title' => '公开', 'name' => 'public', 'scope' => 'auth_open', 'describe' => '公开匿名访问'),
        'proteced' => array('title' => '保护', 'name' => 'proteced', 'scope' => 'auth_union', 'describe' => '授权用户访问'),

        // 'admin' => array('title' => '管理员', 'name' => 'admin', 'describe' => '超级管理员'),
        // 'testing' => array('title' => '测试员', 'name' => 'testing', 'describe' => '设计，开发，测试，维护'),
        // 'manager' => array('title' => '管理员', 'name' => 'manager', 'describe' => '平台管理员'),

        'platform' => array('title' => '平台', 'name' => 'platform', 'scope' => 'auth_platform', 'describe' => '平台管理'),
        // 'app' => array('title' => '应用', 'name' => 'app', 'describe' => '应用管理员'),

        'enterprise' => array('title' => '企业', 'name' => 'enterprise', 'scope' => 'auth_corp', 'describe' => '企业组织成员访问'),
        'corporate' => array('title' => '公司', 'name' => 'corporate', 'scope' => 'auth_corp', 'describe' => '公司员工访问'),
        'merchant' => array('title' => '商户', 'name' => 'merchant', 'scope' => 'auth_merchant', 'describe' => '商户员工访问'),
        'organize' => array('title' => '组织', 'name' => 'organize', 'scope' => 'auth_organize', 'describe' => '组织成员访问'),
        // 'store' => array('title' => '门店', 'name' => 'store', 'describe' => '门店员工访问'),
        'property' => array('title' => '物业', 'name' => 'property', 'scope' => 'auth_property', 'describe' => '物业成员访问'),
        'community' => array('title' => '小区', 'name' => 'community', 'scope' => 'auth_community', 'describe' => '社区居民访问'),
        'project' => array('title' => '项目', 'name' => 'project', 'scope' => 'auth_project', 'describe' => '项目成员访问'),
        'employee' => array('title' => '员工', 'name' => 'employee', 'scope' => 'auth_employee', 'describe' => '员工访问'),

        'private' => array('title' => '私有', 'name' => 'private', 'scope' => 'auth_employee', 'describe' => '只可访问自身数据'),
        'final' => array('title' => '最终', 'name' => 'final', 'scope' => 'auth_final', 'describe' => ''),
        'static' => array('title' => '静态', 'name' => 'static', 'scope' => 'auth_static', 'describe' => ''),
        'abstract' => array('title' => '抽象', 'name' => 'abstract', 'scope' => 'auth_abstract', 'describe' => ''),
        'system' => array('title' => '系统', 'name' => 'system', 'scope' => 'auth_system', 'describe' => '保证系统服务的正常运行，赋予系统及系统服务的权限；收发通知，计划定时任务执行，机器人'),
        'disable' => array('title' => '禁用', 'name' => 'disable', 'scope' => 'auth_disable', 'describe' => '禁止访问'));

    /**
     * 获取授权作用域
     *
     * @param string $scope
     *
     * @return array
     */
    public static function getScope(string $scope): array {
        if (array_key_exists($scope, self::$scopes)) {
            return self::$scopes[$scope] ?? array();
        }

        foreach (self::$scopes as $key => $item) {
            if (($item['scope'] ?? '') == $scope) {
                return $item;
            }
        }

        return self::$scopes[array_key_exists($scope, self::$scopes) ? $scope : 'default'];
    }

}