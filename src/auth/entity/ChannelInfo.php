<?php
declare (strict_types=1);

namespace mark\auth\entity;

/**
 * Class ChannelInfo
 *
 * @package mark\auth\entity
 */
final class ChannelInfo {

    /**
     * 频道状态
     *
     * @var array[]
     */
    public static $states = array(
        0 => array('id' => 0, 'title' => '初始状态', 'name' => 'invalid', 'style' => 'label-default', 'describe' => '频道初始创建状态'),
        1 => array('id' => 1, 'title' => '待提交', 'name' => 'initial', 'style' => 'label-primary', 'describe' => '创建频道时，在完善信息后保存，该频道便处于待提交的状态。'),
        2 => array('id' => 2, 'title' => '审核中', 'name' => 'handle', 'style' => 'label-secondary', 'describe' => '提交测试之后，频道就会处于该状态等待审核。'),
        3 => array('id' => 3, 'title' => '测试中', 'name' => 'testing', 'style' => 'label-orange', 'describe' => '频道已被授受测试'),
        4 => array('id' => 4, 'title' => '已删除', 'name' => 'disable', 'style' => 'label-danger', 'describe' => '频道已被删除'),
        5 => array('id' => 5, 'title' => '已驳回', 'name' => 'failure', 'style' => 'label-danger', 'describe' => '频道未通过测试，请查看未通过原因并修改后进行重新提交'),
        6 => array('id' => 6, 'title' => '已发布', 'name' => 'release', 'style' => 'label-success', 'describe' => '频道已上线'),
        7 => array('id' => 7, 'title' => '已过时', 'name' => 'deprecated', 'style' => 'label-secondary', 'describe' => '频道已过时，该频道将逐渐被新的所替代'),
        // array('id' => 7, 'title' => '已冻结', 'name' => 'freezing', 'describe' => '频道冻结后，该频道将无法展现，直至冻结期限结束'),
        8 => array('id' => 8, 'title' => '已下架', 'name' => 'processed', 'style' => 'label-warning', 'describe' => '频道已下架'),
        9 => array('id' => 9, 'title' => '维护中', 'name' => 'maintain', 'style' => 'label-warning', 'describe' => '系统维护中'),
        10 => array('id' => 10, 'title' => '异常状态', 'name' => 'maintain', 'style' => 'label-danger', 'describe' => '异常状态'));

    /**
     * 获取频道状态
     *
     * @param $state
     *
     * @return array
     */
    public static function getState($state): array {
        if (array_key_exists($state, self::$states)) {
            return self::$states[$state] ?? array();
        }

        return self::$states[array_key_exists($state, self::$states) ? $state : 10];
    }

}