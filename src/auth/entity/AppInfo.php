<?php
declare (strict_types=1);

namespace mark\auth\entity;

/**
 * Class AppInfo
 *
 * @package mark\auth\entity
 */
final class AppInfo {

    /**
     * 应用类型
     *
     * @var array[]
     */
    public static $types = [
        0 => ['id' => 0, 'title' => '网页 App', 'name' => 'WebApp'],
        1 => ['id' => 1, 'title' => '本地 App', 'name' => 'NativeApp'],
        2 => ['id' => 2, 'title' => '混合 App', 'name' => 'HybridApp'],
        3 => ['id' => 3, 'title' => 'IOS App', 'name' => 'IosApp'],
        4 => ['id' => 4, 'title' => 'Android App', 'name' => 'AndroidApp'],
    ];

    /**
     * 系统运行模式
     *
     * @var \string[][]
     */
    public static $patterns = [
        'develop' => ['title' => '开发版', 'name' => 'Develop', 'describe' => '基于Chromium开发，每周出新功能，并且这些功能还有一定的筛选，另外还修复了一些Bug和不稳定因素'],
        'testing' => ['title' => '测试版', 'name' => 'Testing', 'describe' => ''],
        'alpha' => ['title' => 'Alpha版', 'name' => 'Alpha', 'describe' => '软件或系统的内部测试版本，会有很多Bug，仅内部人员使用'],
        'beta' => ['title' => 'Beta版', 'name' => 'Beta', 'describe' => '系统的公开测试版本，基于Alpha版进行改进，一般按月更新，功能更加完善'],
        'gamma' => ['title' => 'Gamma版', 'name' => 'Gamma', 'describe' => '软件或系统接近于成熟的版本，只需要做一些小的改进就能发行'],

        'demo' => ['title' => '演示版', 'name' => 'Demo', 'describe' => '仅仅集成了正式版中的几个功能，不能升级成正式版'],
        'preview' => ['title' => '预览版', 'name' => 'Preview', 'describe' => '软件作者为了满足那些对新版本很关注的人，发布的可以看到大部分功能的测试软件'],
        'trial' => ['title' => '试用版', 'name' => 'Trial', 'describe' => '通常都有时间限制，有些试用版软件还在功能上做了一定的限制'],

        'feature' => ['title' => '抢先版', 'name' => 'Feature', 'describe' => '开发代号为Vulcan的新功能，该功能计划用于下一代星际飞船。'],
        'express' => ['title' => '特别版', 'name' => 'Express', 'describe' => '指创立一个新生的事物后,在该基础上进行修改或加强，使其和原来的有所差异'],

        'release' => ['title' => '发行版', 'name' => 'Release Candidate', 'describe' => '正式发布版本，这一版本不会增加新功能，多要进行Debug'],
        'hotfix' => ['title' => '修正版', 'name' => 'Hotfix', 'describe' => '修正了正式版推出后发现的Bug'],

        'rtm' => ['title' => '压片版', 'name' => 'RTM', 'describe' => '（Release to Manufacture）：给工厂大量生产的压片版本，与正式版内容一样'],
        'oem' => ['title' => '预装版', 'name' => 'OEM', 'describe' => '（Original Entrusted Manufacture）：给计算机厂商的出场销售版本，不零售只预装'],
        'eval' => ['title' => '评估版', 'name' => 'EVAL', 'describe' => '而流通在网络上的EVAL版，与“评估版”类似，功能上和零售版没有区别'],
        'rtl' => ['title' => '零售版', 'name' => 'RTL', 'describe' => 'RTL（Retail）：零售版是真正的正式版，正式上架零售版'],

        'stable' => ['title' => '稳定版', 'name' => 'Stable', 'describe' => '也就是Chrome的正式版本，这一版本基于Beta版，已知Bug都被修复，一般情况下，更新比较慢'],
        'lts' => ['title' => 'LTS版', 'name' => 'Long Term Support', 'describe' => '长期演进版，系统会对这一版本的支持时间更长。目前Java也在运用这种方式'],
        'deprecated' => ['title' => '已弃用', 'name' => 'Deprecated', 'describe' => '已弃用的历史版本，来告知用户那些不再被使用的元素']
    ];

    /**
     * 应用状态列表
     *
     * @var array[]
     */
    public static $states = array(
        0 => array('id' => 0, 'title' => '未激活', 'name' => 'invalid', 'icon' => 'icon-notice', 'style' => 'label-default', 'describe' => ''),
        1 => array('id' => 1, 'title' => '未认证', 'name' => 'not_certified', 'icon' => 'icon-question-gray', 'style' => 'label-secondary',
                   'describe' => '创建应用时，在完善信息后保存，该应用便处于待提交的状态'),
        2 => array('id' => 2, 'title' => '申请中', 'name' => 'applying', 'icon' => 'icon-waiting', 'style' => 'label-primary', 'describe' => '提交测试之后，应用就会处于该状态等待审核。'),
        3 => array('id' => 3, 'title' => '审核中', 'name' => 'audited', 'icon' => 'icon-info', 'style' => 'label-orange', 'describe' => '应用审核测试中'),
        4 => array('id' => 4, 'title' => '已禁用', 'name' => 'disable', 'icon' => 'icon-forbidden', 'style' => 'label-danger', 'describe' => '应用已被禁用'),
        5 => array('id' => 5, 'title' => '驳回申请', 'name' => 'rejected', 'icon' => 'icon-protect-error', 'style' => 'label-danger',
                   'describe' => '请在该列表的操作项中查看未通过测试的原因或进行重新提交，重新提交前请参看未通过测试的原因进行修改'),
        6 => array('id' => 6, 'title' => '认证成功', 'name' => 'release', 'icon' => 'icon-success', 'style' => 'label-success', 'describe' => '应用已发布、上线'),
        7 => array('id' => 7, 'title' => '已冻结', 'name' => 'freezing', 'icon' => 'icon-question', 'style' => 'label-secondary',
                   'describe' => '应用冻结后，该应用将无法更新、上传，也无法在商店中展现、搜索或下载，直至冻结期限结束'),
        8 => array('id' => 8, 'title' => '已下架', 'name' => 'processed', 'icon' => 'icon-reduce', 'style' => 'label-warning', 'describe' => '应用已下架'),
        9 => array('id' => 9, 'title' => '维护中', 'name' => 'maintain', 'icon' => 'icon-warning', 'style' => 'label-warning', 'describe' => '系统维护中'),
        10 => array('id' => 10, 'title' => '异常状态', 'name' => 'maintain', 'style' => 'label-danger', 'describe' => '异常状态')
    );

    /**
     * 获取App状态
     *
     * @param $state
     *
     * @return array
     */
    public static function getState($state): array {
        if (array_key_exists($state, self::$states)) {
            return self::$states[$state] ?? array();
        }

        return self::$states[array_key_exists($state, self::$states) ? $state : 10];
    }

}