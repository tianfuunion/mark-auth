<?php
declare (strict_types=1);

namespace mark\auth\model;

use mark\auth\Authorize;
use mark\http\Client\Client;
use mark\response\Response;

/**
 * Class ChannelModel
 *
 * @package mark\auth\model
 */
class ChannelModel {

    /**
     * 根据标识符获取频道信息
     *
     * @param string $appid
     * @param string $identifier
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function getIdentifier(string $appid, string $identifier, $cache = true): Response {
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($identifier)) {
            return Response::create('', 412, '', '无效的频道标示符');
        }

        $cacheKey = 'auth:sdk:channel:identifier:appid:' . $appid . ':identifier:' . $identifier;

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->appendData('identifier', urlencode($identifier))
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/channel/identifier', 'json')
                     ->api_decode();
    }

    /**
     * 根据标识符，获取权限信息
     *
     * @param string $appid
     * @param int    $channelid
     * @param int    $roleid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function getAccess(string $appid, int $channelid, int $roleid = 404, $cache = true): Response {
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($channelid)) {
            return Response::create('', 412, '', '无效的频道ID');
        }

        $cacheKey = 'auth:sdk:access:appid:' . $appid . ':channelid:' . $channelid . ':roleid:' . $roleid;

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->appendData('channelid', $channelid)
                     ->appendData('roleid', $roleid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/access/info', 'json')
                     ->api_decode();
    }

    /**
     * 校验是否拥有权限
     *
     * @param string $appid      APPID
     * @param string $identifier 频道标识符
     * @param string $permission 权限标识符
     * @param int    $roleid     角色ID
     * @param bool   $cache      是否缓存
     *
     * @return bool
     */
    public static function hasPermission(string $appid, $identifier = '', $permission = 'button', $roleid = 404, $cache = true): bool {
        if (empty($appid)) {
            return false;
        }
        if (empty($identifier)) {
            $identifier = Authorize::getInstance()->getIdentifier();
        }
        if (empty($roleid)) {
            $roleid = 404;
        }

        $identifier = str_replace('-', ':', $identifier);
        $haystack = array('titlebar', 'navbar', 'menubar', 'sidebar', 'workspace', 'button', 'footbar', 'allow', 'method', 'status');
        if (!in_array($permission, $haystack)) {
            return false;
        }

        $cacheKey = 'auth:sdk:permission' . ':appid:' . $appid . ':roleid:' . $roleid . ':identifier:' . $identifier;

        $response = Client::getInstance()
                          ->appendData('appid', $appid)
                          ->appendData('identifier', urlencode($identifier))
                          ->appendData('roleid', $roleid)
                          ->appendData('status', 1)
                          ->addHeader('pattern', Authorize::getInstance()->getPattern())
                          ->addHeader('auth-type', Authorize::$_type)
                          ->addHeader('auth-version', Authorize::$_version)
                          ->addHeader('cache', $cache)
                          ->setResponseHeader(true)
                          ->setExpire(Authorize::getInstance()->getExpire())
                          ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                          ->setCacheKey($cacheKey)
                          ->setCache($cache)
                          ->setCallback(function (Response $response, Client $client) {
                              if ($response->api_decode()->getResponseCode() != 200) {
                                  $client->setCache(false);
                                  $client->clearCache();
                              }

                              return $response;
                          })
                          ->get(Authorize::getInstance()->getHost() . '/api.php/access/info', 'json')
                          ->api_decode();

        if ($response->getResponseCode() == 200 && !$response->isEmpty()) {
            $access = $response->toArray();
            return ($access['status'] ?? '') == 1 && isset($access[$permission]) && !empty($access[$permission] ?? '') && $access[$permission] == 1;
        }

        return false;
    }

    /**
     * 获取桌面频道列表
     *
     * @param string $appid
     * @param string $poolid
     * @param int    $roleid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function getWorkspace(string $appid, string $poolid, int $roleid = 404, $cache = true): Response {
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($poolid)) {
            return Response::create('', 412, '', '无效的PoolID');
        }

        $cacheKey = 'auth:sdk:channel:workspace:appid:' . $appid . ':poolid:' . $poolid . ':roleid:' . $roleid;

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->appendData('poolid', $poolid)
                     ->appendData('roleid', $roleid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/channel/workspace', 'json')
                     ->api_decode();
    }

    /**
     * 获取标题栏
     *
     * @param string $appid
     * @param string $poolid
     * @param int    $roleid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function getTitleBar(string $appid, string $poolid, int $roleid = 404, $cache = true): Response {
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($poolid)) {
            return Response::create('', 412, '', '无效的PoolID');
        }

        $cacheKey = 'auth:sdk:channel:titlebar:appid:' . $appid . ':poolid:' . $poolid . ':roleid:' . $roleid;

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->appendData('poolid', $poolid)
                     ->appendData('roleid', $roleid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/channel/titlebar', 'json')
                     ->api_decode();
    }

    /**
     * 获取导航栏
     *
     * @param string $appid
     * @param string $poolid
     * @param int    $roleid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function getNavBar(string $appid, string $poolid, int $roleid = 404, $cache = true): Response {
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($poolid)) {
            return Response::create('', 412, '', '无效的PoolID');
        }

        $cacheKey = 'auth:sdk:channel:navbar:appid:' . $appid . ':poolid:' . $poolid . ':roleid:' . $roleid;

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->appendData('poolid', $poolid)
                     ->appendData('roleid', $roleid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/channel/navbar', 'json')
                     ->api_decode();
    }

    /**
     * 获取菜单栏
     *
     * @param string $appid
     * @param string $poolid
     * @param int    $roleid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function getMenuBar(string $appid, string $poolid, int $roleid = 404, $cache = true): Response {
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($poolid)) {
            return Response::create('', 412, '', '无效的PoolID');
        }

        $cacheKey = 'auth:sdk:channel:menubar:appid:' . $appid . ':poolid:' . $poolid . ':roleid:' . $roleid;

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->appendData('poolid', $poolid)
                     ->appendData('roleid', $roleid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/channel/menubar', 'json')
                     ->api_decode();
    }

    /**
     * 获取侧边栏
     *
     * @param string $appid
     * @param string $poolid
     * @param int    $roleid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function getSideBar(string $appid, string $poolid, int $roleid = 404, $cache = true): Response {
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($poolid)) {
            return Response::create('', 412, '', '无效的PoolID');
        }

        $cacheKey = 'auth:sdk:channel:sidebar:appid:' . $appid . ':poolid:' . $poolid . ':roleid:' . $roleid;

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->appendData('poolid', $poolid)
                     ->appendData('roleid', $roleid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/channel/sidebar', 'json')
                     ->api_decode();
    }

    /**
     * 获取底部栏
     *
     * @param string $appid
     * @param string $poolid
     * @param int    $roleid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function getFootBar(string $appid, string $poolid, int $roleid = 404, $cache = true): Response {
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($poolid)) {
            return Response::create('', 412, '', '无效的PoolID');
        }

        $cacheKey = 'auth:sdk:channel:footbar:appid:' . $appid . ':poolid:' . $poolid . ':roleid:' . $roleid;

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->appendData('poolid', $poolid)
                     ->appendData('roleid', $roleid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/channel/footbar', 'json')
                     ->api_decode();
    }

    /**
     * 获取标签栏
     *
     * @param string $appid
     * @param string $poolid
     * @param int    $roleid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function getTabBar(string $appid, string $poolid, int $roleid = 404, $cache = true): Response {
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($poolid)) {
            return Response::create('', 412, '', '无效的PoolID');
        }

        $cacheKey = 'auth:sdk:channel:tabbar:appid:' . $appid . ':poolid:' . $poolid . ':roleid:' . $roleid;

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->appendData('poolid', $poolid)
                     ->appendData('roleid', $roleid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/channel/tabbar', 'json')
                     ->api_decode();
    }
}