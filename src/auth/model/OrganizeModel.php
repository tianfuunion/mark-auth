<?php
declare (strict_types=1);

namespace mark\auth\model;

use mark\auth\Authorize;
use mark\http\Client\Client;
use mark\response\Response;

/**
 * Class OrganizeModel
 *
 * @package mark\auth\model
 */
final class OrganizeModel {

    /**
     * 获取组织详情
     *
     * @param string $access_token
     * @param string $corpid
     * @param string $orgid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function info(string $access_token, string $corpid, string $orgid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($corpid)) {
            return Response::create('', 412, '', '无效的CorpID');
        }
        if (empty($orgid)) {
            return Response::create('', 412, '', '无效的OrgID');
        }

        $cacheKey = 'auth:sdk:organize:info:corpid:' . $corpid . ':orgid:' . $orgid;

        return Client::getInstance()
                     ->appendData('corpid', $corpid)
                     ->appendData('orgid', $orgid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/organize/info?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 列表组织架构
     *
     * @param string $access_token
     * @param string $orgid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function list(string $access_token, string $orgid = '', $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($orgid)) {
            return Response::create('', 412, '', '无效的OrgID');
        }

        $cacheKey = 'auth:sdk:organize:list:orgid:' . $orgid;

        return Client::getInstance()
                     ->appendData('orgid', $orgid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/organize/list?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 树形组织架构
     *
     * @param string $access_token
     * @param string $orgid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function tree(string $access_token, string $orgid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($orgid)) {
            return Response::create('', 412, '', '无效的OrgID');
        }

        $cacheKey = 'auth:sdk:organize:tree:orgid:' . $orgid;

        return Client::getInstance()
                     ->appendData('orgid', $orgid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/organize/tree?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 添加角色信息
     *
     * @param string $access_token
     * @param array  $organize
     *
     * @return \mark\response\Response
     */
    public static function create(string $access_token, array $organize): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($organize)) {
            return Response::create('', 412, '', '无效的组织信息');
        }

        return Client::getInstance()
                     ->append($organize)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/organize/create?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 更新角色信息
     *
     * @param string $access_token
     * @param array  $organize
     *
     * @return \mark\response\Response
     */
    public static function update(string $access_token, array $organize): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($organize)) {
            return Response::create('', 412, '', '无效的组织信息');
        }

        return Client::getInstance()
                     ->append($organize)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/organize/update?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 删除组织节点
     *
     * @param string $access_token
     * @param string $orgid
     *
     * @return \mark\response\Response
     */
    public static function delete(string $access_token, string $orgid): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($orgid)) {
            return Response::create('', 412, '', '无效的OrgID');
        }

        return Client::getInstance()
                     ->appendData('orgid', $orgid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/organize/delete?access_token=' . $access_token, 'json')
                     ->api_decode();
    }
}