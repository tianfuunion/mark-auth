<?php
declare (strict_types=1);

namespace mark\auth\model;

use mark\auth\Authorize;
use mark\http\Client\Client;
use mark\response\Response;

/**
 * Class UnionModel
 *
 * @package mark\auth\model
 */
final class UnionModel {

    /**
     * 获取授权详情(UnionID机制)
     *
     * @param string $access_token
     * @param string $openid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function info(string $access_token, string $openid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '无效的OpenID');
        }
        $cacheKey = 'auth:sdk:union:info:openid:' . $openid;

        return Client::getInstance()
                     ->appendData('openid', $openid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/union/info?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 查询用户授权信息
     *
     * @param string $access_token
     * @param array  $param
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function query(string $access_token, array $param, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }

        $cacheKey = 'auth:sdk:union:query';
        $client = Client::getInstance();

        if (!empty($param['userid'] ?? '')) {
            $cacheKey .= ':userid:' . $param['userid'];
            $client->appendData('userid', $param['userid'] ?? '');
        } elseif (!empty($param['openid'] ?? '')) {
            $cacheKey .= ':openid:' . $param['openid'];
            $client->appendData('openid', $param['openid'] ?? '');
        } elseif (!empty($param['unionid'] ?? '')) {
            $cacheKey .= ':unionid:' . $param['unionid'];
            $client->appendData('unionid', $param['unionid'] ?? '');
        } else {
            return Response::create('', 412, '', '无效的OpenID');
        }

        if (!empty($param['appid'] ?? '')) {
            $cacheKey .= ':appid:' . $param['appid'];
            $client->appendData('appid', $param['appid'] ?? '');
        }

        if (!empty($param['corpid'] ?? '')) {
            $cacheKey .= ':corpid:' . $param['corpid'];
            $client->appendData('corpid', $param['corpid'] ?? '');
        }
        if (!empty($param['projectid'] ?? '')) {
            $cacheKey .= ':projectid:' . $param['projectid'];
            $client->appendData('projectid', $param['projectid'] ?? '');
        }
        if (!empty($param['roleid'] ?? '')) {
            $cacheKey .= ':roleid:' . $param['roleid'];
            $client->appendData('roleid', $param['roleid'] ?? '');
        }
        if (!empty($param['custom'] ?? '')) {
            $cacheKey .= ':custom:' . $param['custom'];
            $client->appendData('custom', $param['custom'] ?? '');
        }
        if (!empty($param['scope'] ?? '')) {
            $cacheKey .= ':scope:' . $param['scope'];
            $client->appendData('scope', $param['scope'] ?? '');
        }

        return $client->addHeader('pattern', Authorize::getInstance()->getPattern())
                      ->addHeader('auth-type', Authorize::$_type)
                      ->addHeader('auth-version', Authorize::$_version)
                      ->addHeader('cache', $cache)
                      ->setResponseHeader(true)
                      ->setExpire(Authorize::getInstance()->getExpire())
                      ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                      ->setCacheKey($cacheKey)
                      ->setCache($cache)
                      ->setCallback(function (Response $response, Client $client) {
                          if ($response->api_decode()->getResponseCode() != 200) {
                              $client->setCache(false);
                              $client->clearCache();
                          }

                          return $response;
                      })
                      ->get(Authorize::getInstance()->getHost() . '/api.php/union/query?access_token=' . $access_token, 'json')
                      ->api_decode();
    }

    /**
     * 获取授权列表
     *
     * @param string $access_token
     * @param string $appid
     * @param string $corpid
     * @param string $projectid
     * @param string $roleid
     * @param string $scope
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function list(string $access_token, string $appid, $corpid = '', $projectid = '', $roleid = '', $scope = 'auth_union', $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        $cacheKey = 'auth:sdk:union:list:appid:' . $appid;
        $client = Client::getInstance()
                        ->appendData('appid', $appid);

        if (!empty($corpid)) {
            $client->appendData('corpid', $corpid);
            $cacheKey .= ':corpid:' . $corpid;
        }
        if (!empty($projectid)) {
            $client->appendData('projectid', $projectid);
            $cacheKey .= ':projectid:' . $projectid;
        }
        if (!empty($roleid)) {
            $client->appendData('roleid', $roleid);
            $cacheKey .= ':roleid:' . $roleid;
        }
        $cacheKey .= ':scope:' . $scope;

        return $client->appendData('scope', $scope)
                      ->addHeader('pattern', Authorize::getInstance()->getPattern())
                      ->addHeader('auth-type', Authorize::$_type)
                      ->addHeader('auth-version', Authorize::$_version)
                      ->addHeader('cache', $cache)
                      ->setResponseHeader(true)
                      ->setExpire(Authorize::getInstance()->getExpire())
                      ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                      ->setCacheKey($cacheKey)
                      ->setCache($cache)
                      ->setCallback(function (Response $response, Client $client) {
                          if ($response->api_decode()->getResponseCode() != 200) {
                              $client->setCache(false);
                              $client->clearCache();
                          }

                          return $response;
                      })
                      ->get(Authorize::getInstance()->getHost() . '/api.php/union/list?access_token=' . $access_token, 'json')
                      ->api_decode();
    }

    /**
     * 预创建授权 * 邀请好友授权
     *
     * @param string $access_token
     * @param array  $union
     *
     * @return \mark\response\Response
     */
    public static function preCreate(string $access_token, array $union): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($union)) {
            return Response::create('', 412, '', '无效的授权信息');
        }

        return Client::getInstance()
                     ->append($union)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/union/precreate?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 添加授权信息
     *
     * @param string $access_token
     * @param array  $union
     *
     * @return \mark\response\Response
     */
    public static function create(string $access_token, array $union = array()): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($union)) {
            return Response::create('', 412, '', '无效的授权信息');
        }

        return Client::getInstance()
                     ->append($union)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/union/create?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 更新授权信息
     *
     * @param string $access_token
     * @param array  $union
     *
     * @return \mark\response\Response
     */
    public static function update(string $access_token, array $union): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($union)) {
            return Response::create('', 412, '', '无效的授权信息');
        }

        return Client::getInstance()
                     ->append($union)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/union/update?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 删除授权信息
     *
     * @param string $access_token
     * @param string $appid
     * @param string $openid
     *
     * @return \mark\response\Response
     */
    public static function delete(string $access_token, string $appid, string $openid): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        if (empty($openid)) {
            return Response::create('', 412, '', '无效的OpenID');
        }

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->appendData('openid', $openid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/union/delete?access_token=' . $access_token, 'json')
                     ->api_decode();
    }
}