<?php
declare (strict_types=1);

namespace mark\auth\model;

use mark\auth\Authorize;
use mark\http\Client\Client;
use mark\response\Response;
use mark\system\Os;

/**
 * Class AppModel
 *
 * @package mark\auth\model
 */
final class AppModel {

    /**
     * 获取应用信息
     *
     * @param string $access_token
     * @param string $appid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function info(string $access_token, string $appid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        $cacheKey = 'auth:sdk:app:info:appid:' . $appid;

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/app/info?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 获取应用列表
     *
     * @param string $access_token
     * @param string $poolid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function list(string $access_token, string $poolid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($poolid)) {
            return Response::create('', 412, '', '无效的PoolID');
        }

        $cacheKey = 'auth:sdk:app:list:poolid:' . $poolid;

        return Client::getInstance()
                     ->appendData('poolid', $poolid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/app/list?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 创建应用
     *
     * @param string $access_token
     * @param array  $app
     *
     * @return \mark\response\Response
     */
    public static function create(string $access_token, array $app): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($app)) {
            return Response::create('', 412, '', '无效的AppInfo');
        }

        return Client::getInstance()
                     ->append($app)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/app/create?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 更新应用
     *
     * @param string $access_token
     * @param array  $app
     *
     * @return \mark\response\Response
     */
    public static function update(string $access_token, array $app): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($app)) {
            return Response::create('', 412, '', '无效的AppInfo');
        }

        return Client::getInstance()
                     ->append($app)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/app/update?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 删除应用
     *
     * @param string $access_token
     * @param string $appid
     *
     * @return \mark\response\Response
     */
    public static function delete(string $access_token, string $appid): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/app/delete?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 应用异常反馈
     *
     * @param string $access_token
     * @param array  $param
     *
     * @return \mark\response\Response
     */
    public static function feedback(string $access_token, array $param): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($param['appid'] ?? '')) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        if (empty($param['method'] ?? '')) {
            $param['method'] = $_SERVER['REQUEST_METHOD'] ?? '';
        }
        if (empty($param['referer'] ?? '')) {
            $param['referer'] = $_SERVER['HTTP_REFERER'] ?? '';
        }
        if (empty($param['accept'] ?? '')) {
            $param['accept'] = $_SERVER['HTTP_ACCEPT'] ?? '';
        }
        if (empty($param['user_agent'] ?? '')) {
            $param['user_agent'] = $_SERVER['HTTP_USER_AGENT'] ?? Os::getAgent(true);
        }
        if (empty($param['host'] ?? '')) {
            $param['host'] = $_SERVER['HTTP_HOST'] ?? '';
        }
        if (empty($param['url'] ?? '')) {
            $param['url'] = ($_SERVER['REQUEST_SCHEME'] ?? 'http') . '://' . ($_SERVER['HTTP_HOST'] ?? '') . ($_SERVER['REQUEST_URI'] ?? '');
        }
        if (empty($param['ipv4'] ?? '')) {
            $param['ipv4'] = Os::getIPV4();
        }
        if (empty($param['time'] ?? '')) {
            $param['time'] = time();
        }

        return Client::getInstance()
                     ->append($param)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/app/feedback?access_token=' . $access_token, 'json')
                     ->api_decode();
    }
}