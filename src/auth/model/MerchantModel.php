<?php
declare (strict_types=1);

namespace mark\auth\model;

use mark\auth\Authorize;
use mark\http\Client\Client;
use mark\response\Response;

/**
 * Class MerchantModel
 *
 * @package mark\auth\model
 */
final class MerchantModel {

    /**
     * 查询商户信息
     *
     * @param string $access_token
     * @param string $mchid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function info(string $access_token, string $mchid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($mchid)) {
            return Response::create('', 412, '', '无效的mchid');
        }

        $cacheKey = 'auth:sdk:merchant:info:mchid:' . $mchid;

        return Client::getInstance()
                     ->appendData('mchid', $mchid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/merchant/info?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 查询商户信息
     *
     * @param string $access_token
     * @param string $corpid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function list(string $access_token, string $corpid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($corpid)) {
            return Response::create('', 412, '', '无效的CorpID');
        }

        $cacheKey = 'auth:sdk:merchant:list:corpid:' . $corpid;

        return Client::getInstance()
                     ->appendData('corpid', $corpid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/merchant/list?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 创建商户
     *
     * @param string $access_token
     * @param array  $merchant
     *
     * @return \mark\response\Response
     */
    public static function create(string $access_token, array $merchant): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($merchant)) {
            return Response::create('', 412, '', '无效的merchant Info');
        }

        return Client::getInstance()
                     ->append($merchant)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/merchant/create?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 关闭商户
     *
     * @param string $access_token
     * @param string $mchid
     *
     * @return \mark\response\Response
     */
    public static function close(string $access_token, string $mchid): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($mchid)) {
            return Response::create('', 412, '', '无效的mchid');
        }

        return Client::getInstance()
                     ->appendData('mchid', $mchid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/merchant/close?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 更新商户信息
     *
     * @param string $access_token
     * @param array  $merchant
     *
     * @return \mark\response\Response
     */
    public static function update(string $access_token, $merchant = array()): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($merchant)) {
            return Response::create('', 412, '', '无效的merchant info');
        }

        return Client::getInstance()
                     ->append($merchant)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/merchant/update?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 删除商户
     *
     * @param string $access_token
     * @param string $mchid
     *
     * @return \mark\response\Response
     */
    public static function delete(string $access_token, string $mchid): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($mchid)) {
            return Response::create('', 412, '', '无效的mchid');
        }

        return Client::getInstance()
                     ->appendData('mchid', $mchid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/merchant/delete?access_token=' . $access_token, 'json')
                     ->api_decode();
    }
}