<?php
declare (strict_types=1);

namespace mark\auth\model;

use mark\auth\Authorize;
use mark\http\Client\Client;
use mark\response\Response;

/**
 * Class CorporateModel
 *
 * @package mark\auth\model
 */
final class CorporateModel {

    /**
     * 获取企业详情
     *
     * @param string $access_token
     * @param string $corpid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function info(string $access_token, string $corpid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($corpid)) {
            return Response::create('', 412, '', '无效的CorpID');
        }

        $cacheKey = 'auth:sdk:corporate:info:corpid:' . $corpid;

        return Client::getInstance()
                     ->appendData('corpid', $corpid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/corporate/info?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 获取团队列表
     *
     * @param string $access_token
     * @param string $corpid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function list(string $access_token, string $corpid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($corpid)) {
            return Response::create('', 412, '', '无效的CorpID');
        }

        $cacheKey = 'auth:sdk:corporate:list:corpid:' . $corpid;

        return Client::getInstance()
                     ->appendData('corpid', $corpid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/corporate/list?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 获取树形团队
     *
     * @param string $access_token
     * @param string $corpid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function tree(string $access_token, string $corpid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($corpid)) {
            return Response::create('', 412, '', '无效的CorpID');
        }

        $cacheKey = 'auth:sdk:corporate:tree:corpid:' . $corpid;

        return Client::getInstance()
                     ->appendData('corpid', $corpid)
                     ->appendData('view', 'tree')
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/corporate/list?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 获取企业营业执照
     *
     * @param string $access_token
     * @param string $corpid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function license(string $access_token, string $corpid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($corpid)) {
            return Response::create('', 412, '', '无效的CorpID');
        }

        $cacheKey = 'auth:sdk:corporate:license:corpid:' . $corpid;

        return Client::getInstance()
                     ->appendData('corpid', $corpid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/license/info?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 获取企业法人
     *
     * @param string $access_token
     * @param string $corpid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function legal(string $access_token, string $corpid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($corpid)) {
            return Response::create('', 412, '', '无效的CorpID');
        }

        $cacheKey = 'auth:sdk:corporate:legal:corpid:' . $corpid;

        return Client::getInstance()
                     ->appendData('corpid', $corpid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/legal/info?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 获取企业管理员
     *
     * @param string $access_token
     * @param string $corpid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function manager(string $access_token, string $corpid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($corpid)) {
            return Response::create('', 412, '', '无效的CorpID');
        }

        $cacheKey = 'auth:sdk:corporate:manager:corpid:' . $corpid;

        return Client::getInstance()
                     ->appendData('corpid', $corpid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/corporate/manager?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 创建企业
     *
     * @param string $access_token
     * @param array  $corporate
     *
     * @return \mark\response\Response
     */
    public static function create(string $access_token, array $corporate): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($corporate)) {
            return Response::create('', 412, '', '无效的Corporate信息');
        }

        return Client::getInstance()
                     ->append($corporate)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/corporate/create?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 更新企业
     *
     * @param string $access_token
     * @param array  $corporate
     *
     * @return \mark\response\Response
     */
    public static function update(string $access_token, array $corporate): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }

        if (empty($corporate)) {
            return Response::create('', 412, '', '无效的Corporate信息');
        }

        return Client::getInstance()
                     ->append($corporate)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/corporate/update?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 删除企业
     *
     * @param string $access_token
     * @param string $corpid
     *
     * @return \mark\response\Response
     */
    public static function delete(string $access_token, string $corpid): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }

        if (empty($corpid)) {
            return Response::create('', 412, '', '无效的CorpID');
        }

        return Client::getInstance()
                     ->appendData('corpid', $corpid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/corporate/delete?access_token=' . $access_token, 'json')
                     ->api_decode();
    }
}