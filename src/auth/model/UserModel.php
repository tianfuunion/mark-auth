<?php
declare (strict_types=1);

namespace mark\auth\model;

use mark\auth\Authorize;
use mark\http\Client\Client;
use mark\response\Response;

/**
 * Class UserModel
 *
 * @package mark\auth\model
 */
final class UserModel {

    /**
     * 获取手机号
     *
     * @param string $access_token
     * @param string $openid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function phone(string $access_token, string $openid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '无效的OpenID');
        }

        $cacheKey = 'auth:sdk:user:phone:openid:' . $openid;

        return Client::getInstance()
                     ->appendData('openid', $openid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/mobile/phone?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 获取邮箱地址
     *
     * @param string $access_token
     * @param string $openid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function email(string $access_token, string $openid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        $cacheKey = 'auth:sdk:user:email:openid:' . $openid;

        return Client::getInstance()
                     ->appendData('openid', $openid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/email/address?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 获取用户绑定的微信授权信息
     *
     * @param string $access_token
     * @param string $openid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function wechat(string $access_token, string $openid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '无效的OpenID');
        }

        $cacheKey = 'auth:sdk:wechat:info:openid:' . $openid;

        return Client::getInstance()
                     ->appendData('openid', $openid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/wechat/info?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 获取用户绑定的微信基本信息(UnionID机制)
     *
     * @param string $access_token
     * @param string $openid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function getWeChatUnion(string $access_token, string $openid, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '无效的OpenID');
        }

        $cacheKey = 'auth:sdk:wechat:union:openid:' . $openid;

        return Client::getInstance()
                     ->appendData('openid', $openid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/wechat/union?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 获取用户基本信息
     *
     * @param string $access_token
     * @param array  $param
     * @param string $lang
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function info(string $access_token, array $param, $lang = 'zh-cn', $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }

        $cacheKey = 'auth:sdk:user:info';

        $client = Client::getInstance();
        if (!empty($param['userid'] ?? '')) {
            $cacheKey .= ':userid:' . $param['userid'];
            $client->appendData('userid', $param['userid'] ?? '');
        } elseif (!empty($param['openid'] ?? '')) {
            $cacheKey .= ':openid:' . $param['openid'];
            $client->appendData('openid', $param['openid'] ?? '');
        } elseif (!empty($param['phone'] ?? '')) {
            $cacheKey .= ':phone:' . $param['phone'];
            $client->appendData('phone', $param['phone'] ?? '');
        } elseif (!empty($param['email'] ?? '')) {
            $cacheKey .= ':email:' . $param['email'];
            $client->appendData('email', $param['email'] ?? '');
        } else {
            return Response::create('', 412, '', '无效的用户ID');
        }

        return $client->appendData('lang', $lang ?: 'zh-cn')
                      ->addHeader('pattern', Authorize::getInstance()->getPattern())
                      ->addHeader('auth-type', Authorize::$_type)
                      ->addHeader('auth-version', Authorize::$_version)
                      ->addHeader('cache', $cache)
                      ->setResponseHeader(true)
                      ->setExpire(Authorize::getInstance()->getExpire())
                      ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                      ->setCacheKey($cacheKey)
                      ->setCache($cache)
                      ->setCallback(function (Response $response, Client $client) {
                          if ($response->api_decode()->getResponseCode() != 200) {
                              $client->setCache(false);
                              $client->clearCache();
                          }

                          return $response;
                      })
                      ->get(Authorize::getInstance()->getHost() . '/api.php/user/info?access_token=' . $access_token, 'json')
                      ->api_decode();
    }

    /**
     * 获取用户列表
     *
     * @param string $access_token
     * @param array  $param
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function list(string $access_token, array $param, $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }

        $cacheKey = 'auth:sdk:user:list';
        $client = Client::getInstance();
        if (!empty($param['order'] ?? '')) {
            $cacheKey .= ':order:' . $param['order'];
            $client->appendData('order', $param['order'] ?? '');
        }
        if (!empty($param['sort'] ?? '')) {
            $cacheKey .= ':sort:' . $param['sort'];
            $client->appendData('sort', $param['sort'] ?? '');
        }
        if (!empty($param['limit'] ?? '')) {
            $cacheKey .= ':limit:' . $param['limit'];
            $client->appendData('limit', $param['limit'] ?? '');
        }

        return $client->addHeader('pattern', Authorize::getInstance()->getPattern())
                      ->addHeader('auth-type', Authorize::$_type)
                      ->addHeader('auth-version', Authorize::$_version)
                      ->addHeader('cache', $cache)
                      ->setResponseHeader(true)
                      ->setExpire(Authorize::getInstance()->getExpire())
                      ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                      ->setCacheKey($cacheKey)
                      ->setCache($cache)
                      ->setCallback(function (Response $response, Client $client) {
                          if ($response->api_decode()->getResponseCode() != 200) {
                              $client->setCache(false);
                              $client->clearCache();
                          }

                          return $response;
                      })
                      ->get(Authorize::getInstance()->getHost() . '/api.php/user/list?access_token=' . $access_token, 'json')
                      ->api_decode();
    }

    /**
     * 退出登录
     *
     * @param string $access_token
     * @param string $openid
     *
     * @return \mark\response\Response
     */
    public static function logout(string $access_token, string $openid): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '无效的OpenID');
        }

        return Client::getInstance()
                     ->appendData('openid', $openid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/user/logout?access_token=' . $access_token, 'json')
                     ->api_decode();
    }
}