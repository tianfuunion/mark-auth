<?php
declare (strict_types=1);

namespace mark\auth\model;

use mark\auth\Authorize;
use mark\http\Client\Client;
use mark\response\Response;

/**
 * Class RoleModel
 *
 * @package mark\auth\model
 */
final class RoleModel {

    /**
     * 获取角色详情
     *
     * @param string $access_token
     * @param string $appid
     * @param string $roleid
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function info(string $access_token, $appid = '', $roleid = '', $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($roleid)) {
            return Response::create('', 412, '', '无效的RoleID');
        }

        $cacheKey = 'auth:sdk:role:info:roleid:' . $roleid;

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->appendData('roleid', $roleid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/role/info?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 获取角色列表
     *
     * @param string $access_token
     * @param string $appid
     * @param string $corpid
     * @param string $projectid
     * @param string $scope
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function list(string $access_token, $appid = '', $corpid = '', $projectid = '', $scope = 'default', $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        $cacheKey = 'auth:sdk:role:list:appid:' . $appid;
        if (!empty($corpid)) {
            $cacheKey .= ':corpid:' . $corpid;
        }
        if (!empty($projectid)) {
            $cacheKey .= ':projectid:' . $projectid;
        }
        if (!empty($scope)) {
            $cacheKey .= ':scope:' . $scope;
        }

        $client = Client::getInstance()
                        ->appendData('appid', $appid);

        if (!empty($corpid)) {
            $client->appendData('corpid', $corpid);
        }

        if (!empty($projectid)) {
            $client->appendData('projectid', $projectid);
        }

        if (!empty($scope)) {
            $client->appendData('scope', $scope);
        }

        return $client->addHeader('pattern', Authorize::getInstance()->getPattern())
                      ->addHeader('auth-type', Authorize::$_type)
                      ->addHeader('auth-version', Authorize::$_version)
                      ->addHeader('cache', $cache)
                      ->setResponseHeader(true)
                      ->setExpire(Authorize::getInstance()->getExpire())
                      ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                      ->setCacheKey($cacheKey)
                      ->setCache($cache)
                      ->setCallback(function (Response $response, Client $client) {
                          if ($response->api_decode()->getResponseCode() != 200) {
                              $client->setCache(false);
                              $client->clearCache();
                          }

                          return $response;
                      })
                      ->get(Authorize::getInstance()->getHost() . '/api.php/role/list?access_token=' . $access_token, 'json')
                      ->api_decode();
    }

    /**
     * 获取角色树形列表
     *
     * @param string $access_token
     * @param string $appid
     * @param string $scope
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function tree(string $access_token, $appid = '', string $scope = 'default', $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        $cacheKey = 'auth:sdk:role:tree:appid:' . $appid;

        return Client::getInstance()
                     ->appendData('appid', $appid)
                     ->appendData('scope', $scope)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(Authorize::getInstance()->getHost() . '/api.php/role/tree?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 创建角色信息
     *
     * @param string $access_token
     * @param array  $role
     *
     * @return \mark\response\Response
     */
    public static function create(string $access_token, array $role): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($role)) {
            return Response::create('', 412, '', '无效的角色信息');
        }

        return Client::getInstance()
                     ->append($role)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/role/create?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 更新角色信息
     *
     * @param string $access_token
     * @param array  $role
     *
     * @return \mark\response\Response
     */
    public static function update(string $access_token, array $role): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($role)) {
            return Response::create('', 412, '', '无效的角色信息');
        }

        return Client::getInstance()
                     ->append($role)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/role/update?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 删除角色信息
     *
     * @param string $access_token
     * @param string $roleid
     *
     * @return \mark\response\Response
     */
    public static function delete(string $access_token, $roleid = ''): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }

        if (empty($roleid)) {
            return Response::create('', 412, '', '无效的RoleID');
        }

        return Client::getInstance()
                     ->appendData('roleid', $roleid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/role/delete?access_token=' . $access_token, 'json')
                     ->api_decode();
    }
}