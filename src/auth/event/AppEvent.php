<?php
declare (strict_types=1);

namespace mark\auth\event;

/**
 * Class AppEvent
 *
 * @package mark\auth\event
 */
final class AppEvent {
    private function __construct() { }

    public static $APP_CREATE = 'app.create';     // 应用创建通知
    public static $APP_UPDATE = 'app.update';     // 应用更新通知
    public static $APP_SUCCESS = 'app.success';   // 应用审核通知
    public static $APP_RELEASE = 'app.release';   // 应用上线通知
    public static $APP_REJECT = 'app.reject';     // 应用驳回通知
    public static $APP_DISABLE = 'app.disable';   // 应用禁用通知
    public static $APP_CLOSED = 'app.closed';     // 应用关闭通知
    public static $APP_REMOVE = 'app.remove';     // 应用移除通知
    public static $APP_DELETE = 'app.delete';     // 应用删除通知
    public static $APP_ABNORMAL = 'app.abnormal'; // 应用异常通知

    public static $CHANNEL_CREATE = 'channel.create';     // 频道创建通知
    public static $CHANNEL_UPDATE = 'channel.update';     // 频道更新通知
    public static $CHANNEL_SUCCESS = 'channel.success';   // 频道审核通知
    public static $CHANNEL_RELEASE = 'channel.release';   // 频道上线通知
    public static $CHANNEL_REJECT = 'channel.reject';     // 频道驳回通知
    public static $CHANNEL_DISABLE = 'channel.disable';   // 频道禁用通知
    public static $CHANNEL_CLOSED = 'channel.closed';     // 频道关闭通知
    public static $CHANNEL_REMOVE = 'channel.remove';     // 频道移除通知
    public static $CHANNEL_DELETE = 'channel.delete';     // 频道删除通知
    public static $CHANNEL_ABNORMAL = 'channel.abnormal'; // 频道异常通知

    public static $ACCESS_CREATE = 'access.create';     // 权限创建通知
    public static $ACCESS_UPDATE = 'access.update';     // 权限更新通知
    public static $ACCESS_SUCCESS = 'access.success';   // 权限审核通知
    public static $ACCESS_RELEASE = 'access.release';   // 权限上线通知
    public static $ACCESS_REJECT = 'access.reject';     // 权限驳回通知
    public static $ACCESS_DISABLE = 'access.disable';   // 权限禁用通知
    public static $ACCESS_CLOSED = 'access.closed';     // 权限关闭通知
    public static $ACCESS_REMOVE = 'access.remove';     // 权限移除通知
    public static $ACCESS_DELETE = 'access.delete';     // 权限删除通知
    public static $ACCESS_ABNORMAL = 'access.abnormal'; // 权限异常通知

    public static $USER_CREATE = 'user.create';        // 用户创建通知
    public static $USER_UPDATE = 'user.update';        // 用户更新通知
    public static $USER_SUCCESS = 'user.success';      // 用户审核通知
    public static $USER_RELEASE = 'user.release';      // 用户上线通知
    public static $USER_REJECT = 'user.reject';        // 用户驳回通知
    public static $USER_DISABLE = 'user.disable';      // 用户禁用通知
    public static $USER_FROZEN = 'user.frozen';        // 用户冻结通知
    public static $USER_RECOVERY = 'user.recovery';    // 用户恢复通知
    public static $USER_REMOVE = 'user.remove';        // 用户移除通知
    public static $USER_DELETE = 'user.delete';        // 用户删除通知
    public static $USER_ABNORMAL = 'user.abnormal';    // 用户异常通知

    public static $UNION_CREATE = 'union.create';             // 授权创建通知
    public static $UNION_UPDATE = 'union.update';             // 授权更新通知
    public static $UNION_SUCCESS = 'union.success';           // 授权审核通知
    public static $UNION_RELEASE = 'union.release';           // 授权上线通知
    public static $UNION_REJECT = 'union.reject';             // 授权驳回通知
    public static $UNION_DISABLE = 'union.disable';           // 授权禁用通知
    public static $UNION_REVOKED = 'union.revoked';           // 授权撤销通知
    public static $UNION_CLOSED = 'union.closed';             // 授权关闭通知
    public static $UNION_REMOVE = 'union.remove';             // 授权移除通知
    public static $UNION_DELETE = 'union.delete';             // 授权删除通知
    public static $UNION_ABNORMAL = 'union.abnormal';         // 授权异常通知

    public static $CORPORATE_CREATE = 'corporate.create';     // 企业创建通知
    public static $CORPORATE_UPDATE = 'corporate.update';     // 企业更新通知
    public static $CORPORATE_SUCCESS = 'corporate.success';   // 企业审核通知
    public static $CORPORATE_RELEASE = 'corporate.release';   // 企业上线通知
    public static $CORPORATE_REJECT = 'corporate.reject';     // 企业驳回通知
    public static $CORPORATE_DISABLE = 'corporate.disable';   // 企业禁用通知
    public static $CORPORATE_REVOKED = 'corporate.revoked';   // 企业撤销通知
    public static $CORPORATE_CLOSED = 'corporate.closed';     // 企业关闭通知
    public static $CORPORATE_REMOVE = 'corporate.remove';     // 企业移除通知
    public static $CORPORATE_DELETE = 'corporate.delete';     // 企业删除通知
    public static $CORPORATE_ABNORMAL = 'corporate.abnormal'; // 企业异常通知

    public static $LICENSE_CREATE = 'license.create';     // 营业执照创建通知
    public static $LICENSE_UPDATE = 'license.update';     // 营业执照更新通知
    public static $LICENSE_SUCCESS = 'license.success';   // 营业执照审核通知
    public static $LICENSE_RELEASE = 'license.release';   // 营业执照上线通知
    public static $LICENSE_REJECT = 'license.reject';     // 营业执照驳回通知
    public static $LICENSE_DISABLE = 'license.disable';   // 营业执照禁用通知
    public static $LICENSE_REVOKED = 'license.revoked';   // 营业执照撤销通知
    public static $LICENSE_CLOSED = 'license.closed';     // 营业执照关闭通知
    public static $LICENSE_REMOVE = 'license.remove';     // 营业执照移除通知
    public static $LICENSE_DELETE = 'license.delete';     // 营业执照删除通知
    public static $LICENSE_ABNORMAL = 'license.abnormal'; // 营业执照异常通知

    public static $LEGAL_CREATE = 'legal.create';          // 法人创建通知
    public static $LEGAL_UPDATE = 'legal.update';          // 法人更新通知
    public static $LEGAL_SUCCESS = 'legal.success';        // 法人审核通知
    public static $LEGAL_RELEASE = 'legal.release';        // 法人上线通知
    public static $LEGAL_REJECT = 'legal.reject';          // 法人驳回通知
    public static $LEGAL_DISABLE = 'legal.disable';        // 法人禁用通知
    public static $LEGAL_REVOKED = 'legal.revoked';        // 法人撤销通知
    public static $LEGAL_CLOSED = 'legal.closed';          // 法人关闭通知
    public static $LEGAL_REMOVE = 'legal.remove';          // 法人移除通知
    public static $LEGAL_DELETE = 'legal.delete';          // 法人删除通知
    public static $LEGAL_ABNORMAL = 'legal.abnormal';      // 法人异常通知

    /**
     * 事件列表
     *
     * @var array[]
     */
    private static $event_type = array(
        'app.create' => array('title' => '应用创建通知', 'name' => 'app.create', 'describe' => ''),
        'app.update' => array('title' => '应用更新通知', 'name' => 'app.update', 'describe' => ''),
        'app.success' => array('title' => '应用审核通知', 'name' => 'app.success', 'describe' => ''),
        'app.release' => array('title' => '应用上线通知', 'name' => 'app.release', 'describe' => ''),
        'app.reject' => array('title' => '应用驳回通知', 'name' => 'app.reject', 'describe' => ''),
        'app.disable' => array('title' => '应用禁用通知', 'name' => 'app.disable', 'describe' => ''),
        'app.closed' => array('title' => '应用关闭通知', 'name' => 'app.closed', 'describe' => ''),
        'app.remove' => array('title' => '应用移除通知', 'name' => 'app.remove', 'describe' => ''),
        'app.delete' => array('title' => '应用删除通知', 'name' => 'app.delete', 'describe' => ''),
        'app.abnormal' => array('title' => '应用异常通知', 'name' => 'app.abnormal', 'describe' => ''),

        'channel.create' => array('title' => '频道创建通知', 'name' => 'channel.create', 'describe' => ''),
        'channel.update' => array('title' => '频道更新通知', 'name' => 'channel.update', 'describe' => ''),
        'channel.success' => array('title' => '频道审核通知', 'name' => 'channel.success', 'describe' => ''),
        'channel.release' => array('title' => '频道上线通知', 'name' => 'channel.release', 'describe' => ''),
        'channel.reject' => array('title' => '频道驳回通知', 'name' => 'channel.reject', 'describe' => ''),
        'channel.disable' => array('title' => '频道禁用通知', 'name' => 'channel.disable', 'describe' => ''),
        'channel.closed' => array('title' => '频道关闭通知', 'name' => 'channel.closed', 'describe' => ''),
        'channel.remove' => array('title' => '频道移除通知', 'name' => 'channel.remove', 'describe' => ''),
        'channel.delete' => array('title' => '频道删除通知', 'name' => 'channel.delete', 'describe' => ''),
        'channel.abnormal' => array('title' => '频道异常通知', 'name' => 'channel.abnormal', 'describe' => ''),

        'access.create' => array('title' => '权限创建通知', 'name' => 'access.create', 'describe' => ''),
        'access.update' => array('title' => '权限更新通知', 'name' => 'access.update', 'describe' => ''),
        'access.success' => array('title' => '权限审核通知', 'name' => 'access.success', 'describe' => ''),
        'access.release' => array('title' => '权限上线通知', 'name' => 'access.release', 'describe' => ''),
        'access.reject' => array('title' => '权限驳回通知', 'name' => 'access.reject', 'describe' => ''),
        'access.disable' => array('title' => '权限禁用通知', 'name' => 'access.disable', 'describe' => ''),
        'access.closed' => array('title' => '权限关闭通知', 'name' => 'access.closed', 'describe' => ''),
        'access.remove' => array('title' => '权限移除通知', 'name' => 'access.remove', 'describe' => ''),
        'access.delete' => array('title' => '权限删除通知', 'name' => 'access.delete', 'describe' => ''),
        'access.abnormal' => array('title' => '权限异常通知', 'name' => 'access.abnormal', 'describe' => ''),

        'user.create' => array('title' => '用户创建通知', 'name' => 'user.create', 'describe' => ''),
        'user.update' => array('title' => '用户更新通知', 'name' => 'user.update', 'describe' => ''),
        'user.success' => array('title' => '用户审核通知', 'name' => 'user.success', 'describe' => ''),
        'user.release' => array('title' => '用户上线通知', 'name' => 'user.release', 'describe' => ''),
        'user.reject' => array('title' => '用户驳回通知', 'name' => 'user.reject', 'describe' => ''),
        'user.disable' => array('title' => '用户禁用通知', 'name' => 'user.disable', 'describe' => ''),
        'user.frozen' => array('title' => '用户冻结通知', 'name' => 'user.frozen', 'describe' => ''),
        'user.recovery' => array('title' => '用户恢复通知', 'name' => 'user.recovery', 'describe' => ''),

        'user.remove' => array('title' => '用户移除通知', 'name' => 'user.remove', 'describe' => ''),
        'user.delete' => array('title' => '用户删除通知', 'name' => 'user.delete', 'describe' => ''),
        'user.abnormal' => array('title' => '用户异常通知', 'name' => 'user.abnormal', 'describe' => ''),

        'union.create' => array('title' => '授权创建通知', 'name' => 'union.create', 'describe' => ''),
        'union.update' => array('title' => '授权更新通知', 'name' => 'union.update', 'describe' => ''),
        'union.success' => array('title' => '授权审核通知', 'name' => 'union.success', 'describe' => ''),
        'union.release' => array('title' => '授权上线通知', 'name' => 'union.release', 'describe' => ''),
        'union.reject' => array('title' => '授权驳回通知', 'name' => 'union.reject', 'describe' => ''),
        'union.disable' => array('title' => '授权禁用通知', 'name' => 'union.disable', 'describe' => ''),
        'union.revoked' => array('title' => '授权撤销通知', 'name' => 'union.revoked', 'describe' => ''),
        'union.closed' => array('title' => '授权关闭通知', 'name' => 'union.closed', 'describe' => ''),
        'union.remove' => array('title' => '授权移除通知', 'name' => 'union.remove', 'describe' => ''),
        'union.delete' => array('title' => '授权删除通知', 'name' => 'union.delete', 'describe' => ''),
        'union.abnormal' => array('title' => '授权异常通知', 'name' => 'union.abnormal', 'describe' => ''),

        'corporate.create' => array('title' => '企业创建通知', 'name' => 'corporate.create', 'describe' => ''),
        'corporate.update' => array('title' => '企业更新通知', 'name' => 'corporate.update', 'describe' => ''),
        'corporate.success' => array('title' => '企业审核通知', 'name' => 'corporate.success', 'describe' => ''),
        'corporate.release' => array('title' => '企业上线通知', 'name' => 'corporate.release', 'describe' => ''),
        'corporate.reject' => array('title' => '企业驳回通知', 'name' => 'corporate.reject', 'describe' => ''),
        'corporate.disable' => array('title' => '企业禁用通知', 'name' => 'corporate.disable', 'describe' => ''),
        'corporate.revoked' => array('title' => '企业撤销通知', 'name' => 'corporate.revoked', 'describe' => ''),
        'corporate.closed' => array('title' => '企业关闭通知', 'name' => 'corporate.closed', 'describe' => ''),
        'corporate.remove' => array('title' => '企业移除通知', 'name' => 'corporate.remove', 'describe' => ''),
        'corporate.delete' => array('title' => '企业删除通知', 'name' => 'corporate.delete', 'describe' => ''),
        'corporate.abnormal' => array('title' => '企业异常通知', 'name' => 'corporate.abnormal', 'describe' => ''),

        'license.create' => array('title' => '营业执照创建通知', 'name' => 'license.create', 'describe' => ''),
        'license.update' => array('title' => '营业执照更新通知', 'name' => 'license.update', 'describe' => ''),
        'license.success' => array('title' => '营业执照审核通知', 'name' => 'license.success', 'describe' => ''),
        'license.release' => array('title' => '营业执照上线通知', 'name' => 'license.release', 'describe' => ''),
        'license.reject' => array('title' => '营业执照驳回通知', 'name' => 'license.reject', 'describe' => ''),
        'license.disable' => array('title' => '营业执照禁用通知', 'name' => 'license.disable', 'describe' => ''),
        'license.revoked' => array('title' => '营业执照撤销通知', 'name' => 'license.revoked', 'describe' => ''),
        'license.closed' => array('title' => '营业执照关闭通知', 'name' => 'license.closed', 'describe' => ''),
        'license.remove' => array('title' => '营业执照移除通知', 'name' => 'license.remove', 'describe' => ''),
        'license.delete' => array('title' => '营业执照删除通知', 'name' => 'license.delete', 'describe' => ''),
        'license.abnormal' => array('title' => '营业执照异常通知', 'name' => 'license.abnormal', 'describe' => ''),

        'legal.create' => array('title' => '法人创建通知', 'name' => 'legal.create', 'describe' => ''),
        'legal.update' => array('title' => '法人更新通知', 'name' => 'legal.update', 'describe' => ''),
        'legal.success' => array('title' => '法人审核通知', 'name' => 'legal.success', 'describe' => ''),
        'legal.release' => array('title' => '法人上线通知', 'name' => 'legal.release', 'describe' => ''),
        'legal.reject' => array('title' => '法人驳回通知', 'name' => 'legal.reject', 'describe' => ''),
        'legal.disable' => array('title' => '法人禁用通知', 'name' => 'legal.disable', 'describe' => ''),
        'legal.revoked' => array('title' => '法人撤销通知', 'name' => 'legal.revoked', 'describe' => ''),
        'legal.closed' => array('title' => '法人关闭通知', 'name' => 'legal.closed', 'describe' => ''),
        'legal.remove' => array('title' => '法人移除通知', 'name' => 'legal.remove', 'describe' => ''),
        'legal.delete' => array('title' => '法人删除通知', 'name' => 'legal.delete', 'describe' => ''),
        'legal.abnormal' => array('title' => '法人异常通知', 'name' => 'legal.abnormal', 'describe' => '')
    );

    /**
     * 获取通知事件
     *
     * @param string $type
     *
     * @return array
     */
    public static function getEventType(string $type): array {
        if (empty($type)) {
            return array();
        }

        if (in_array(strtolower($type), self::$event_type)) {
            return self::$event_type[$type];
        }

        return array();
    }

    /**
     * 获取事件列表
     *
     * @return array[]
     */
    public static function getEventList(): array {
        return self::$event_type;
    }

}