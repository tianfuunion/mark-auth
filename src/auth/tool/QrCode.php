<?php
declare (strict_types=1);

namespace mark\auth\tool;

use mark\auth\Authorize;
use mark\http\Client\Client;
use mark\response\Response;

/**
 * Class QrCode
 *
 * @description 二维码
 * @package     mark\auth\tool
 */
final class QrCode {

    /**
     * QrCode constructor.
     */
    private function __construct() { }

    /**
     * 生成二维码
     *
     * @param string $access_token
     * @param array  $options
     *
     * @return \mark\response\Response
     */
    public static function encode(string $access_token, $options = array()): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }

        return Client::getInstance()
                     ->appendData('appid', Authorize::getInstance()->getAppId())
                     ->append($options)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/qrcode/encode?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 解析二维码
     *
     * @param string $access_token
     * @param string $url
     *
     * @return \mark\response\Response
     */
    public static function decode(string $access_token, string $url = ''): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }

        return Client::getInstance()
                     ->appendData('appid', Authorize::getInstance()->getAppId())
                     ->appendData('url', urlencode($url))
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->get(Authorize::getInstance()->getHost() . '/api.php/qrcode/decode?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 创建二维码
     *
     * @param string $access_token
     * @param array  $data
     *
     * @return \mark\response\Response
     * @see \mark\auth\tool\QrCode::encode()
     * @deprecated
     */
    public static function create(string $access_token, $data = array()): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }

        return Client::getInstance()
                     ->append($data)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/qrcode/create?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

    /**
     * 生成二维码
     *
     * @param string $access_token
     * @param string $text
     * @param array  $label
     *
     * @return \mark\response\Response
     * @deprecated
     * @see \mark\auth\tool\QrCode::decode()
     */
    public static function compile(string $access_token, string $text, $label = array()): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }

        return Client::getInstance()
                     ->appendData('text', $text)
                     ->appendData('label', $label)
                     ->appendData('writer', 'png')
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(Authorize::getInstance()->getHost() . '/api.php/qrcode/compile?access_token=' . $access_token, 'json')
                     ->api_decode();
    }

}