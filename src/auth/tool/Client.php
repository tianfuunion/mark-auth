<?php
declare (strict_types=1);

namespace mark\auth\tool;

use mark\auth\Authorize;
use mark\response\Response;

/**
 * Class Client
 *
 * @package mark\auth\tool
 */
class Client {

    /**
     * 获取IP详情
     *
     * @param string $access_token
     * @param string $ip
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public static function getIpInfo(string $access_token, $ip = '', $cache = true): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的access_token');
        }
        if (empty($ip)) {
            return Response::create('', 412, '', '无效的IP');
        }

        $cacheKey = 'auth:sdk:ip:info:' . $ip;

        return \mark\http\Client\Client::getInstance()
                                       ->appendData('ip', $ip)
                                       ->addHeader('pattern', Authorize::getInstance()->getPattern())
                                       ->addHeader('auth-type', Authorize::$_type)
                                       ->addHeader('auth-version', Authorize::$_version)
                                       ->addHeader('cache', $cache)
                                       ->setResponseHeader(true)
                                       ->setExpire(Authorize::getInstance()->getExpire())
                                       ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                                       ->setCacheKey($cacheKey)
                                       ->setCache($cache)
                                       ->setCallback(function (Response $response, \mark\http\Client\Client $client) {
                                           if ($response->api_decode()->getResponseCode() != 200) {
                                               $client->setCache(false);
                                               $client->clearCache();
                                           }

                                           return $response;
                                       })
                                       ->get(Authorize::getInstance()->getHost() . '/api.php/ip/info?access_token=' . $access_token, 'json')
                                       ->api_decode();
    }

}