<?php
declare (strict_types=1);

namespace mark\auth\tool;

use mark\auth\Authorize;
use Psr\Log\LoggerInterface;

/**
 * Class Logcat
 *
 * @package mark\auth\tool
 */
final class Logcat implements LoggerInterface {

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logcat;

    public function __construct(LoggerInterface $logcat = null) {
        if (!empty($logcat) && $logcat instanceof LoggerInterface) {
            $this->logcat = $logcat;
        }
    }

    /**
     * System is unusable.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function emergency($message, array $context = array()): void {
        $this->logcat->emergency($message, $context);
    }

    /**
     * Action must be taken immediately.
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function alert($message, array $context = array()): void {
        $this->logcat->alert($message, $context);
    }

    /**
     * Critical conditions.
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function critical($message, array $context = array()): void {
        $this->logcat->critical($message, $context);
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function error($message, array $context = array()): void {
        $this->logcat->error($message, $context);
    }

    /**
     * Exceptional occurrences that are not errors.
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function warning($message, array $context = array()): void {
        $this->logcat->warning($message, $context);
    }

    /**
     * Normal but significant events.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function notice($message, array $context = array()): void {
        if (Authorize::getInstance()->isDebug()) {
            $this->logcat->notice($message, $context);
        }
    }

    /**
     * Interesting events.
     * Example: User logs in, SQL logs.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function info($message, array $context = array()): void {
        if (Authorize::getInstance()->isDebug()) {
            $this->logcat->info($message, $context);
        }
    }

    /**
     * Detailed debug information.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function debug($message, array $context = array()): void {
        if (Authorize::getInstance()->isDebug()) {
            $this->logcat->debug($message, $context);
        }
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed   $level
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function log($level, $message, array $context = array()): void {
        switch ($level) {
            case 'info':
            case 'notice':
            case 'debug':
                if (Authorize::getInstance()->isDebug()) {
                    $this->logcat->log($level, $message, $context);
                }
                break;
            case 'warning':
            case 'error':
            case 'critical':
            case 'alert':
            case 'emergency':
            default:
                $this->logcat->log($level, $message, $context);
                break;
        }
    }
}