<?php
declare (strict_types=1);

namespace mark\auth\sso;

use mark\response\Response;

/**
 * Interface OAuthInterface
 *
 * @package mark\auth\sso
 */
interface OAuthInterface {

    /**
     * 创建Facade实例。
     *
     * @param bool $origin
     *
     * @return static
     */
    public static function getInstance(bool $origin = false);

    /**
     * 第一步：用户同意授权，获取code
     *
     * @param string $redirect_uri
     * @param string $scope
     * @param string $state
     *
     * @return \mark\response\Response
     */
    public function getCode(string $redirect_uri, $scope = '', $state = ''): Response;

    /**
     * 第二步：通过code换取网页授权access_token
     *
     * @param string $code
     * @param string $state
     *
     * @return \mark\response\Response
     */
    public function getAccessToken(string $code = '', string $state = ''): Response;

    /**
     * 第三步：刷新access_token（如果需要）
     *
     * @param string $refresh_token
     *
     * @return \mark\response\Response
     */
    public function refreshToken(string $refresh_token): Response;

    /**
     * 第四步：通过code换取网页授权openid
     *
     * @param string $code
     *
     * @return \mark\response\Response
     */
    public function getOpenID(string $code): Response;

    /**
     * 第四步：拉取用户信息(需scope为 snsapi_userinfo)
     *
     * @param string $access_token
     * @param string $openid
     * @param string $lang
     *
     * @return \mark\response\Response
     */
    public function getUserInfo(string $access_token, string $openid, string $lang = 'zh-cn'): Response;

    /**
     * 附：检验授权凭证（access_token）是否有效
     *
     * @param string $access_token
     * @param string $openid
     *
     * @return \mark\response\Response
     */
    public function verifyToken(string $access_token, string $openid): Response;

}