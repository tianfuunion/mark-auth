<?php
declare (strict_types=1);

namespace mark\auth\sso\driver;

use mark\auth\Authorize;
use mark\auth\sso\Sso;
use mark\http\Client\Client;
use mark\response\Response;
use mark\system\Os;

/**
 * Class WeChatWeb
 *
 * @description 网站应用微信登录是基于OAuth2.0协议标准构建的微信OAuth2.0授权登录系统
 * @link        https://developers.weixin.qq.com/doc/oplatform/Website_App/WeChat_Login/Wechat_Login.html
 * @package     mark\auth\sso\driver
 */
class WeChatWeb extends Sso {

    public const GET_AUTH_CODE_URL = 'https://open.weixin.qq.com/connect/qrconnect';
    public const GET_ACCESS_TOKEN_URL = 'https://api.weixin.qq.com/sns/oauth2/access_token';
    public const GET_REFRESH_TOKEN_URL = 'https://api.weixin.qq.com/sns/oauth2/refresh_token';
    public const GET_OPENID_URL = '';
    public const GET_USER_INFO_URL = 'https://api.weixin.qq.com/sns/userinfo';
    public const GET_VERIFY_TOKEN_URL = 'https://api.weixin.qq.com/sns/auth';

    private static $instance;

    /**
     * 创建Facade实例。
     *
     * @param bool $origin
     *
     * @return static
     */
    public static function getInstance(bool $origin = false): self {
        if (!(self::$instance instanceof self) || $origin) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $redirect_uri
     * @param string $code
     * @param string $scope
     * @param string $state
     * @param string $lang
     *
     * @return \mark\response\Response
     */
    public function authorize(string $redirect_uri, $code = '', $scope = 'snsapi_base', $state = '', $lang = 'zh-cn') {
        // 1、第一步：用户同意授权，获取code
        if (empty($code)) {
            return $this->getCode($redirect_uri, $scope ?: 'snsapi_base', $state);
        }

        //2、第二步：通过code换取网页授权access_token
        $token = $this->getAccessToken($code, $state);

        if ($scope === 'snsapi_base') {
            // return $token;
        }

        //4、第四步：拉取用户信息(需scope为 snsapi_userinfo)
        return $this->getUserInfo($token['access_token'] ?? '', $token['openid'] ?? '', $lang);
    }

    /**
     * @param string $redirect_uri
     * @param string $scope
     * @param string $state
     *
     * @return \mark\response\Response
     */
    public function getCode(string $redirect_uri, $scope = 'snsapi_login', $state = ''): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($redirect_uri)) {
            return Response::create('', 412, '', '缺少redirect_uri参数');
        }
        if (empty($scope)) {
            $scope = 'snsapi_login';
        }
        if (empty($state)) {
            $state = $this->generateState('md5', true);
        }

        $auth_url = self::GET_AUTH_CODE_URL
            . '?response_type=code'
            . '&appid=' . $this->appid
            . '&redirect_uri=' . rawurlencode($redirect_uri)
            . '&scope=' . $scope
            . '&state=' . $state
            . '#wechat_redirect';

        // header('HTTP/1.1 301 Moved Permanently');
        // header('Location:' . $auth_url);

        return Response::create($auth_url, 302);
    }

    /**
     * Step2：通过Authorization Code获取AccessToken
     *
     * @param string $code
     * @param string $state
     *
     * @return \mark\response\Response
     */
    public function getAccessToken(string $code = '', string $state = ''): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        $cacheKey = 'auth:sso:wechat_web:access_token:appid:' . $this->appid . ':agent_md5:' . md5(Os::getAgent() . Os::getIpvs());

        if (empty($code)) {
            // throw new InvalidArgumentException('缺少Code参数', 412);
        }

        if (!$this->checkState($state) && !empty($state)) {
            Authorize::getInstance()->getLogcat()->warning('OAuth.WeChatWeb::getAccessToken(checkState)' . $state . '=>' . $this->getState());
            if (Authorize::getInstance()->isStrict()) {
                return Response::create('', 425, 'The state does not match. You may be a victim of CSRF');
            }
        }

        if (empty($this->secret)) {
            return Response::create('', 412, '', '缺少AppSecret参数');
        }

        // 构造请求access_token的url
        $token_url = self::GET_ACCESS_TOKEN_URL . '?grant_type=' . 'authorization_code' . '&appid=' . $this->appid . '&secret=' . $this->secret . '&code=' . $code;
        return Client::getInstance()
                     ->setExpire(7000)
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache(empty($code))
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->get($token_url, 'json');
    }

    /**
     * @param string $refresh_token
     *
     * @return \mark\response\Response
     */
    public function refreshToken(string $refresh_token): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($refresh_token)) {
            return Response::create('', 412, '', '缺少RefreshToken参数');
        }

        $refresh_url = self::GET_REFRESH_TOKEN_URL . '?grant_type=refresh_token' . '&appid=' . $this->appid . '&refresh_token=' . $refresh_token;
        return Client::getInstance()
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->get($refresh_url, 'json');
    }

    /**
     * @param string $access_token
     *
     * @return \mark\response\Response
     */
    public function getOpenID(string $access_token): Response {
        return self::getAccessToken($access_token);
    }

    /**
     * @param string $access_token
     * @param string $openid
     * @param string $lang
     *
     * @return \mark\response\Response
     */
    public function getUserInfo(string $access_token, string $openid, string $lang = 'zh-CN'): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少AccessToken参数');
        }
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '缺少OpenID参数');
        }

        $userinfo_url = self::GET_USER_INFO_URL . '?access_token=' . $access_token . '&openid=' . $openid . '&lang=' . $lang;
        return Client::getInstance()
            // ->setExpire(Authorize::getInstance()->getExpire())
            // ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
            // ->setCacheKey($cacheKey)
            // ->setCache(Authorize::getInstance()->isCache())
                     ->setCache(false)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->get($userinfo_url, 'json');
    }

    /**
     * @param string $access_token
     * @param string $openid
     *
     * @return \mark\response\Response
     */
    public function verifyToken(string $access_token, string $openid): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少AccessToken参数');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '缺少OpenID参数');
        }

        $verify_url = self::GET_VERIFY_TOKEN_URL . '?access_token=' . $access_token . '&openid=' . $openid;
        return Client::getInstance()
                     ->setCache(false)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->get($verify_url, 'json');
    }
}