<?php
declare (strict_types=1);

namespace mark\auth\sso\driver;

use mark\auth\Authorize;
use mark\auth\sso\Sso;
use mark\http\Client\Client;
use mark\response\Response;
use mark\system\Os;
use Psr\SimpleCache\InvalidArgumentException as CacheInvalidArgumentException;

/**
 * Class WeChatProvider
 *
 * @description  如果用户在微信客户端中访问第三方网页，公众号可以通过微信网页授权机制，来获取用户基本信息，进而实现业务逻辑。
 * @author       Mark<mark@tianfuunion.cn>
 * @package      mark\auth\sso\driver
 */
class WeChatProvider extends Sso {

    public const GET_AUTH_CODE_URL = 'https://open.weixin.qq.com/connect/oauth2/authorize';
    public const GET_ACCESS_TOKEN_URL = 'https://api.weixin.qq.com/sns/oauth2/access_token';
    public const GET_REFRESH_TOKEN_URL = 'https://api.weixin.qq.com/sns/oauth2/refresh_token';
    public const GET_OPENID_URL = '';
    public const GET_USER_INFO_URL = 'https://api.weixin.qq.com/sns/userinfo';
    public const GET_VERIFY_TOKEN_URL = 'https://api.weixin.qq.com/sns/auth';
    public const GET_UNION_INFO_URL = 'https://api.weixin.qq.com/cgi-bin/user/info';

    /**
     * @var string
     */
    private $provider_appid = '';

    /**
     * @var string
     */
    private $provider_secret = '';

    /**
     * @var string
     */
    private $component_access_token = '';

    private static $instance;

    /**
     * 创建Facade实例。
     *
     * @param bool $origin
     *
     * @return static
     */
    public static function getInstance(bool $origin = false): self {
        if (!(self::$instance instanceof self) || $origin) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $provider_appid 服务方的 appid，在申请创建公众号服务成功后，可在公众号服务详情页找到
     *
     * @return $this
     */
    public function setProviderAppid(string $provider_appid): self {
        if (!empty($provider_appid)) {
            $this->provider_appid = $provider_appid;
        }

        return $this;
    }

    /**
     * @param string $provider_secret
     *
     * @return $this
     */
    public function setProviderSecret(string $provider_secret): self {
        if (!empty($provider_secret)) {
            $this->provider_secret = $provider_secret;
        }

        return $this;
    }

    /**
     * 设置服务商
     *
     * @param string $access_token
     *
     * @return $this
     */
    public function setComponentAccessToken(string $access_token): self {
        if (!empty($access_token)) {
            $this->component_access_token = $access_token;
        }

        return $this;
    }

    /**
     * @param string $ticket
     * @param string $redirect_uri
     * @param string $code
     * @param string $scope
     * @param string $state
     * @param string $lang
     *
     * @return \mark\response\Response
     */
    public function authorize(string $ticket, string $redirect_uri, $code = '', $scope = 'snsapi_base', $state = '', $lang = 'zh-cn') {
        // 1、第一步：用户同意授权，获取code
        if (empty($code)) {
            return $this->getCode($redirect_uri, $scope, $state);
        }

        $component_access_token = $this->getProviderAccessToken($ticket);
        if (empty($component_access_token)) {
            return Response::create('', 412, 'component_access_token', '缺少component_access_token参数');
        }

        //2、第二步：通过code换取网页授权access_token
        $token = $this->getAccessToken($code, $state)->toArray();
        if ($scope === 'snsapi_base') {
            // return $token;
        }

        //4、第四步：拉取用户信息(需scope为 snsapi_userinfo)
        return $this->getUserInfo($token['access_token'] ?? '', $token['openid'] ?? '', $lang);
    }

    /**
     * 第一步：用户同意授权，获取code
     *
     * @explain      GET：{'code':'081PxY4C06i5ki2MNv5C0LCC4C0PxY4A','state':'5ae74940188ff277cb3ea5021d543ea0'}
     *
     * @param string $redirect_uri   授权后重定向的回调链接地址， 请使用 urlEncode 对链接进行处理
     * @param string $scope          应用授权作用域，snsapi_base （不弹出授权页面，直接跳转，只能获取用户openid），snsapi_userinfo （弹出授权页面，可通过openid拿到昵称、性别、所在地。
     *                               并且， 即使在未关注的情况下，只要用户授权，也能获取其信息 ）
     * @param string $state          重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
     *
     * @return \mark\response\Response
     * @link         https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/Official_Accounts/official_account_website_authorization.html
     */
    public function getCode(string $redirect_uri, $scope = 'snsapi_base', $state = ''): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($redirect_uri)) {
            return Response::create('', 412, '', '无效的redirect_uri');
        }

        switch ($scope) {
            default:
            case 'snsapi_base':
                $scope = 'snsapi_base';
                break;
            case 'snsapi_userinfo':
                $scope = 'snsapi_userinfo';
                break;
        }

        if (empty($state)) {
            $state = $this->generateState('md5', true);
        }

        $url = 'https://open.weixin.qq.com/connect/oauth2/authorize'
            . '?response_type=code'
            . '&appid=' . $this->appid
            . '&component_appid=' . $this->provider_appid
            . '&redirect_uri=' . rawurlencode($redirect_uri)
            . '&scope=' . $scope
            . '&state=' . $state;

        if ($this->forcePopup) {
            $url .= '&forcePopup=true';
        }
        if ($this->forceSnapShot) {
            $url .= '&forceSnapShot=true';
        }

        $url .= '#wechat_redirect';

        // header('HTTP/1.1 301 Moved Permanently');
        // header('Location: ' . $url);
        return Response::create($url, 302);
    }

    /**
     * @var bool 用户弹窗确认
     */
    private $forcePopup = false;
    /**
     * @var bool 快照页逻辑判定
     */
    private $forceSnapShot = false;

    /**
     * 设置用户弹窗确认快照页逻辑判定
     *
     * @param false $forcePopup
     * @param false $forceSnapShot
     */
    public function setforce($forcePopup = false, $forceSnapShot = false) {
        $this->forcePopup = $forcePopup;
        $this->forceSnapShot = $forceSnapShot;
    }

    /**
     * 第二步：通过code换取网页授权access_token
     *
     * @explain JSON：
     * {
     * 'access_token':'ACCESS_TOKEN',
     * 'expires_in':7200,
     * 'refresh_token':'REFRESH_TOKEN',
     * 'openid':'OPENID',
     * 'scope':'SCOPE'
     * }
     * @link    https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html#2
     *
     * @param string $code 用于换取access_token的code，微信提供
     * @param string $state
     *
     * @return \mark\response\Response
     */
    public function getAccessToken(string $code = '', string $state = ''): Response {
        if (empty($this->provider_appid)) {
            return Response::create('', 412, '', '缺少provider_appid参数');
        }

        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        $cacheKey = 'auth:sso:wechat_provider:access_token:appid:' . $this->appid . ':agent_md5:' . md5(Os::getAgent() . Os::getIpvs());

        if (empty($this->component_access_token)) {
            $this->getProviderAccessToken();
        }
        if (empty($this->component_access_token)) {
            return Response::create('', 412, '', '缺少ComponentAccessToken参数');
        }

        if (empty($code)) {
            return Response::create('', 412, '', '缺少Code参数');
        }

        $url = 'https://api.weixin.qq.com/sns/oauth2/component/access_token'
            . '?appid=' . $this->appid
            . '&code=' . $code
            . '&grant_type=authorization_code'
            . '&component_appid=' . $this->provider_appid
            . '&component_access_token=' . $this->component_access_token;

        return Client::getInstance()
                     ->setExpire(7000)
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache(empty($code))
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->get($url, 'json');
    }

    /**
     * 3、第三步：刷新access_token（如果需要）
     *
     * @explain JSON：
     *{
     * 'access_token':'ACCESS_TOKEN',
     * 'expires_in':7200,
     * 'refresh_token':'REFRESH_TOKEN',
     * 'openid':'OPENID',
     * 'scope':'SCOPE'
     * }
     *
     * @param string $refresh_token 填写通过access_token获取到的refresh_token参数
     *
     * @return \mark\response\Response
     * @link    https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html#2
     */
    public function refreshToken(string $refresh_token): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($this->provider_appid)) {
            return Response::create('', 412, '', '缺少provider_appid参数');
        }
        if (empty($refresh_token)) {
            return Response::create('', 412, '', '缺少RefreshToken参数');
        }

        if (empty($this->component_access_token)) {
            return Response::create('', 412, '', '缺少ComponentAccessToken参数');
        }

        $url = 'https://api.weixin.qq.com/sns/oauth2/component/refresh_token'
            . '?appid=' . $this->appid
            . '&grant_type=refresh_token'
            . '&component_appid=' . $this->provider_appid
            . '&component_access_token=' . $this->component_access_token
            . '&refresh_token=' . $refresh_token;

        return Client::getInstance()
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->get($url, 'json');
    }

    /**
     * @param string $code
     *
     * @return \mark\response\Response
     */
    public function getOpenID(string $code): Response {
        return self::getAccessToken($code);
    }

    /**
     * 4、第四步：拉取用户信息(需scope为 snsapi_userinfo)
     *
     * @param string $access_token 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
     * @param string $openid       用户的唯一标识
     * @param string $lang         返回国家地区语言版本，zh-cn 简体，zh-TW 繁体，en 英语
     *
     * @return \mark\response\Response
     * @link https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html#3
     */
    public function getUserInfo(string $access_token, string $openid, string $lang = 'zh-cn'): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少Access_Token参数');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '缺少OpenID参数');
        }

        $cacheKey = 'auth:sso:wechat_provider:userinfo:appid:' . $this->appid . ':openid:' . $openid . ':lang:' . $lang;
        $userinfo_url = self::GET_USER_INFO_URL . '?access_token=' . $access_token . '&openid=' . $openid . '&lang=' . $lang;
        return Client::getInstance()
            // ->setExpire(Authorize::getInstance()->getExpire())
            // ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
            // ->setCacheKey($cacheKey)
            // ->setCache(Authorize::getInstance()->isCache())
                     ->setCache(false)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->get($userinfo_url, 'json');
    }

    /**
     * 5、附：检验授权凭证（access_token）是否有效
     *
     * @param string $access_token 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
     * @param string $openid       用户的唯一标识
     *
     * @return \mark\response\Response
     * @link         https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html#4
     * @example      { 'errcode':0,'errmsg':'ok'}
     */
    public function verifyToken(string $access_token, string $openid): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少AccessToken参数');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '缺少OpenID参数');
        }

        return Client::getInstance()
                     ->setCache(false)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->get('https://api.weixin.qq.com/sns/auth?access_token=' . $access_token . '&openid=' . $openid, 'json');

    }

    /**=======================================================**/

    /**
     * Prpcrypt class
     * 提供接收和推送给公众平台消息的加解密接口.
     */
    public const PRE_AUTH_CODE_KEY = 'auth:sso:wechat_provider:pre_auth_code_key';
    public const COMPONENT_VERIFY_TICKET = 'ComponentVerifyTicket';
    public const COMPONENT_ACCESS_TOKEN = 'component_access_token';
    public const PRE_AUTH_CODE = 'auth:sso:wechat_provider:pre_auth_code';

    /**=====================================================**/

    /**
     * 获取服务商验证票据
     *
     * @param string $ticket_key
     *
     * @return \mark\response\Response
     */
    public function getProviderVerifyTicket(string $ticket_key = ''): Response {
        if (empty($this->provider_appid)) {
            return Response::create('', 412, '', '缺少provider_appid参数');
        }

        try {
            $cacheKey = $ticket_key ?: 'auth:sso:wechat_provider:component_verify_ticket:appid:' . $this->provider_appid;
            if (Authorize::getInstance()->getCacheHandle()->has($cacheKey)) {
                $ticket = Authorize::getInstance()->getCacheHandle()->get($cacheKey);
                if (!empty($ticket[self::COMPONENT_VERIFY_TICKET] ?? '')) {
                    return Response::create($ticket[self::COMPONENT_VERIFY_TICKET] ?? '');
                }
            }

            Authorize::getInstance()->getCacheHandle()->set($cacheKey, '', 1);
            Authorize::getInstance()->getCacheHandle()->delete($cacheKey);
        } catch (CacheInvalidArgumentException $e) {
            return Response::create('', $e->getCode() ?: 500, $e->getMessage(), $e->getMessage());
        }

        return Response::create('', 404);
    }

    /**
     * 缓存从微信接收到的 Component_Verify_Ticket_key
     *
     * @param array  $ticket     Ticket数据
     * @param string $ticket_key 自定义的缓存Key，传空则为默认
     *
     * @return \mark\response\Response
     */
    public function saveProviderVerifyTicket(array $ticket, string $ticket_key = ''): Response {
        if (empty($ticket[self::COMPONENT_VERIFY_TICKET] ?? '')) {
            return Response::create('', 412, '', '缺少Component_Verify_Ticket参数');
        }
        if (empty($ticket['AppId'] ?? '')) {
            return Response::create('', 412, '', '缺少AppId参数');
        }
        if (empty($ticket['CreateTime'] ?? '')) {
            return Response::create('', 412, '', '缺少CreateTime参数');
        }

        try {
            $cacheKey = $ticket_key ?: 'auth:sso:wechat_provider:component_verify_ticket:appid:' . ($ticket['AppId'] ?? '');
            $ticket['Create_Date'] = date('YmdHis', (int)($ticket['CreateTime'] ?: time()));
            $response = Authorize::getInstance()->getCacheHandle()->set($cacheKey, $ticket, 7000 - (time() - (int)($ticket['CreateTime'] ?? '0')));

            return $response ? Response::create('', 200, '', '存储成功') : Response::create('', 404, '', '存储失败');
        } catch (CacheInvalidArgumentException $e) {
            return Response::create('', $e->getCode() ?: 500, $e->getMessage(), $e->getMessage());
        }
    }

    /**
     * 令牌（component_access_token）是第三方平台接口的调用凭据。令牌的获取是有限制的，每个令牌的有效期为 2 小时，请自行做好令牌的管理，在令牌快过期时（比如1小时50分），重新调用接口获取。
     * 如未特殊说明，令牌一般作为被调用接口的 GET 参数 component_access_token 的值使用
     *
     * @param string $ticket component_verify_ticket     微信后台推送的 ticket
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public function getProviderAccessToken($ticket = '', $cache = true): Response {
        if (empty($this->provider_appid)) {
            return Response::create('', 412, '', '无效的Provider_AppID');
        }
        if (empty($this->provider_secret)) {
            return Response::create('', 412, '', '无效的Provider_Secret');
        }

        if (empty($ticket)) {
            $ticket = self::getProviderVerifyTicket()->toArray();
        }

        if (empty($ticket)) {
            return Response::create('', 412, '', '无效的Ticket');
        }

        $cacheKey = 'auth:sso:wechat_provider:component_access_token:provider_appid:' . $this->provider_appid . ':agent_md5:' . md5(Os::getAgent() . Os::getIpvs());

        try {
            if (Authorize::getInstance()->getCacheHandle()->has($cacheKey) && is_true($cache)) {
                $token = Authorize::getInstance()->getCacheHandle()->get($cacheKey);
                if (!empty($token[self::COMPONENT_ACCESS_TOKEN] ?? '')) {
                    $this->setComponentAccessToken($token[self::COMPONENT_ACCESS_TOKEN] ?? '');
                    return Response::create($token);
                }
            }
        } catch (CacheInvalidArgumentException $e) {
            Authorize::getInstance()->getLogcat()->error('OAuth.WeChatProvider::getProviderAccessToken(CacheInvalidArgumentException.get)' . $e->getMessage());
        }

        $client = Client::getInstance()
                        ->appendData('component_appid', $this->provider_appid)
                        ->appendData('component_appsecret', $this->provider_secret)
                        ->appendData('component_verify_ticket', $ticket)
                        ->setExpire(7000)
                        ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                        ->setCacheKey($cacheKey)
                        ->setCache($cache)
                        ->setCallback(function (Response $response, Client $client) {
                            $data = $response->toArray();
                            if (isset($data['errcode']) && $data['errcode'] != 0) {
                                $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                                $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                                $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                                $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                                $client->setCache(false);
                                $client->clearCache();
                                // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                            }

                            return $response;
                        })
                        ->post('https://api.weixin.qq.com/cgi-bin/component/api_component_token', 'json');
        try {
            $response = $client->toArray();
            if (!empty($response[self::COMPONENT_ACCESS_TOKEN] ?? '')) {
                Authorize::getInstance()->getCacheHandle()->set($cacheKey, $response, 7000);
                $this->setComponentAccessToken($response[self::COMPONENT_ACCESS_TOKEN] ?? '');
            } else {
                Authorize::getInstance()->getCacheHandle()->set($cacheKey, '', 1);
                Authorize::getInstance()->getCacheHandle()->delete($cacheKey);
            }
        } catch (CacheInvalidArgumentException $e) {
            Authorize::getInstance()->getLogcat()->error('OAuth.WeChatProvider::getProviderAccessToken(CacheInvalidArgumentException)' . $e->getMessage());
        }

        return $client;
    }

    /**
     * 获得服务商预授权码
     * 预授权码（pre_auth_code）是第三方平台方实现授权托管的必备信息，每个预授权码有效期为 1800秒。需要先获取令牌才能调用。
     *
     * @param string $access_token component_access_token    第三方平台component_access_token，不是authorizer_access_token
     * @param bool   $cache
     *
     * @return \mark\response\Response
     * @link https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/api/component_access_token.html
     */
    public function getProviderPreAuthCode(string $access_token, $cache = true): Response {
        if (empty($this->provider_appid)) {
            return Response::create('', 412, '', '无效的Provider_AppID');
        }
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的component_access_token');
        }

        $cacheKey = self::PRE_AUTH_CODE . ':provider_appid:' . $this->provider_appid;
        return Client::getInstance()
                     ->appendData('component_access_token', $access_token)
                     ->appendData('component_appid', $this->provider_appid)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setResponseHeader(true)
                     ->setExpire(1800)
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         if (empty($data) || !isset($data['pre_auth_code']) || empty($data['pre_auth_code'])) {
                             if (empty($data['errcode'] ?? '')) {
                                 $response->setCode($data['errcode'] ?: 404);
                             }
                             if (empty($data['errmsg'] ?? '')) {
                                 $response->setResponseStatus($data['errmsg'] ?: '');
                                 $response->setResponseReason($data['errmsg'] ?: '');
                             }

                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->post('https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token=' . $access_token, 'json');
    }

    /**
     * 4.使用授权码换取公众号的接口调用凭据和授权信息：
     *
     * @param string $access_token component_access_token
     * @param string $auth_code    authorization_code
     *
     * @return \mark\response\Response
     */
    public function query_auth(string $access_token, string $auth_code): Response {
        if (empty($this->provider_appid)) {
            return Response::create('', 412, '', '无效的Provider_AppID');
        }
        if (empty($access_token)) {
            return Response::create('', 412, '', '无效的component_access_token');
        }
        if (empty($auth_code)) {
            return Response::create('', 412, '', '无效的authorization_code');
        }

        try {
            if (Authorize::getInstance()->getCacheHandle()->has($auth_code)) {
                $response = Authorize::getInstance()->getCacheHandle()->get($auth_code);
                if (!empty($response)) {
                    // return $response;
                }
            }
        } catch (CacheInvalidArgumentException $e) {
            Authorize::getInstance()->getLogcat()->error('OAuth.WeChatProvider::query_auth(CacheInvalidArgumentException)' . $e->getMessage());
        }

        //1. 使用授权码换取公众号的接口调用凭据和授权信息
        return Client::getInstance()
                     ->appendData('component_access_token', $access_token)
                     ->appendData('component_appid', $this->provider_appid)
                     ->appendData('authorization_code', $auth_code)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->post('https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token=' . $access_token, 'json');
    }

    /**
     * 获取授权方的账号基本信息
     *
     * @link  https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/api/api_get_authorizer_info.html
     *
     * @param string $access_token component_access_token 第三方平台component_access_token，不是authorizer_access_token
     * @param string $auth_appid   authorizer_appid       授权方 appid
     *
     * @return \mark\response\Response
     */
    public function getAuthorizerInfo(string $access_token, string $auth_appid): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少component_access_token参数');
        }
        if (empty($auth_appid)) {
            return Response::create('', 412, '', '缺少authorizer_appid参数');
        }
        if (empty($this->provider_appid)) {
            return Response::create('', 412, '', '无效的Provider_AppID');
        }
        return Client::getInstance()
                     ->appendData('component_access_token', $access_token)
                     ->appendData('component_appid', $this->provider_appid)
                     ->appendData('authorizer_appid', $auth_appid)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->post('https://api.weixin.qq.com/cgi-bin/component/api_get_authorizer_info?component_access_token=' . $access_token, 'json');
    }

    /**
     * 拉取所有已授权的账号信息
     *
     * @link  https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/api/api_get_authorizer_list.html
     *
     * @param string $access_token 第三方平台component_access_token，不是authorizer_access_token
     * @param int    $offset       偏移位置/起始位置
     * @param int    $count        拉取数量，最大为 500
     *
     * @return \mark\response\Response
     */
    public function getAuthorizerList(string $access_token, int $offset = 0, int $count = 500): Response {
        if (empty($this->provider_appid)) {
            return Response::create('', 412, '', '缺少provider_appid参数');
        }
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少component_access_token参数');
        }

        return Client::getInstance()
                     ->appendData('component_access_token', $access_token)
                     ->appendData('component_appid', $this->provider_appid)
                     ->appendData('offset', $offset)
                     ->appendData('count', $count)
                     ->setCallback(function (Response $response, Client $client) use ($access_token) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');

                             $client->setCache(false);
                             $client->clearCache();
                             // throw new ResponseException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         } elseif (!empty($data) && isset($data['list']) && !empty($data['list'])) {
                             foreach ($data['list'] as $key => $item) {
                                 $author_resp = self::getAuthorizerInfo($access_token, $item['authorizer_appid'] ?? '');
                                 $author = $author_resp->toArray();
                                 if ($author_resp->getCode() == 200) {
                                     $data['list'][$key] = array_merge($data['list'][$key], $author['authorizer_info'] ?? []);
                                 } else {
                                     $data['list_empty'][$key] = $author_resp->getOrigin();
                                 }
                             }

                             $response->setData($data);
                         }

                         return $response;
                     })
                     ->post('https://api.weixin.qq.com/cgi-bin/component/api_get_authorizer_list?component_access_token=' . $access_token, 'json');
    }

    /**
     * 获取/刷新接口调用令牌
     *
     * @param string $access_token  第三方平台component_access_token
     * @param string $auth_appid    授权方 appid(authorizer_appid)
     * @param string $refresh_token 刷新令牌(authorizer_refresh_token)，获取授权信息时得到
     *
     * @return \mark\response\Response
     */
    public function getAuthorizerToken(string $access_token, string $auth_appid, string $refresh_token): Response {
        if (empty($this->provider_appid)) {
            return Response::create('', 412, '', '缺少provider_appid参数');
        }
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少component_access_token参数');
        }

        return Client::getInstance()
                     ->appendData('component_access_token', $access_token)
                     ->appendData('component_appid', $this->provider_appid)
                     ->appendData('authorizer_appid', $auth_appid)
                     ->appendData('authorizer_refresh_token', $refresh_token)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');

                             $client->setCache(false);
                             $client->clearCache();
                             // throw new ResponseException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->post('https://api.weixin.qq.com/cgi-bin/component/api_authorizer_token?component_access_token=' . $access_token, 'json');
    }

    /**
     * 将公众号/小程序从开放平台账号下解绑
     *
     * @param string $access_token 第三方平台接口调用令牌authorizer_access_token
     * @param string $appid        授权公众号或小程序的 appid
     *
     * @return \mark\response\Response
     */
    public function unbind(string $access_token, string $appid): Response {
        if (empty($this->provider_appid)) {
            return Response::create('', 412, '', '缺少provider_appid参数');
        }
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少authorizer_access_token参数');
        }
        if (empty($appid)) {
            return Response::create('', 412, '', '缺少appid参数');
        }

        return Client::getInstance()
                     ->appendData('access_token', $access_token)
                     ->appendData('appid', $appid)
                     ->appendData('open_appid', $this->provider_appid)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new ResponseException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->post('https://api.weixin.qq.com/cgi-bin/open/unbind?access_token=' . $access_token, 'json');
    }

    /**
     * 获取账号的关注者OpenID列表
     *
     * @param string $access_token
     * @param string $openid
     *
     * @return \mark\response\Response
     */
    public function getUsers(string $access_token, $openid = ''): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少authorizer_access_token参数');
        }

        return Client::getInstance()
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $response->setResponseCode($data['errcode'] ?: 404, false);
                             $response->setResponseStatus($data['errmsg'] ?: '');
                             $response->setResponseReason($data['errmsg'] ?: '');
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);

                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get('https://api.weixin.qq.com/cgi-bin/user/get?access_token=' . $access_token . '&next_openid=' . $openid, 'json');
    }

    /**
     * 启动ticket推送服务
     *
     * @return \mark\response\Response
     */
    public function start_push_ticket(): Response {
        if (empty($this->provider_appid)) {
            return Response::create('', 412, '', '缺少provider_appid参数');
        }
        if (empty($this->provider_secret)) {
            return Response::create('', 412, '', '缺少provider_secret参数');
        }

        return Client::getInstance()
                     ->appendData('component_appid', $this->provider_appid)
                     ->appendData('component_secret', $this->provider_secret)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = WeChat::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->post('https://api.weixin.qq.com/cgi-bin/component/api_start_push_ticket', 'json');
    }

}