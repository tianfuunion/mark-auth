<?php
declare (strict_types=1);

namespace mark\auth\sso\driver;

use mark\auth\Authorize;
use mark\auth\sso\Sso;
use mark\http\Client\Client;
use mark\response\Response;
use mark\system\Os;
use Psr\SimpleCache\InvalidArgumentException as CacheInvalidArgumentException;

/**
 * Class Qq
 *
 * @description 第三方网站接入QQ互联开放平台后，即可通过调用平台提供的API实现用户使用QQ账号登录网站功能，且可以获取到腾讯QQ用户的相关信息。
 * @author      Mark<mark@tianfuunion.cn>
 * @package     mark\auth\sso\driver
 */
class Qq extends Sso {

    public const GET_AUTH_CODE_URL = 'https://graph.qq.com/oauth2.0/authorize';
    public const GET_ACCESS_TOKEN_URL = 'https://graph.qq.com/oauth2.0/token';
    public const GET_REFRESH_TOKEN_URL = 'https://graph.qq.com/oauth2.0/token';
    public const GET_OPENID_URL = 'https://graph.qq.com/oauth2.0/me';
    public const GET_USER_INFO_URL = 'https://graph.qq.com/user/get_user_info';
    public const GET_VERIFY_TOKEN_URL = '';

    /*
     * 初始化APIMap
     * 加#表示非必须，无则不传入url(url中不会出现该参数)， "key" => "val" 表示key如果没有定义则使用默认值val
     * 规则 array( baseUrl, argListArr, method)
     *
     * @var array[]
     */
    public $APIMap = array(
        /*                       qzone                    */
        'add_blog' => array('https://graph.qq.com/blog/add_one_blog', array('title', 'format' => 'json', 'content' => null), 'POST'),
        'add_topic' => array('https://graph.qq.com/shuoshuo/add_topic', array('richtype', 'richval', 'con', '#lbs_nm', '#lbs_x', '#lbs_y', 'format' => 'json', '#third_source'),
                             'POST'),
        'get_user_info' => array('https://graph.qq.com/user/get_user_info', array('format' => 'json'), 'GET'),
        'add_one_blog' => array('https://graph.qq.com/blog/add_one_blog', array('title', 'content', 'format' => 'json'), 'GET'),
        'add_album' => array('https://graph.qq.com/photo/add_album', array('albumname', '#albumdesc', '#priv', 'format' => 'json'), 'POST'),
        'upload_pic' => array('https://graph.qq.com/photo/upload_pic',
                              array('picture', '#photodesc', '#title', '#albumid', '#mobile', '#x', '#y', '#needfeed', '#successnum', '#picnum', 'format' => 'json'), 'POST'),
        'list_album' => array('https://graph.qq.com/photo/list_album', array('format' => 'json')),
        'add_share' => array('https://graph.qq.com/share/add_share',
                             array('title', 'url', '#comment', '#summary', '#images', 'format' => 'json', '#type', '#playurl', '#nswb', 'site', 'fromurl'), 'POST'),
        'check_page_fans' => array('https://graph.qq.com/user/check_page_fans', array('page_id' => '314416946', 'format' => 'json')),
        /*                    wblog                             */
        'add_t' => array('https://graph.qq.com/t/add_t', array('format' => 'json', 'content', '#clientip', '#longitude', '#compatibleflag'), 'POST'),
        'add_pic_t' => array('https://graph.qq.com/t/add_pic_t',
                             array('content', 'pic', 'format' => 'json', '#clientip', '#longitude', '#latitude', '#syncflag', '#compatiblefalg'), 'POST'),
        'del_t' => array('https://graph.qq.com/t/del_t', array('id', 'format' => 'json'), 'POST'),
        'get_repost_list' => array('https://graph.qq.com/t/get_repost_list', array('flag', 'rootid', 'pageflag', 'pagetime', 'reqnum', 'twitterid', 'format' => 'json')),
        'get_info' => array('https://graph.qq.com/user/get_info', array('format' => 'json')),
        'get_other_info' => array('https://graph.qq.com/user/get_other_info', array('format' => 'json', '#name', 'fopenid')),
        'get_fanslist' => array('https://graph.qq.com/relation/get_fanslist', array('format' => 'json', 'reqnum', 'startindex', '#mode', '#install', '#sex')),
        'get_idollist' => array('https://graph.qq.com/relation/get_idollist', array('format' => 'json', 'reqnum', 'startindex', '#mode', '#install')),
        'add_idol' => array('https://graph.qq.com/relation/add_idol', array('format' => 'json', '#name-1', '#fopenids-1'), 'POST'),
        'del_idol' => array('https://graph.qq.com/relation/del_idol', array('format' => 'json', '#name-1', '#fopenid-1'), 'POST'),
        /*                           pay                          */
        'get_tenpay_addr' => array('https://graph.qq.com/cft_info/get_tenpay_addr', array('ver' => 1, 'limit' => 5, 'offset' => 0, 'format' => 'json'))
    );

    private $keysArr;

    /**
     * QQ constructor.
     */
    public function construct() {
        //如果access_token和openid为空，则从session里去取，适用于demo展示情形
        if (empty($access_token)) {
            $token = $this->getAccessToken()->toArray();
            $access_token = $token['access_token'] ?? '';
        }

        if (empty($openid)) {
            $open_resp = $this->getOpenID('')->toArray();
            $openid = $open_resp['openid'] ?? '';
        }

        $this->keysArr = array('oauth_consumer_key' => $this->appid, 'access_token' => $access_token, 'openid' => $openid);
    }

    private static $instance;

    /**
     * 创建Facade实例。
     *
     * @param bool $origin
     *
     * @return static
     */
    public static function getInstance(bool $origin = false): self {
        if (!(self::$instance instanceof self) || $origin) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $redirect_uri
     * @param string $code
     * @param string $scope
     * @param string $state
     * @param string $lang
     *
     * @return \mark\response\Response
     */
    public function authorize(string $redirect_uri, $code = '', $scope = 'snsapi_base', $state = '', $lang = 'zh-cn'): Response {
        // 1、第一步：用户同意授权，获取code
        if (empty($code)) {
            return $this->getCode($redirect_uri, $scope ?: 'snsapi_base', $state);
        }

        //2、第二步：通过code换取网页授权access_token
        $token = $this->getAccessToken($code, $state);
        //3、第三步：通过code换取用户的OpenID
        $openid = $this->getOpenID($token['access_token'] ?? '')->toArray();
        //4、第四步：拉取用户信息(需scope为 snsapi_userinfo)
        return $this->getUserInfo($token['access_token'] ?? '', $openid['openid'] ?? '', $lang);
    }

    /**
     * @param string $redirect_uri
     * @param string $scope
     * @param string $state
     *
     * @return \mark\response\Response
     */
    public function getCode(string $redirect_uri, $scope = 'get_user_info', $state = ''): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($redirect_uri)) {
            return Response::create('', 412, '', '缺少redirect_uri参数');
        }
        if (empty($scope)) {
            $scope = 'get_user_info';
        }
        if (empty($state)) {
            $state = $this->generateState('md5', true);
        }

        $auth_url = self::GET_AUTH_CODE_URL
            . '?response_type=code'
            . '&client_id=' . $this->appid
            . '&redirect_uri=' . rawurlencode($redirect_uri)
            . '&scope=' . $scope
            . '&state=' . $state;

        if (Os::isMobile()) {
            $auth_url .= '&display=mobile';
        }

        try {
            Authorize::getInstance()->getCacheHandle()->set('auth:sso:qq:redirect_uri:appid:' . $this->appid . ':state:' . $state, $redirect_uri, 600);
        } catch (CacheInvalidArgumentException $e) {
            Authorize::getInstance()->getLogcat()->warning('OAuth.QQ::getCode::CacheInvalidArgumentException.set(' . $e->getCode() . ')' . $e->getMessage());
        }

        return Response::create($auth_url, 302);
    }

    /**
     * Step2：通过Authorization Code获取AccessToken
     *
     * @param string $code
     * @param string $state
     *
     * @return \mark\response\Response
     */
    public function getAccessToken(string $code = '', string $state = ''): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        $cacheKey = 'auth:sso:qq:access_token:appid:' . $this->appid . ':agent_md5:' . md5(Os::getAgent() . Os::getIpvs());

        if (empty($code)) {
            return Response::create('', 412, '', '缺少Code参数');
        }

        if (!$this->checkState($state) && !empty($state)) {
            Authorize::getInstance()->getLogcat()->warning('OAuth.QQ::getAccessToken(checkState)' . $state . '=>' . $this->getState());
            if (Authorize::getInstance()->isStrict()) {
                return Response::create('', 425, 'The state does not match. You may be a victim of CSRF');
            }
        }

        $redirect_uri = '';
        try {
            $redirect_uri = Authorize::getInstance()->getCacheHandle()->get('auth:sso:qq:redirect_uri:appid:' . $this->appid . ':state:' . $state);
        } catch (CacheInvalidArgumentException $e) {
            Authorize::getInstance()->getLogcat()->error('OAuth.QQ::getAccessToken::CacheInvalidArgumentException.get' . $e->getMessage(), $e->getCode());
        }
        if (empty($redirect_uri)) {
            return Response::create('', 412, '', '无效的redirect_uri参数');
        }

        if (empty($this->secret)) {
            return Response::create('', 412, '', '缺少AppSecret参数');
        }

        // 构造请求access_token的url
        $token_url = self::GET_ACCESS_TOKEN_URL
            . '?grant_type=' . 'authorization_code'
            . '&client_id=' . $this->appid
            . '&client_secret=' . $this->secret
            . '&code=' . $code
            . '&redirect_uri=' . rawurlencode($redirect_uri)
            . '&fmt=' . 'json';

        return Client::getInstance()
            // ->setExpire(7000)
            // ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
            // ->setCacheKey($cacheKey)
            // ->setCache(Authorize::getInstance()->isCache())
            // ->setCache(empty($code))
                     ->setCache(false)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['ret']) && $data['reg'] != 0) {
                             $response->setResponseCode($data['ret'] ?: 404, false);
                             $response->setResponseStatus($data['msg'] ?: '');
                             $response->setResponseReason($data['msg'] ?: '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                             // throw new OAuthException($data['error_description'] ?? '', $data['error'] ?? 404);
                         }

                         return $response;
                     })
                     ->get($token_url, 'json');
    }

    /**
     * @param string $refresh_token
     *
     * @return \mark\response\Response
     */
    public function refreshToken(string $refresh_token): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($refresh_token)) {
            return Response::create('', 412, '', '缺少RefreshToken参数');
        }

        $refresh_url = self::GET_REFRESH_TOKEN_URL . '?grant_type=refresh_token'
            . '&client_id=' . $this->appid
            . '&client_secret=' . $this->secret
            . '&refresh_token=' . $refresh_token
            . '&fmt=json';

        return Client::getInstance()
                     ->setCache(false)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (!isset($data['error']) || !isset($data['error_description'])) {
                             $response->setCode($data['error'] ?? 404);
                             $response->setResponse($data['error_description'] ?? '');
                             $response->setResponseStatus($data['error_description'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get($refresh_url, 'json');
    }

    /**
     * @param string $access_token
     *
     * @return \mark\response\Response
     */
    public function getOpenID(string $access_token): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少AccessToken参数');
        }

        $graph_url = self::GET_OPENID_URL . '?access_token=' . $access_token . '&fmt=json';
        return Client::getInstance()
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();

                         if (isset($data['error']) || isset($data['error_description'])) {
                             $response->setCode($data['error'] ?? 404);
                             $response->setResponse($data['error_description'] ?? '');
                             $response->setResponseStatus($data['error_description'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         if ($data['client_id'] != $this->appid) {
                             $response->setCode(425);
                             $response->setResponseStatus('');
                             $response->setResponse($response['error_description'] ?? 'OpenID和AppID不匹配');
                             // throw new UnexpectedValueException('OpenID和AppID不匹配');
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get($graph_url, 'json');
    }

    /**
     * @param string $access_token
     * @param string $openid
     * @param string $lang
     *
     * @return \mark\response\Response
     */
    public function getUserInfo(string $access_token, string $openid, string $lang = 'zh-cn'): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少AccessToken参数');
        }
        if (empty($openid)) {
            $open_resp = $this->getOpenID($access_token)->toArray();
            $openid = $open_resp['openid'] ?? '';
        }

        if (empty($openid)) {
            return Response::create('', 412, '', '缺少OpenID参数');
        }

        $graph_url = self::GET_USER_INFO_URL . '?access_token=' . $access_token . '&oauth_consumer_key=' . $this->appid . '&openid=' . $openid;
        return Client::getInstance()
            // ->setExpire(Authorize::getInstance()->getExpire())
            // ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
            // ->setCacheKey($cacheKey)
                     ->setCache(false)
                     ->setCallback(function (Response $response, Client $client) use ($openid) {
                         $data = $response->toArray();
                         if (isset($data['error']) || isset($data['error_description'])) {
                             $response->setResponseCode($data['error'] ?: 404, false);
                             $response->setResponseStatus($data['error_description'] ?: '');
                             $response->setResponseReason($data['error_description'] ?: '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['error_description'] ?: '', $data['error'] ?: 404);
                         }
                         if (isset($data['ret']) && $data['ret'] != 0) {
                             $response->setResponseCode($data['ret'] ?: 404, false);
                             $response->setResponseStatus($data['error_description'] ?: '');
                             $response->setResponseReason($data['error_description'] ?: '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['error_description'] ?: '', $data['error'] ?: 404);
                         }

                         $data['openid'] = $openid;
                         switch ($data['gender_type'] ?? $data['gender'] ?? 0) {
                             case 0:
                             default:
                                 $data['sex'] = 0;
                                 $data['gender'] = '保密';
                                 break;
                             case 1:
                             case '女':
                                 $data['sex'] = 2;
                                 break;
                             case 2:
                             case '男':
                                 $data['sex'] = 1;
                                 break;
                         }
                         $response->setData($data);

                         return $response;
                     })
                     ->get($graph_url, 'json');
    }

    /**
     * @param string $access_token
     * @param string $openid
     *
     * @return \mark\response\Response
     */
    public function verifyToken(string $access_token, string $openid): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少Access_Token参数');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '缺少OpenID参数');
        }

        $verify_url = self::GET_VERIFY_TOKEN_URL . '?access_token=' . $access_token . '&openid=' . $openid;

        return Client::getInstance()
                     ->setCache(false)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (!isset($data['errcode']) || $data['errcode'] != 0) {
                             $response->setResponseCode($data['errcode'] ?? 404);
                             $response->setResponseStatus($data['errmsg'] ?? '');
                             $response->setResponseReason($data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             Authorize::getInstance()->getLogcat()->warning('OAuth.QQ::verifyToken()' . json_encode($response));
                         }

                         return $response;
                     })
                     ->get($verify_url, 'json');
    }

    /**
     * 调用相应api
     *
     * @param $arr
     * @param $argsList
     * @param $baseUrl
     * @param $method
     *
     * @return \mark\response\Response
     */
    private function _applyAPI($arr, $argsList, $baseUrl, $method): Response {
        $pre = '#';
        $keysArr = $this->keysArr;

        $optionArgList = array();//一些多项选填参数必选一的情形
        foreach ($argsList as $key => $val) {
            $tmpKey = $key;
            $tmpVal = $val;

            if (!is_string($key)) {
                $tmpKey = $val;
                if (strpos($val, $pre) === 0) {
                    $tmpVal = $pre;
                    $tmpKey = substr($tmpKey, 1);
                    if (preg_match('/-(\d$)/', $tmpKey, $res)) {
                        $tmpKey = str_replace($res[0], '', $tmpKey);
                        $optionArgList[$res[1]][] = $tmpKey;
                    }
                } else {
                    $tmpVal = null;
                }
            }

            //-----如果没有设置相应的参数
            if (!isset($arr[$tmpKey]) || $arr[$tmpKey] === '') {
                if ($tmpVal == $pre) {//则使用默认的值
                    continue;
                } elseif ($tmpVal) {
                    $arr[$tmpKey] = $tmpVal;
                } elseif ($v = $_FILES[$tmpKey]) {
                    $filename = dirname($v['tmp_name']) . '/' . $v['name'];
                    move_uploaded_file($v['tmp_name'], $filename);
                    $arr[$tmpKey] = "@$filename";
                } else {
                    return Response::create('', 412, '', 'api调用参数错误,未传入参数' . $tmpKey);
                }
            }

            $keysArr[$tmpKey] = $arr[$tmpKey];
        }

        //检查选填参数必填一的情形
        foreach ($optionArgList as $val) {
            $n = 0;
            foreach ($val as $v) {
                if (in_array($v, array_keys($keysArr))) {
                    $n++;
                }
            }

            if (!$n) {
                $str = implode(',', $val);
                return Response::create('', 412, '', 'api调用参数错误' . $str . '必填一个');
            }
        }

        switch (strtolower($method)) {
            case 'get':
                $response = Client::getInstance()->append($keysArr)->get($baseUrl, 'json');
                break;
            case 'post':
                if ($baseUrl == 'https://graph.qq.com/blog/add_one_blog') {
                    $response = Client::getInstance()->append($keysArr)->post($baseUrl, 'json');
                } else {
                    $response = Client::getInstance()->append($keysArr)->post($baseUrl, 'json');
                }
                break;
            default:
                // throw new ResponseException('非法的请求方式:' . $method, 405);
                return Response::create('', 405, '', '非法的请求方式:' . $method);
            // not break;
        }

        return $response;
    }

    /**
     * _call
     * 魔术方法，做api调用转发
     *
     * @param string $name 调用的方法名称
     * @param array  $arg  参数列表数组
     *
     * @return \mark\response\Response 返加调用结果数组
     * @since 5.0
     */
    public function __call($name = '', $arg = array()): Response {
        //如果APIMap不存在相应的api
        if (empty($this->APIMap[$name])) {
            return Response::create('', 404, '', "api调用名称错误,不存在的API: <span style='color:#ff0000;'>$name</span>");
        }

        //从APIMap获取api相应参数
        $baseUrl = $this->APIMap[$name][0];
        $argsList = $this->APIMap[$name][1];
        $method = isset($this->APIMap[$name][2]) ? $this->APIMap[$name][2] : 'GET';

        if (empty($arg)) {
            $arg[0] = null;
        }

        //对于get_tenpay_addr，特殊处理，php json_decode对\xA312此类字符支持不好
        $request = $this->_applyAPI($arg[0], $argsList, $baseUrl, $method)->toJson();
        if ($name == 'get_tenpay_addr') {
            $responseArr = $this->simple_json_parser($request);
        } else {
            $response = json_decode($request);
            $responseArr = $this->objToArr($response);
        }
        //检查返回ret判断api是否成功调用
        if ($responseArr['ret'] == 0) {
            return Response::create($responseArr);
        }

        Authorize::getInstance()->getLogcat()->warning('OAuth.QQ::__call()' . json_encode($responseArr));
        return Response::create($responseArr, $responseArr['ret'] ?? 404, '', $responseArr['msg'] ?? '');
    }

    /**
     * php 对象到数组转换
     *
     * @param $obj
     *
     * @return array
     */
    private function objToArr($obj): array {
        if (!is_object($obj) && !is_array($obj)) {
            return $obj;
        }

        $arr = array();
        foreach ($obj as $k => $v) {
            $arr[$k] = $this->objToArr($v);
        }

        return $arr ?: array();
    }

    /**
     * 简单实现json到php数组转换功能
     *
     * @param string $json
     *
     * @return array
     */
    private function simple_json_parser(string $json): array {
        $json = str_replace('{', '', str_replace('}', '', $json));
        $jsonValue = explode(',', $json);

        $arr = array();
        foreach ($jsonValue as $v) {
            $jValue = explode(':', $v);
            $arr[str_replace('"', '', $jValue[0])] = (str_replace('"', '', $jValue[1]));
        }

        return $arr;
    }
}