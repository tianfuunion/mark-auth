<?php
declare (strict_types=1);

namespace mark\auth\sso\driver;

use mark\auth\Authorize;
use mark\auth\sso\Sso;
use mark\http\Client\Client;
use mark\response\Response;
use mark\system\Os;

/**
 * Class Wechat
 *
 * @description 如果用户在微信客户端中访问第三方网页，公众号可以通过微信网页授权机制，来获取用户基本信息，进而实现业务逻辑。
 * @author      Mark<mark@tianfuunion.cn>
 * @link        https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html
 * @package     mark\auth\sso\driver
 */
class WeChat extends Sso {

    public const GET_AUTH_CODE_URL = 'https://open.weixin.qq.com/connect/oauth2/authorize';
    public const GET_ACCESS_TOKEN_URL = 'https://api.weixin.qq.com/sns/oauth2/access_token';
    public const GET_REFRESH_TOKEN_URL = 'https://api.weixin.qq.com/sns/oauth2/refresh_token';
    public const GET_OPENID_URL = '';
    public const GET_USER_INFO_URL = 'https://api.weixin.qq.com/sns/userinfo';
    public const GET_VERIFY_TOKEN_URL = 'https://api.weixin.qq.com/sns/auth';
    public const GET_UNION_INFO_URL = 'https://api.weixin.qq.com/cgi-bin/user/info';

    private static $instance;

    /**
     * 创建Facade实例。
     *
     * @param bool $origin
     *
     * @return static
     */
    public static function getInstance(bool $origin = false): self {
        if (!(self::$instance instanceof self) || $origin) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $redirect_uri
     * @param string $code
     * @param string $scope
     * @param string $state
     * @param string $lang
     *
     * @return array|string
     */

    public function authorize(string $redirect_uri, $code = '', $scope = 'snsapi_base', $state = '', $lang = 'zh-cn') {
        // 1、第一步：用户同意授权，获取code
        if (empty($code)) {
            return $this->getCode($redirect_uri, $scope ?: 'snsapi_base', $state);
        }

        //2、第二步：通过code换取网页授权access_token
        $token = $this->getAccessToken($code, $state)->toArray();
        if ($scope === 'snsapi_base') {
            // return $token;
        }

        //4、第四步：拉取用户信息(需scope为 snsapi_userinfo)
        return $this->getUserInfo($token['access_token'] ?? '', $token['openid'] ?? '', $lang)->toArray();
    }

    /**
     * 第一步：用户同意授权，获取code
     *
     * @explain      GET：{'code':'081PxY4C06i5ki2MNv5C0LCC4C0PxY4A','state':'5ae74940188ff277cb3ea5021d543ea0'}
     *
     * @param string $redirect_uri  授权后重定向的回调链接地址， 请使用 urlEncode 对链接进行处理
     * @param string $scope         应用授权作用域，snsapi_base （不弹出授权页面，直接跳转，只能获取用户openid），snsapi_userinfo （弹出授权页面，可通过openid拿到昵称、性别、所在地。
     *                              并且， 即使在未关注的情况下，只要用户授权，也能获取其信息 ）
     * @param string $state         重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
     *
     * @return \mark\response\Response
     */
    public function getCode(string $redirect_uri, $scope = 'snsapi_base', $state = ''): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($redirect_uri)) {
            return Response::create('', 412, '', '无效的redirect_uri');
        }
        if (empty($scope)) {
            $scope = 'snsapi_base';
        }

        switch ($scope) {
            default:
            case 'snsapi_base':
                $scope = 'snsapi_base';
                break;
            case 'snsapi_userinfo':
                $scope = 'snsapi_userinfo';
                break;
        }

        if (empty($state)) {
            $state = $this->generateState('md5', true);
        }

        $url = self::GET_AUTH_CODE_URL
            . '?response_type=code'
            . '&appid=' . $this->appid
            . '&redirect_uri=' . rawurlencode($redirect_uri)
            . '&scope=' . $scope
            . '&state=' . $state;

        if ($this->forcePopup) {
            $url .= '&forcePopup=true';
        }
        if ($this->forceSnapShot) {
            $url .= '&forceSnapShot=true';
        }
        $url .= '#wechat_redirect';
        // header('HTTP/1.1 301 Moved Permanently');
        // header('Location: ' . $url);

        return Response::create($url, 302);
    }

    /**
     * @var bool 用户弹窗确认
     */
    private $forcePopup = false;
    /**
     * @var bool 快照页逻辑判定
     */
    private $forceSnapShot = false;

    /**
     * 设置用户弹窗确认快照页逻辑判定
     *
     * @param false $forcePopup
     * @param false $forceSnapShot
     */
    public function setforce($forcePopup = false, $forceSnapShot = false) {
        $this->forcePopup = $forcePopup;
        $this->forceSnapShot = $forceSnapShot;
    }

    /**
     * 第二步：通过code换取网页授权access_token
     *
     * @explain JSON：
     * {
     * 'access_token':'ACCESS_TOKEN',
     * 'expires_in':7200,
     * 'refresh_token':'REFRESH_TOKEN',
     * 'openid':'OPENID',
     * 'scope':'SCOPE'
     * }
     * @link    https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html#2
     *
     * @param string $code  用于换取access_token的code，微信提供
     * @param string $state 重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
     *
     * @return \mark\response\Response
     */
    public function getAccessToken(string $code = '', string $state = ''): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($code)) {
            return Response::create('', 412, '', '缺少Code参数');
        }

        $cacheKey = 'auth:sso:wechat_media:access_token:appid:' . $this->appid . ':agent_md5:' . md5(Os::getAgent() . Os::getIpvs());

        if (!$this->checkState($state) && !empty($state)) {
            Authorize::getInstance()->getLogcat()->warning('OAuth.WeChat::getAccessToken(checkState)' . $state . '=>' . $this->getState());
            if (Authorize::getInstance()->isStrict()) {
                return Response::create('', 425, 'The state does not match. You may be a victim of CSRF', '请求存在跨域风险');
            }
        }

        if (empty($this->secret)) {
            return Response::create('', 412, '', '缺少AppSecret参数');
        }

        // $token_url = self::GET_ACCESS_TOKEN_URL . '?appid=' . $this->appid . '&secret=' . $this->secret . '&code=' . $code . '&grant_type=authorization_code';
        $token_url = 'https://api.weixin.qq.com/sns/oauth2/access_token'
            . '?appid=' . $this->appid
            . '&secret=' . $this->secret
            . '&code=' . $code
            . '&grant_type=authorization_code';

        return Client::getInstance()
            // ->setExpire(7000)
            // ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
            // ->setCacheKey($cacheKey)
            // ->setCache(Authorize::getInstance()->isCache())
                     ->setCache(false)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = self::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->get($token_url, 'json');
    }

    /**
     * 3、第三步：刷新access_token（如果需要）
     *
     * @explain JSON：
     *{
     * 'access_token':'ACCESS_TOKEN',
     * 'expires_in':7200,
     * 'refresh_token':'REFRESH_TOKEN',
     * 'openid':'OPENID',
     * 'scope':'SCOPE'
     * }
     *
     * @param string $refresh_token 填写通过access_token获取到的refresh_token参数
     *
     * @return \mark\response\Response
     * @link    https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html#2
     */
    public function refreshToken(string $refresh_token): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($refresh_token)) {
            return Response::create('', 412, '', '缺少RefreshToken参数');
        }

        $refresh_url = self::GET_REFRESH_TOKEN_URL . '?grant_type=refresh_token&appid=' . $this->appid . '&refresh_token=' . $refresh_token;
        return Client::getInstance()
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = self::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new ResponseException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         if (!empty($data['access_token'] ?? '') && !empty($data['refresh_token'] ?? '')) {
                             // 缓存AccessToken及RefreshToken
                         }

                         return $response;
                     })
                     ->get($refresh_url, 'json');
    }

    /**
     * @param string $code
     *
     * @return \mark\response\Response
     */
    public function getOpenID(string $code): Response {
        return self::getAccessToken($code);
    }

    /**
     * 4、第四步：拉取用户信息(需scope为 snsapi_userinfo)
     *
     * @param string $access_token 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
     * @param string $openid       用户的唯一标识
     * @param string $lang         返回国家地区语言版本，zh-cn 简体，zh-TW 繁体，en 英语
     *
     * @return \mark\response\Response
     * @link https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html#3
     */
    public function getUserInfo(string $access_token, string $openid, $lang = 'zh-cn'): Response {
        if (empty($access_token)) {
            $token = $this->getAccessToken()->toArray();
            if (!empty($token['access_token'] ?? '')) {
                $access_token = $token['access_token'];
            }
        }
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少Access_Token参数');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '缺少OpenID参数');
        }

        $userinfo_url = self::GET_USER_INFO_URL . '?access_token=' . $access_token . '&openid=' . $openid . '&lang=' . $lang;
        $cacheKey = 'auth:sso:wechat_media:userinfo:appid:' . $this->appid . ':openid:' . $openid . ':lang:' . $lang;
        return Client::getInstance()
            // ->setExpire(Authorize::getInstance()->getExpire())
            // ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
            // ->setCacheKey($cacheKey)
            // ->setCache(Authorize::getInstance()->isCache())
                     ->setCache(false)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = self::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->get($userinfo_url, 'json');
    }

    /**
     * 5、附：检验授权凭证（access_token）是否有效
     *{ 'errcode':0,'errmsg':'ok'}
     *
     * @param string $access_token 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
     * @param string $openid       用户的唯一标识
     *
     * @return \mark\response\Response
     * @link https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html#4
     */
    public function verifyToken(string $access_token, string $openid): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少AccessToken参数');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '缺少OpenID参数');
        }

        $verify_url = self::GET_VERIFY_TOKEN_URL . '?access_token=' . $access_token . '&openid=' . $openid;
        return Client::getInstance()
                     ->setCache(false)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (!isset($data['errcode']) || $data['errcode'] != 0) {
                             $response->setCode($data['errcode'] ?? 404);
                             $response->setResponse($data['errmsg'] ?? '');
                             $response->setResponseStatus($data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get($verify_url, 'json');
    }

    /**
     * 获取用户基本信息（包括UnionID机制）
     *
     * @param string $base_access_token
     * @param string $openid 普通用户的标识，对当前公众号唯一
     * @param string $lang   返回国家地区语言版本，zh-cn 简体，zh-TW 繁体，en 英语
     *
     * @return \mark\response\Response
     */
    public function getUnionInfo(string $base_access_token, string $openid, $lang = 'zh-cn'): Response {
        if (empty($base_access_token)) {
            // $token = $this->getAccessToken()->toArray();
            $token = $this->getBaseAccessToken()->toArray();
            if (empty($token['access_token'] ?? '')) {
                $token = $this->getBaseAccessToken(false)->toArray();
            }
            if (!empty($token['access_token'] ?? '')) {
                $base_access_token = $token['access_token'];
            }
        }
        if (empty($base_access_token)) {
            return Response::create('', 412, '', '缺少Base_Access_Token参数');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '缺少OpenID参数');
        }

        $union_url = self::GET_UNION_INFO_URL . '?access_token=' . $base_access_token . '&openid=' . $openid . '&lang=' . $lang;

        $cacheKey = 'auth:sso:wechat_media:unioninfo:appid:' . $this->appid . ':openid:' . $openid . ':lang:' . $lang;
        return Client::getInstance()
            // ->setExpire(Authorize::getInstance()->getExpire())
            // ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
            // ->setCacheKey($cacheKey)
            // ->setCache(Authorize::getInstance()->isCache())
                     ->setCache(false)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = self::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->get($union_url, 'json');
    }

    /**
     * 批量获取用户基本信息
     *
     * @param string $base_access_token
     * @param array  $openid
     * @param string $lang
     *
     * @return \mark\response\Response
     */
    public function batchget(string $base_access_token = '', array $openid = [], $lang = 'zh-cn'): Response {
        if (empty($base_access_token)) {
            // $token = $this->getAccessToken()->toArray();
            $token = $this->getBaseAccessToken()->toArray();

            if (empty($token['access_token'] ?? '')) {
                $token = $this->getBaseAccessToken(false)->toArray();
            }
            if (!empty($token['access_token'] ?? '')) {
                $base_access_token = $token['access_token'];
            }
        }
        if (empty($base_access_token)) {
            return Response::create('', 412, '', '缺少Base_Access_Token参数');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '缺少openid列表');
        }

        $batchget_url = 'https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=' . $base_access_token;

        $user_list = [];
        if (is_array($openid)) {
            foreach ($openid as $key => $item) {
                $user_list[] = ['openid' => $item, 'lang' => $lang];
            }
        }

        return Client::getInstance()
                     ->appendData('user_list', $user_list)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = self::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new ResponseException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->post($batchget_url, 'json');
    }

    /**
     * 获取微信基础AccessToken
     *
     * @param bool $cache
     *
     * @return \mark\response\Response
     */
    public function getBaseAccessToken(bool $cache = true): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($this->secret)) {
            return Response::create('', 412, '', '缺少AppSecret参数');
        }

        $cacheKey = 'auth:sso:wechat_media:base_access_token:appid:' . $this->appid;

        $token_url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $this->appid . '&secret=' . $this->secret;
        return Client::getInstance()
                     ->setExpire(7000)
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = self::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');

                             $client->setCache(false);
                             $client->clearCache();
                             // throw new ResponseException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         } else {
                             $data['expire_time'] = time() + 7000;
                             $data['expire_date'] = date('YmdHis', time() + 7000);
                             $response->setData($data);
                         }

                         return $response;
                     })
                     ->get($token_url, 'json');
    }

    /**
     * 获取微信基础AccessToken
     *
     * @param bool $cache
     *
     * @return \mark\response\Response
     */
    public function getStableAccessToken(bool $cache = true): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($this->secret)) {
            return Response::create('', 412, '', '缺少AppSecret参数');
        }
        $cacheKey = 'auth:sso:wechat_media:stable_access_token:appid:' . $this->appid;

        $token_url = 'https://api.weixin.qq.com/cgi-bin/stable_token';
        return Client::getInstance()
                     ->appendData('grant_type', 'client_credential')
                     ->appendData('appid', $this->appid)
                     ->appendData('secret', $this->secret)
                     ->appendData('force_refresh', !$cache)
                     ->setExpire(7000)
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = self::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');

                             $client->setCache(false);
                             $client->clearCache();
                             // throw new ResponseException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->post($token_url, 'json');
    }

    /**
     * 临时二维码请求说明
     * http请求方式: POST URL: https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=TOKEN
     * POST数据格式：json POST数据例子：{"expire_seconds": 604800, "action_name": "QR_SCENE", "action_info": {"scene": {"scene_id": 123}}}
     * 或者也可以使用以下POST数据创建字符串形式的二维码参数：{"expire_seconds": 604800, "action_name": "QR_STR_SCENE", "action_info": {"scene": {"scene_str": "test"}}}
     * 永久二维码请求说明
     * http请求方式: POST URL: https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=TOKEN
     * POST数据格式：json POST数据例子：{"action_name": "QR_LIMIT_SCENE", "action_info": {"scene": {"scene_id": 123}}}
     * 或者也可以使用以下POST数据创建字符串形式的二维码参数： {"action_name": "QR_LIMIT_STR_SCENE", "action_info": {"scene": {"scene_str": "test"}}}
     *
     * @param string $access_token
     * @param array  $options
     *
     * @return \mark\response\Response {'ticket':'gQH47joAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL2taZ2Z3TVRtNzJXV1Brb3ZhYmJJAAIEZ23sUwMEmm3sUw==','expire_seconds':60,'url':'https://weixin.qq.com/q/kZgfwMTm72WWPkovabbI'}
     * @link https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=TOKEN
     * @link https://developers.weixin.qq.com/doc/offiaccount/Account_Management/Generating_a_Parametric_QR_Code.html
     */
    public function getQRTicket(string $access_token = '', array $options = array()): Response {
        if (empty($access_token)) {
            $token = $this->getBaseAccessToken()->toArray();
            if (!empty($token) && !empty($token['access_token'] ?? '')) {
                $access_token = $token['access_token'] ?: '';
            }
        }

        if (empty($access_token)) {
            $token = $this->getBaseAccessToken(false)->toArray();
            if (!empty($token) && !empty($token['access_token'] ?? '')) {
                $access_token = $token['access_token'] ?: '';
            }
        }

        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少Access_Token参数');
        }

        $post = array();
        switch ($options['action_name'] ?? '') {
            case 'QR_SCENE': // 临时的整型参数值
                $post['action_name'] = 'QR_SCENE';
                break;
            case 'QR_STR_SCENE': // 临时的字符串参数值
                $post['action_name'] = 'QR_STR_SCENE';
                break;
            case 'QR_LIMIT_SCENE': // 永久的整型参数值
                $post['action_name'] = 'QR_LIMIT_SCENE';
                break;
            case 'QR_LIMIT_STR_SCENE': // 永久的字符串参数值
                $post['action_name'] = 'QR_LIMIT_STR_SCENE';
                break;
        }

        if (isset($options['expire_seconds'])) {
            $post['expire_seconds'] = $options['expire_seconds'];
        }

        if (isset($options['scene_id'])) {
            $post['scene']['scene_id'] = $options['scene_id'];
        }

        if (isset($options['scene_str'])) {
            $post['scene']['scene_str'] = $options['scene_str'];
        }

        return Client::getInstance()
                     ->append($post)
                     ->setCallback(function (Response $response, Client $client) use ($options) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             if (($data['errcode'] ?? '') == 40001) {
                                 // invalid credential, access_token is invalid or not latest
                                 $token = $this->getBaseAccessToken(false)->toArray();
                                 if (!empty($token) && !empty($token['access_token'] ?? '')) {
                                     return self::getQRTicket($token['access_token'] ?? '', $options);
                                 }
                             }

                             $resp = self::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');

                             $client->setCache(false);
                             $client->clearCache();
                             // throw new ResponseException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         if (empty($data['ticket'] ?? '')) {
                             // throw new OAuthException('无效的ticket', 404);
                         }

                         return $response;
                     })
                     ->post('https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=' . $access_token, 'json');
    }

    /**
     * 为了满足用户渠道推广分析和用户账号绑定等场景的需要，公众平台提供了生成带参数二维码的接口。使用该接口可以获得多个带不同场景值的二维码，用户扫描后，公众号可以接收到事件推送。
     *
     * @link https://developers.weixin.qq.com/doc/offiaccount/Account_Management/Generating_a_Parametric_QR_Code.html
     *
     * @param string $ticket
     *
     * @return \mark\response\Response
     */
    public function getQRCode(string $ticket): Response {
        if (empty($ticket)) {
            return Response::create('', 412, '', '无效的ticket参数');
        }

        return Client::getInstance()
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = self::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');

                             $client->setCache(false);
                             $client->clearCache();
                             // throw new ResponseException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->get('https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=' . urlencode($ticket), 'json');
    }

    /** JSSDK **/

    /**
     * 获取SignPackage
     *
     * @param string $url
     * @param string $ticket
     *
     * @return \mark\response\Response
     */
    public function getSignPackage(string $url = '', $ticket = ''): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        if (empty($ticket)) {
            $jsapi_ticket = $this->getJsApiTicket()->toArray();
            if (!empty($jsapi_ticket['jsapi_ticket'] ?? '')) {
                $ticket = $jsapi_ticket['jsapi_ticket'] ?: '';
            }
        }

        if (empty($ticket)) {
            return Response::create('', 412, '', '缺少ticket参数');
        }

        if (empty($url)) {
            // 注意 URL 一定要动态获取，不能 hardcode.
            $protocol = (($_SERVER['HTTPS'] ?? '') !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
            $url = $protocol . ($_SERVER['HTTP_HOST'] ?? '') . ($_SERVER['REQUEST_URI'] ?? '');
        }

        $timestamp = time();
        $nonceStr = $this->createNonceStr();

        // 这里参数的顺序要按照 key 值 ASCII 码升序排序
        $string = 'jsapi_ticket=' . $ticket . '&noncestr=' . $nonceStr . '&timestamp=' . $timestamp . '&url=' . $url;
        $signature = sha1($string);

        return Response::create(array('appId' => $this->appid,
                                      'nonceStr' => $nonceStr,
                                      'timestamp' => $timestamp,
                                      'url' => $url,
                                      'signature' => $signature,
                                      'rawString' => $string));
    }

    /**
     * 生成签名的随机串
     *
     * @param int $length
     *
     * @return string
     */
    private function createNonceStr($length = 16): string {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            try {
                $str .= $chars[random_int(0, strlen($chars) - 1)];
            } catch (\Exception $e) {
                $str .= $chars[rand(0, strlen($chars) - 1)];
            }
        }

        return $str;
    }

    /**
     * 获取JsApiTicket
     *
     * @param string $access_token
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public function getJsApiTicket($access_token = '', $cache = true): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        if (empty($access_token)) {
            $token = $this->getBaseAccessToken($cache)->toArray();
            if (empty($token['access_token'] ?? '')) {
                $token = $this->getBaseAccessToken(false)->toArray();
            }

            if (!empty($token['access_token'] ?? '')) {
                $access_token = $token['access_token'] ?? '';
            }
        }

        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少Access_Token参数');
        }

        $cacheKey = 'auth:sso:wechat_media:jsapi:ticket:appid:' . $this->appid;
        // 如果是企业号用以下 URL 获取 ticket
        // $url = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=$accessToken";
        $ticket_url = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=' . $access_token;

        return Client::getInstance()
                     ->setExpire(7000)
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache(true)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = self::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');

                             $client->setCache(false);
                             $client->clearCache();
                         } elseif (empty($data['ticket'] ?? '')) {
                             $response->setResponseCode(412, false);
                             $response->setResponseStatus('Invalid ticket info');
                             $response->setResponseReason('无效的ticket信息');

                             $client->setCache(false);
                             $client->clearCache();
                         } else {
                             $data['expire_time'] = time() + 7000;
                             $data['expire_date'] = date('YmdHis', time() + 7000);
                             $data['jsapi_ticket'] = $data['ticket'] ?? '';

                             $response->setData($data);
                         }

                         return $response;
                     })
                     ->get($ticket_url, 'json');
    }

    /**
     * 发送模板消息
     *
     * @param array  $template
     * @param string $access_token
     *
     * @return \mark\response\Response
     */
    public function sendMessage(array $template, $access_token = ''): Response {
        if (empty($access_token)) {
            $token = $this->getBaseAccessToken()->toArray();
            if (empty($token['access_token'] ?? '')) {
                $token = $this->getBaseAccessToken(false)->toArray();
            }

            if (!empty($token['access_token'] ?? '')) {
                $access_token = $token['access_token'] ?: '';
            }
        }

        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少Access_Token参数');
        }

        $msg_url = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=' . $access_token;
        return Client::getInstance()
                     ->append($template)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = self::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');

                             $client->setCache(false);
                             $client->clearCache();
                             // throw new ResponseException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->post($msg_url, 'json');
    }

    /**
     * 使用AppSecret重置 API 调用次数 * clearQuotaByAppSecret
     *
     * @link https://developers.weixin.qq.com/doc/offiaccount/openApi/clearQuotaByAppSecret.html
     * @return \mark\response\Response
     */
    public function clearQuota(): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($this->secret)) {
            return Response::create('', 412, '', '缺少AppSecret参数');
        }

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('secret', $this->secret)
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['errcode']) && $data['errcode'] != 0) {
                             $resp = self::ResponseCode($data['errcode'] ?? 404);
                             $response->setResponseCode($resp['code'] ?? $data['errcode'] ?? 404, false);
                             $response->setResponseStatus($resp['status'] ?? $data['errmsg'] ?? '');
                             $response->setResponseReason($resp['reason'] ?? $data['errmsg'] ?? '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new ResponseException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->post('https://api.weixin.qq.com/cgi-bin/clear_quota/v2?appid=' . $this->appid . '&appsecret=' . $this->secret, 'json');
    }

    /**
     * 微信响应代码列表
     *
     * @var array[]
     */
    public static $response_codes = [
        ['code' => 413, 'errcode' => -1, 'status' => 'System busy', 'reason' => '系统繁忙，请稍候再试', 'explain' => '系统繁忙，此时请开发者稍候再试'],
        ['code' => 200, 'errcode' => 0, 'status' => 'OK', 'reason' => '请求成功', 'explain' => '请求成功'],
        ['code' => 412, 'errcode' => 1003, 'status' => '', 'reason' => '非法的post参数', 'explain' => 'post参数非法'],
        ['code' => 404, 'errcode' => 20002, 'status' => '', 'reason' => '商品 id 不存在', 'explain' => '商品 id 不存在'],
        ['code' => 412, 'errcode' => 40001, 'status' => 'invalid credential, access_token is invalid or not latest', 'reason' => '无效的Access_Token',
         'explain' => '获取 access_token 时 AppSecret 错误，或者 access_token 无效。请开发者认真比对 AppSecret 的正确性，或查看是否正在为恰当的公众号调用接口'],
        ['code' => 406, 'errcode' => 40002, 'status' => 'invalid grant_type', 'reason' => '非法的凭证类型', 'explain' => '不合法的凭证类型'],
        ['code' => 412, 'errcode' => 40003, 'status' => 'invalid openid', 'reason' => '不合法的 openid', 'explain' => '不合法的 openid ，请开发者确认 openid （该用户）是否已关注公众号，或是否是其他公众号的 openid'],
        ['code' => 412, 'errcode' => 40004, 'status' => 'invalid media file type', 'reason' => '非法的媒体文件类型', 'explain' => '不合法的媒体文件类型'],
        ['code' => 412, 'errcode' => 40005, 'status' => 'invalid file type', 'reason' => '非法的文件类型', 'explain' => '上传素材文件格式不对|不合法的文件类型'],
        ['code' => 413, 'errcode' => 40006, 'status' => 'invalid meida size', 'reason' => '文件大小超出限制', 'explain' => '上传素材文件大小超出限制|不合法的文件大小'],
        ['code' => 412, 'errcode' => 40007, 'status' => 'invalid media_id', 'reason' => '非法的媒体文件 id', 'explain' => '不合法的媒体文件 id'],
        ['code' => 412, 'errcode' => 40008, 'status' => 'invalid message type', 'reason' => '非法的消息类型', 'explain' => '不合法的消息类型'],
        ['code' => 413, 'errcode' => 40009, 'status' => 'invalid image size', 'reason' => '图片尺寸太大', 'explain' => '图片尺寸太大|不合法的图片文件大小'],
        ['code' => 412, 'errcode' => 40010, 'status' => 'invalid voice size', 'reason' => '不合法的语音文件大小', 'explain' => '不合法的语音文件大小'],
        ['code' => 412, 'errcode' => 40011, 'status' => 'invalid video size', 'reason' => '不合法的视频文件大小', 'explain' => '不合法的视频文件大小'],
        ['code' => 412, 'errcode' => 40012, 'status' => 'invalid thumb size', 'reason' => '不合法的缩略图文件大小', 'explain' => '不合法的缩略图文件大小'],
        ['code' => 412, 'errcode' => 40013, 'status' => 'invalid appid', 'reason' => '非法的APPID', 'explain' => '不合法的 appid ，请开发者检查 appid 的正确性，避免异常字符，注意大小写'],
        ['code' => 412, 'errcode' => 40014, 'status' => 'invalid access_token', 'reason' => '非法的access_token',
         'explain' => '不合法的 access_token ，请开发者认真比对 access_token 的有效性（如是否过期），或查看是否正在为恰当的公众号调用接口'],
        ['code' => 412, 'errcode' => 40015, 'status' => 'invalid menu type', 'reason' => '不合法的菜单类型', 'explain' => '不合法的菜单类型'],
        ['code' => 412, 'errcode' => 40016, 'status' => 'invalid button size', 'reason' => '不合法的按钮个数', 'explain' => '不合法的按钮个数'],
        ['code' => 412, 'errcode' => 40017, 'status' => 'invalid button type', 'reason' => '不合法的按钮类型', 'explain' => '不合法的按钮类型'],
        ['code' => 412, 'errcode' => 40018, 'status' => 'invalid button name size', 'reason' => '不合法的按钮名字长度', 'explain' => '不合法的按钮名字长度'],
        ['code' => 412, 'errcode' => 40019, 'status' => 'invalid button key size', 'reason' => '不合法的按钮 key 长度', 'explain' => '不合法的按钮 key 长度'],
        ['code' => 412, 'errcode' => 40020, 'status' => 'invalid button URL length', 'reason' => '不合法的按钮 url 长度', 'explain' => '不合法的按钮 url 长度'],
        ['code' => 412, 'errcode' => 40021, 'status' => 'invalid menu version', 'reason' => '不合法的菜单版本号', 'explain' => '不合法的菜单版本号'],
        ['code' => 412, 'errcode' => 40022, 'status' => 'invalid sub_menu level', 'reason' => '非法的子菜单级数', 'explain' => '不合法的子菜单级数'],
        ['code' => 412, 'errcode' => 40023, 'status' => 'invalid sub button size', 'reason' => '非法的子菜单按钮个数', 'explain' => '不合法的子菜单按钮个数'],
        ['code' => 412, 'errcode' => 40024, 'status' => 'invalid sub button type', 'reason' => '非法的子菜单按钮类型', 'explain' => '不合法的子菜单按钮类型'],
        ['code' => 412, 'errcode' => 40025, 'status' => 'invalid sub button name size', 'reason' => '不合法的子菜单按钮名字长度', 'explain' => '不合法的子菜单按钮名字长度'],
        ['code' => 412, 'errcode' => 40026, 'status' => 'invalid sub button key size', 'reason' => '非法的子菜单按钮 KEY 长度', 'explain' => '不合法的子菜单按钮 key 长度'],
        ['code' => 412, 'errcode' => 40027, 'status' => 'invalid sub button url length', 'reason' => '非法的子菜单按钮 url 长度', 'explain' => '不合法的子菜单按钮 url 长度'],
        ['code' => 412, 'errcode' => 40028, 'status' => 'invalid menu api user', 'reason' => '非法的自定义菜单使用用户', 'explain' => '不合法的自定义菜单使用用户'],
        ['code' => 412, 'errcode' => 40029, 'status' => 'invalid oauth code', 'reason' => '无效的 oauth_code', 'explain' => '无效的 oauth_code'],
        ['code' => 412, 'errcode' => 40030, 'status' => 'invalid refresh_token', 'reason' => '无效的 refresh_token', 'explain' => '不合法的 refresh_token'],
        ['code' => 412, 'errcode' => 40031, 'status' => 'invalid openid list', 'reason' => '非法的 openid 列表', 'explain' => '不合法的 openid 列表'],
        ['code' => 412, 'errcode' => 40032, 'status' => 'invalid openid list size', 'reason' => '非法的 openid 列表长度', 'explain' => '不合法的 openid 列表长度'],
        ['code' => 412, 'errcode' => 40033, 'status' => 'invalid charset. please check your request, if include \uxxxx will create fail!', 'reason' => '非法的请求字符',
         'explain' => '不合法的请求字符，不能包含 \uxxxx 格式的字符'],
        ['code' => 412, 'errcode' => 40034, 'status' => 'invalid template size', 'reason' => '无效的模板大小', 'explain' => ''],
        ['code' => 412, 'errcode' => 40035, 'status' => 'invalid args size', 'reason' => '非法的参数', 'explain' => '不合法的参数'],
        ['code' => 412, 'errcode' => 40036, 'status' => 'invalid template_id size', 'reason' => '不合法的 template_id 长度', 'explain' => '不合法的 template_id 长度'],
        ['code' => 412, 'errcode' => 40037, 'status' => 'invalid template_id', 'reason' => '不合法的 template_id', 'explain' => '不合法的 template_id'],
        ['code' => 412, 'errcode' => 40038, 'status' => 'invalid request format', 'reason' => '非法的请求格式', 'explain' => '不合法的请求格式'],
        ['code' => 412, 'errcode' => 40039, 'status' => 'Bad URL length', 'reason' => '非法的 URL 长度', 'explain' => '不合法的 url 长度'],
        ['code' => 412, 'errcode' => 40040, 'status' => 'invalid plugin token', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40041, 'status' => 'invalid plugin id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40042, 'status' => 'invalid plugin session', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40043, 'status' => 'invalid fav type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40044, 'status' => 'invalid size in link.title', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40045, 'status' => 'invalid size in link.description', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40046, 'status' => 'invalid size in link.iconurl', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40047, 'status' => 'invalid size in link.url', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40048, 'status' => 'Invalid URL', 'reason' => '无效的url', 'explain' => ''],
        ['code' => 412, 'errcode' => 40049, 'status' => 'invalid score report type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40050, 'status' => 'Bad group id', 'reason' => '非法的分组 id', 'explain' => ''],
        ['code' => 412, 'errcode' => 40051, 'status' => 'Bad group name', 'reason' => '非法的分组名字', 'explain' => ''],
        ['code' => 412, 'errcode' => 40052, 'status' => 'invalid action name', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40053, 'status' => 'invalid action info, please check document', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40054, 'status' => 'invalid sub button url domain', 'reason' => '非法的子菜单按钮 url 域名', 'explain' => '不合法的子菜单按钮 url 域名'],
        ['code' => 412, 'errcode' => 40055, 'status' => 'invalid button url domain', 'reason' => '非法的菜单按钮 url 域名', 'explain' => '不合法的菜单按钮 url 域名'],
        ['code' => 412, 'errcode' => 40056, 'status' => 'invalid serial code', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40057, 'status' => 'invalid tabbar size', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40058, 'status' => 'invalid tabbar name size', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40059, 'status' => 'invalid msg id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40060, 'status' => 'invalid article idx', 'reason' => '非法的article_idx', 'explain' => '删除单篇图文时，指定的 article_idx 不合法'],
        ['code' => 412, 'errcode' => 40062, 'status' => 'invalid title size', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40063, 'status' => 'invalid message_ext size', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40064, 'status' => 'invalid app type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40065, 'status' => 'invalid msg status', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40066, 'status' => 'invalid url', 'reason' => '不合法的 url', 'explain' => '不合法的 url ，递交的页面被 sitemap 标记为拦截'],
        ['code' => 412, 'errcode' => 40067, 'status' => 'invalid tvid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40068, 'status' => 'contain mailcious url', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40069, 'status' => 'invalid hardware type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40070, 'status' => 'invalid sku info', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40071, 'status' => 'invalid card type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40072, 'status' => 'invalid location id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40073, 'status' => 'invalid card id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40074, 'status' => 'invalid pay template id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40075, 'status' => 'invalid encrypt code', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40076, 'status' => 'invalid color id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40077, 'status' => 'invalid score type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40078, 'status' => 'invalid card status', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40079, 'status' => 'invalid time', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40080, 'status' => 'invalid card ext', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40081, 'status' => 'invalid template_id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40082, 'status' => 'invalid banner picture size', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40083, 'status' => 'invalid banner url size', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40084, 'status' => 'invalid button desc size', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40085, 'status' => 'invalid button url size', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40086, 'status' => 'invalid sharelink logo size', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40087, 'status' => 'invalid sharelink desc size', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40088, 'status' => 'invalid sharelink title size', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40089, 'status' => 'invalid platform id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40090, 'status' => 'invalid request source (bad client ip)', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40091, 'status' => 'invalid component ticket', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40092, 'status' => 'invalid remark name', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40093, 'status' => 'not completely ok, err_item will return location_id=-1,check your required_fields in json.',
         'reason' => '请在json中检查您需要的字段', 'explain' => '请在json中检查您需要的字段'],
        ['code' => 412, 'errcode' => 40094, 'status' => 'invalid component credential', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40095, 'status' => 'bad source of caller', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40096, 'status' => 'invalid biztype', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40097, 'status' => 'invalid args', 'reason' => '参数错误', 'explain' => '参数错误'],
        ['code' => 412, 'errcode' => 40098, 'status' => 'invalid poiid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40099, 'status' => 'invalid code, this code has consumed.', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40100, 'status' => 'invalid dateinfo', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40101, 'status' => 'missing parameter', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40102, 'status' => 'invalid industry id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40103, 'status' => 'invalid industry index', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40104, 'status' => 'invalid category id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40105, 'status' => 'invalid view type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40106, 'status' => 'invalid user name', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40107, 'status' => 'invalid card id! 1,card status must verify ok; 2,this card must have location_id', 'reason' => '无效的卡片ID',
         'explain' => '无效的卡片ID'],
        ['code' => 412, 'errcode' => 40108, 'status' => 'invalid client version', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40109, 'status' => 'too many code size, must <= 100', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40110, 'status' => 'have empty code', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40111, 'status' => 'have same code', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40112, 'status' => 'can not set bind openid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40113, 'status' => 'unsupported file type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40114, 'status' => 'invalid index value', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40115, 'status' => 'invalid session from', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40116, 'status' => 'invalid code', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40117, 'status' => 'invalid button media_id size', 'reason' => '分组名字不合法', 'explain' => '分组名字不合法'],
        ['code' => 412, 'errcode' => 40118, 'status' => 'invalid sub button media_id size', 'reason' => '非法的media_id 大小', 'explain' => 'media_id 大小不合法'],
        ['code' => 412, 'errcode' => 40119, 'status' => 'invalid use button type', 'reason' => 'button 类型错误', 'explain' => 'button 类型错误'],
        ['code' => 412, 'errcode' => 40120, 'status' => 'invalid use sub button type', 'reason' => '子 button 类型错误', 'explain' => '子 button 类型错误'],
        ['code' => 412, 'errcode' => 40121, 'status' => 'invalid media type in view_limited', 'reason' => '非法的 media_id 类型', 'explain' => '不合法的 media_id 类型'],
        ['code' => 412, 'errcode' => 40122, 'status' => 'invalid card quantity', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40123, 'status' => 'invalid task_id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40124, 'status' => 'too many custom field!', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40125, 'status' => 'invalid appsecret', 'reason' => '无效的AppSecret', 'explain' => '不合法的 appid ，请开发者检查 appid 的正确性，避免异常字符，注意大小写'],
        ['code' => 412, 'errcode' => 40126, 'status' => 'invalid text size', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40127,
         'status' => 'invalid user-card status! hint: the card was given to user, but may be deleted or expired or set unavailable !',
         'reason' => '无效的用户卡片状态', 'explain' => '无效的用户卡片状态'],
        ['code' => 412, 'errcode' => 40128, 'status' => 'invalid media id! must be uploaded by api(cgi-bin/material/add_material)', 'reason' => '无效的媒体ID', 'explain' => '无效的媒体ID'],
        ['code' => 412, 'errcode' => 40129, 'status' => 'invalid scene', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40130, 'status' => 'invalid openid list size, at least two openid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40131, 'status' => 'out of limit of ticket', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40132, 'status' => 'invalid username', 'reason' => '非法的微信号', 'explain' => '微信号不合法'],
        ['code' => 412, 'errcode' => 40133, 'status' => 'invalid encryt data', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40135, 'status' => 'invalid not supply bonus, can not change card_id which supply bonus to be not supply', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40136, 'status' => 'invalid use depositcodemode, make sure sku.quantity>depositcode.quantity', 'reason' => '无效的存款代码模式',
         'explain' => '无效的存款代码模式'],
        ['code' => 412, 'errcode' => 40137, 'status' => 'invalid image format', 'reason' => '不支持的图片格式', 'explain' => '不支持的图片格式'],
        ['code' => 400, 'errcode' => 40138, 'status' => 'emphasis word can not be first neither remark', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40139, 'status' => 'invalid sub merchant id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40140, 'status' => 'invalid sub merchant status', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40141, 'status' => 'invalid image url', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40142, 'status' => 'invalid sharecard parameters', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40143, 'status' => 'invalid least cost info, should be 0', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40144,
         'status' => '1、maybe share_card_list.num or consume_share_self_num too big; 2、maybe card_id_list also has self-card_id；3、maybe card_id_list has many different card_id;4)maybe both consume_share_self_num and share_card_list.num bigger than 0',
         'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40145, 'status' => 'invalid update! can not both set paycell and centercellinfo(include: center_title, center_sub_title, center_url).',
         'reason' => '无效的更新', 'explain' => '无效的更新'],
        ['code' => 412, 'errcode' => 40146, 'status' => 'invalid openid! card may be marked by other user!', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40147, 'status' => 'invalid consume! consume time overranging restricts.', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40148, 'status' => 'invalid friends card type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40149, 'status' => 'invalid use time limit', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40150, 'status' => 'invalid card parameters', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40151, 'status' => 'invalid card info, text/pic hit antispam', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40152, 'status' => 'invalid group id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40153, 'status' => 'self consume cell for friends card must need verify code', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40154, 'status' => 'invalid voip parameters', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40155, 'status' => 'please dont contain other home page url', 'reason' => '请勿添加其他公众号的主页链接', 'explain' => '请勿添加其他公众号的主页链接'],
        ['code' => 412, 'errcode' => 40156, 'status' => 'invalid face recognize parameters', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40157, 'status' => 'invalid picture, has no face', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40158, 'status' => 'invalid use_custom_code, need be false', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40159, 'status' => 'invalid length for path, or the data is not json string', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40160, 'status' => 'invalid image file', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40161, 'status' => 'image file not match', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40162, 'status' => 'invalid lifespan', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40163, 'status' => 'code been used', 'reason' => 'oauth_code已使用', 'explain' => 'oauth_code已使用'],
        ['code' => 412, 'errcode' => 40164, 'status' => 'invalid ip, not in whitelist', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40165, 'status' => 'invalid weapp pagepath', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40166, 'status' => 'invalid weapp appid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40167, 'status' => 'there is no relation with plugin appid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40168, 'status' => 'unlinked weapp card', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40169, 'status' => 'invalid length for scene, or the data is not json string', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40170, 'status' => 'args count exceed count limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40171, 'status' => 'product id can not empty and the length cannot exceed 32', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40172, 'status' => 'can not have same product id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40173, 'status' => 'there is no bind relation', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40174, 'status' => 'not card user', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40175, 'status' => 'invalid material id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40176, 'status' => 'invalid template id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40177, 'status' => 'invalid product id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40178, 'status' => 'invalid sign', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40179, 'status' => 'function is adjusted, rules are not allowed to add or update', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40180, 'status' => 'invalid client tmp token', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40181, 'status' => 'invalid opengid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40182, 'status' => 'invalid pack_id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40183, 'status' => 'invalid product_appid, product_appid should bind with wxa_appid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40184, 'status' => 'invalid url path', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40185, 'status' => 'invalid auth_token, or auth_token is expired', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40186, 'status' => 'invalid delegate', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40187, 'status' => 'invalid ip', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40188, 'status' => 'invalid scope', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40189, 'status' => 'invalid width', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40190, 'status' => 'invalid delegate time', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40191, 'status' => 'invalid pic_url', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40192, 'status' => 'invalid author in news', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40193, 'status' => 'invalid recommend length', 'reason' => '', 'explain' => ''],
        ['code' => 405, 'errcode' => 40194, 'status' => 'illegal recommend', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40195, 'status' => 'invalid show_num', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40196, 'status' => 'invalid smartmsg media_id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40197, 'status' => 'invalid smartmsg media num', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40198, 'status' => 'invalid default msg article size, must be same as show_num', 'reason' => '', 'explain' => ''],
        ['code' => 404, 'errcode' => 40199, 'status' => 'waybill_id not found', 'reason' => '运单 id 不存在，未查到运单', 'explain' => '运单 id 不存在，未查到运单'],
        ['code' => 412, 'errcode' => 40200, 'status' => 'invalid account type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40201, 'status' => 'invalid check url', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40202, 'status' => 'invalid check action', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40203, 'status' => 'invalid check operator', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 40204, 'status' => 'can not delete wash or rumor article', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40205, 'status' => 'invalid check keywords string', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40206, 'status' => 'invalid check begin stamp', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40207, 'status' => 'invalid check alive seconds', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40208, 'status' => 'invalid check notify id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40209, 'status' => 'invalid check notify msg', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40210, 'status' => 'invalid check wxa path', 'reason' => 'pages 中的 path 参数不存在或为空', 'explain' => 'pages 中的 path 参数不存在或为空'],
        ['code' => 412, 'errcode' => 40211, 'status' => 'invalid scope_data', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40212, 'status' => 'invalid query', 'reason' => 'paegs 当中存在不合法的query', 'explain' => 'paegs 当中存在不合法的query，query格式遵循 url 标准，即k1=v1&k2=v2'],
        ['code' => 412, 'errcode' => 40213, 'status' => 'invalid href tag', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40214, 'status' => 'invalid href text', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40215, 'status' => 'invalid image count', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40216, 'status' => 'invalid desc', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40217, 'status' => 'invalid video count', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40218, 'status' => 'invalid video id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40219, 'status' => 'pages is empty', 'reason' => 'pages不存在或者参数为空', 'explain' => 'pages不存在或者参数为空'],
        ['code' => 412, 'errcode' => 40220, 'status' => 'data_list is empty', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40221, 'status' => 'invalid content-encoding', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 40222, 'status' => 'invalid request idc domain', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41001, 'status' => 'access_token missing', 'reason' => '缺少 access_token 参数', 'explain' => '缺少 access_token 参数'],
        ['code' => 412, 'errcode' => 41002, 'status' => 'appid missing', 'reason' => '缺少 appid 参数', 'explain' => '缺少 appid 参数'],
        ['code' => 412, 'errcode' => 41003, 'status' => 'refresh_token missing', 'reason' => '缺少 refresh_token 参数', 'explain' => '缺少 refresh_token 参数'],
        ['code' => 412, 'errcode' => 41004, 'status' => 'appsecret missing', 'reason' => '缺少 secret 参数', 'explain' => '缺少 secret 参数'],
        ['code' => 412, 'errcode' => 41005, 'status' => 'media data missing', 'reason' => '缺少多媒体文件数据', 'explain' => '缺少多媒体文件数据，传输素材无视频或图片内容'],
        ['code' => 412, 'errcode' => 41006, 'status' => 'media_id missing', 'reason' => '缺少 media_id 参数', 'explain' => '缺少 media_id 参数'],
        ['code' => 412, 'errcode' => 41007, 'status' => 'sub_menu data missing', 'reason' => '缺少子菜单数据', 'explain' => '缺少子菜单数据'],
        ['code' => 412, 'errcode' => 41008, 'status' => 'missing code', 'reason' => '缺少 oauth code', 'explain' => '缺少 oauth code'],
        ['code' => 412, 'errcode' => 41009, 'status' => 'missing openid', 'reason' => '缺少 openid', 'explain' => '缺少 openid'],
        ['code' => 412, 'errcode' => 41010, 'status' => 'missing url', 'reason' => '缺失 url 参数', 'explain' => '缺失 url 参数'],
        ['code' => 412, 'errcode' => 41011, 'status' => 'missing required fields! please check document and request json!', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41012, 'status' => 'missing card id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41013, 'status' => 'missing code', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41014, 'status' => 'missing ticket_class', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41015, 'status' => 'missing show_time', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41016, 'status' => 'missing screening_room', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41017, 'status' => 'missing seat_number', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41018, 'status' => 'missing component_appid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41019, 'status' => 'missing platform_secret', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41020, 'status' => 'missing platform_ticket', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41021, 'status' => 'missing component_access_token', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41024, 'status' => 'missing display field', 'reason' => '需要display字段', 'explain' => '需要display字段'],
        ['code' => 412, 'errcode' => 41025, 'status' => 'poi_list empty', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41026, 'status' => 'missing image list info, text maybe empty', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41027, 'status' => 'missing voip call key', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41028, 'status' => 'invalid form id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 41029, 'status' => 'form id used count reach limit', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 41030, 'status' => 'invalid page', 'reason' => 'page路径不正确', 'explain' => 'page路径不正确，需要保证在现网版本小程序中存在，与 app.json 保持一致'],
        ['code' => 400, 'errcode' => 41031, 'status' => 'the form id have been blocked!', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 41032, 'status' => 'not allow to send message with submitted form id, for punishment', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 41033, 'status' => 'invaid register type', 'reason' => '只允许通过 api 创建的小程序使用', 'explain' => '只允许通过 api 创建的小程序使用'],
        ['code' => 400, 'errcode' => 41034, 'status' => 'not allow to send message with submitted form id, for punishment', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 41035, 'status' => 'not allow to send message with prepay id, for punishment', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 41036, 'status' => 'appid ad cid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 41037, 'status' => 'appid ad_mch_appid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 41038, 'status' => 'appid pos_type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 42001, 'status' => 'access_token expired', 'reason' => 'access_token 已超时',
         'explain' => 'access_token 超时，请检查 access_token 的有效期，请参考基础支持 - 获取 access_token 中，对 access_token 的详细机制说明'],
        ['code' => 412, 'errcode' => 42002, 'status' => 'refresh_token expired', 'reason' => 'refresh_token 超时', 'explain' => 'refresh_token 超时'],
        ['code' => 412, 'errcode' => 42003, 'status' => 'oauth code expired', 'reason' => 'oauth_code 超时', 'explain' => 'oauth_code 超时'],
        ['code' => 400, 'errcode' => 42004, 'status' => 'plugin token expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 42005, 'status' => 'api usage expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 42006, 'status' => 'component_access_token expired', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 42007, 'status' => 'Re authorization required', 'reason' => '请重新授权', 'explain' => '用户修改微信密码， accesstoken 和 refreshtoken 失效，需要重新授权'],
        ['code' => 400, 'errcode' => 42008, 'status' => 'voip call key expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 42009, 'status' => 'client tmp token expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 42010, 'status' => '', 'reason' => '相同 media_id 群发过快，请重试', 'explain' => '相同 media_id 群发过快，请重试'],
        ['code' => 405, 'errcode' => 43001, 'status' => 'require get method', 'reason' => '需要 get 请求', 'explain' => '需要 get 请求'],
        ['code' => 405, 'errcode' => 43002, 'status' => 'require post method', 'reason' => '需要 post 请求', 'explain' => '需要 post 请求'],
        ['code' => 426, 'errcode' => 43003, 'status' => 'require https', 'reason' => '需要 https 请求', 'explain' => '需要 https 请求'],
        ['code' => 412, 'errcode' => 43004, 'status' => 'require subscribe', 'reason' => '需要关注公众号', 'explain' => '需要接收者关注'],
        ['code' => 412, 'errcode' => 43005, 'status' => 'require friend relations', 'reason' => '需要好友关系', 'explain' => '需要好友关系'],
        ['code' => 400, 'errcode' => 43006, 'status' => 'require not block', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43007, 'status' => 'require bizuser authorize', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43008, 'status' => 'require biz pay auth', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43009, 'status' => 'can not use custom code, need authorize', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43010, 'status' => 'can not use balance, need authorize', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43011, 'status' => 'can not use bonus, need authorize', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43012, 'status' => 'can not use custom url, need authorize', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43013, 'status' => 'can not use shake card, need authorize', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43014, 'status' => 'require check agent', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43015, 'status' => 'require authorize by wechat team to use this function!', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43016, 'status' => 'require verify', 'reason' => '小程序未认证', 'explain' => '小程序未认证'],
        ['code' => 400, 'errcode' => 43017, 'status' => 'require location id!', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43018, 'status' => 'code has no been mark!', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 43019, 'status' => 'require remove blacklist', 'reason' => '需要将接收者从黑名单中移除', 'explain' => '需要将接收者从黑名单中移除'],
        ['code' => 400, 'errcode' => 43100, 'status' => 'change template too frequently', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43101, 'status' => 'user refuse to accept the msg', 'reason' => '用户拒绝接受消息', 'explain' => '用户拒绝接受消息，如果用户之前曾经订阅过，则表示用户取消了订阅关系'],
        ['code' => 400, 'errcode' => 43102, 'status' => 'the tempalte is not subscriptiontype', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43103, 'status' => 'the api only can cancel the subscription', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43104, 'status' => 'this appid does not have permission', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43105, 'status' => 'news has no binding relation with template_id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 43106, 'status' => 'not allow to add template, for punishment', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 44001, 'status' => 'empty media data', 'reason' => '多媒体文件为空', 'explain' => '多媒体文件为空'],
        ['code' => 412, 'errcode' => 44002, 'status' => 'empty post data', 'reason' => 'POST的数据包为空', 'explain' => 'post 的数据包为空'],
        ['code' => 412, 'errcode' => 44003, 'status' => 'empty news data', 'reason' => '图文消息内容为空', 'explain' => '图文消息内容为空'],
        ['code' => 412, 'errcode' => 44004, 'status' => 'empty content', 'reason' => '文本消息内容为空', 'explain' => '文本消息内容为空'],
        ['code' => 400, 'errcode' => 44005, 'status' => 'empty list size', 'reason' => '空白的列表', 'explain' => '空白的列表'],
        ['code' => 400, 'errcode' => 44006, 'status' => 'empty file data', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 44007, 'status' => 'repeated msg id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 44997, 'status' => 'image url size out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 44998, 'status' => 'keyword string media size out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 44999, 'status' => 'keywords list size out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45000, 'status' => 'msg_id size out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 431, 'errcode' => 45001, 'status' => 'media size out of limit', 'reason' => '多媒体文件大小超过限制', 'explain' => '多媒体文件大小超过限制'],
        ['code' => 431, 'errcode' => 45002, 'status' => 'content size out of limit', 'reason' => '消息内容超过限制', 'explain' => '消息内容超过限制'],
        ['code' => 431, 'errcode' => 45003, 'status' => 'title size out of limit', 'reason' => '标题字段超过限制', 'explain' => '标题字段超过限制'],
        ['code' => 431, 'errcode' => 45004, 'status' => 'description size out of limit', 'reason' => '描述字段超过限制', 'explain' => '描述字段超过限制'],
        ['code' => 431, 'errcode' => 45005, 'status' => 'url size out of limit', 'reason' => '链接字段超过限制', 'explain' => '链接字段超过限制'],
        ['code' => 400, 'errcode' => 45006, 'status' => 'picurl size out of limit', 'reason' => '图片链接字段超过限制', 'explain' => '图片链接字段超过限制'],
        ['code' => 400, 'errcode' => 45007, 'status' => 'playtime out of limit', 'reason' => '语音播放时间超过限制', 'explain' => '语音播放时间超过限制'],
        ['code' => 400, 'errcode' => 45008, 'status' => 'article size out of limit', 'reason' => '图文消息超过限制', 'explain' => '图文消息超过限制'],
        ['code' => 408, 'errcode' => 45009, 'status' => 'reach max api daily quota limit', 'reason' => '接口调用超过限制', 'explain' => '接口调用超过限制'],
        ['code' => 408, 'errcode' => 45010, 'status' => 'create menu limit', 'reason' => '创建菜单个数超过限制', 'explain' => '创建菜单个数超过限制'],
        ['code' => 429, 'errcode' => 45011, 'status' => 'api minute-quota reach limit, must slower, retry next minute', 'reason' => 'API 调用太频繁，请稍候再试',
         'explain' => 'api 调用太频繁，请稍候再试'],
        ['code' => 400, 'errcode' => 45012, 'status' => 'template size out of limit', 'reason' => '模板大小超过限制', 'explain' => '模板大小超过限制'],
        ['code' => 400, 'errcode' => 45013, 'status' => 'too many template args', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45014, 'status' => 'template message size out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 504, 'errcode' => 45015, 'status' => 'response out of time limit or subscription is canceled', 'reason' => '回复时间超过限制', 'explain' => '回复时间超过限制'],
        ['code' => 400, 'errcode' => 45016, 'status' => 'cant modify system group', 'reason' => '系统分组，不允许修改', 'explain' => '系统分组，不允许修改'],
        ['code' => 400, 'errcode' => 45017, 'status' => 'cant set group name too long sys group', 'reason' => '分组名字过长', 'explain' => '分组名字过长'],
        ['code' => 400, 'errcode' => 45018, 'status' => 'too many group now, no need to add new', 'reason' => '分组数量超过上限', 'explain' => '分组数量超过上限'],
        ['code' => 400, 'errcode' => 45019, 'status' => 'too many openid, please input less', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45020, 'status' => 'too many image, please input less', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45021, 'status' => 'some argument may be out of length limit! please check document and request json!', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45022, 'status' => 'bonus is out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45023, 'status' => 'balance is out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45024, 'status' => 'rank template number is out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45025, 'status' => 'poiid count is out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45026, 'status' => 'template num exceeds limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45027, 'status' => 'template conflict with industry', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45028, 'status' => 'has no masssend quota', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45029, 'status' => 'qrcode count out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45030, 'status' => 'limit cardid, not support this function', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45031, 'status' => 'stock is out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45032, 'status' => 'not inner ip for special acct in white-list', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45033, 'status' => 'user get card num is out of get_limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45034, 'status' => 'media file count is out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45035, 'status' => 'access clientip is not registered, not in ip-white-list', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45036, 'status' => 'user receive announcement limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45037, 'status' => 'user out of time limit or never talked in tempsession', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45038, 'status' => 'user subscribed, cannot use tempsession api', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45039, 'status' => 'card_list_size out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45040, 'status' => 'reach max monthly quota limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45041, 'status' => 'this card reach total sku quantity limit!', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45042, 'status' => 'limit card type, this card type can not create by sub merchant', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45043,
         'status' => 'can not set share_friends=true because has no abstract or text_img_list has no img or image url not valid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45044, 'status' => 'icon url size in abstract is out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45045, 'status' => 'unauthorized friends card, please contact administrator', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45046, 'status' => 'operate field conflict, centercell, paycell, selfconsumecell conflict', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45047, 'status' => 'out of response count limit', 'reason' => '客服接口下行条数超过上限', 'explain' => '客服接口下行条数超过上限'],
        ['code' => 412, 'errcode' => 45048, 'status' => 'menu use invalid type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45049, 'status' => 'ivr use invalid type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45050, 'status' => 'custom msg use invalid type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45051, 'status' => 'template msg use invalid link', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45052, 'status' => 'masssend msg use invalid type', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45053, 'status' => 'exceed consume verify code size', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45054, 'status' => 'below consume verify code size', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45055, 'status' => 'the code is not in consume verify code charset', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45056, 'status' => 'too many tag now, no need to add new', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45057, 'status' => 'cant delete the tag that has too many fans', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45058, 'status' => 'cant modify sys tag', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45059, 'status' => 'can not tagging one user too much', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45060, 'status' => 'media is applied in ivr or menu, can not be deleted', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45061, 'status' => 'maybe the update frequency is too often, please try again', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45062, 'status' => 'has agreement ad. please use mp.weixin.qq.com', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45063, 'status' => 'accesstoken is not xiaochengxu', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45064, 'status' => 'no permission to use weapp in menu', 'reason' => '创建菜单包含未关联的小程序', 'explain' => '创建菜单包含未关联的小程序'],
        ['code' => 400, 'errcode' => 45065, 'status' => 'clientmsgid exist', 'reason' => '相同 clientmsgid 已存在群发记录，返回数据中带有已存在的群发任务的 msgid',
         'explain' => '相同 clientmsgid 已存在群发记录，返回数据中带有已存在的群发任务的 msgid'],
        ['code' => 400, 'errcode' => 45066, 'status' => 'same clientmsgid retry too fast', 'reason' => '相同 clientmsgid 重试速度过快，请间隔1分钟重试',
         'explain' => '相同 clientmsgid 重试速度过快，请间隔1分钟重试'],
        ['code' => 400, 'errcode' => 45067, 'status' => 'clientmsgid size out of limit', 'reason' => 'clientmsgid 长度超过限制', 'explain' => 'clientmsgid 长度超过限制'],
        ['code' => 400, 'errcode' => 45068, 'status' => 'file size out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45069, 'status' => 'product list size out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45070, 'status' => 'the business account have been created', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45071, 'status' => 'business account not found', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45072, 'status' => 'invalid command', 'reason' => 'command字段取值不对', 'explain' => 'command字段取值不对'],
        ['code' => 400, 'errcode' => 45073, 'status' => 'not inner vip for sns in white list', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45074, 'status' => 'material list size out of limit, you should delete the useless material', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45075, 'status' => 'invalid keyword id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45076, 'status' => 'invalid count', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45077, 'status' => 'number of business account reach limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45078, 'status' => 'nickname is illegal!', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45079, 'status' => 'nickname is forbidden!(matched forbidden keyword)', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45080,
         'status' => 'need sending message to user, or recving message from user in the last 30 seconds before typing',
         'reason' => '下发输入状态，需要之前30秒内跟用户有过消息交互', 'explain' => '下发输入状态，需要之前30秒内跟用户有过消息交互'],
        ['code' => 400, 'errcode' => 45081, 'status' => 'you are already typing', 'reason' => '请勿重复下发输入状态', 'explain' => '已经在输入状态，不可重复下发'],
        ['code' => 400, 'errcode' => 45082, 'status' => 'need icp license for the url domain', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45083, 'status' => 'the speed out of range', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45084, 'status' => 'no speed message', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45085, 'status' => 'speed server err', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45086, 'status' => 'invalid attrbute data-miniprogram-appid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45087, 'status' => 'customer service message from this account have been blocked!', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45088, 'status' => 'action size out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45089, 'status' => 'expired', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45090, 'status' => 'invalid group msg ticket', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45091, 'status' => 'account_name is illegal!', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45092, 'status' => 'no voice data', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45093, 'status' => 'no quota to send msg', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45094, 'status' => 'not allow to send custom message when user enter session, for punishment', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45095, 'status' => 'not allow to modify stock for the advertisement batch', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45096, 'status' => 'invalid qrcode', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45097, 'status' => 'invalid qrcode prefix', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45098, 'status' => 'msgmenu list size is out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45099, 'status' => 'msgmenu item content size is out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45100, 'status' => 'invalid size of keyword_id_list', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45101, 'status' => 'hit upload limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45102, 'status' => 'this api have been blocked temporarily.', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45103, 'status' => 'this api has been unsupported', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45104, 'status' => 'reach max domain quota limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45154, 'status' => 'the consume verify code not found', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45155, 'status' => 'the consume verify code is existed', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45156, 'status' => 'the consume verify codes length not invalid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45157, 'status' => 'invalid tag name', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45158, 'status' => 'tag name too long', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45159, 'status' => 'invalid tag id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45160, 'status' => 'invalid category to create card', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45161, 'status' => 'this video id must be generated by calling upload api', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45162, 'status' => 'invalid type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45163, 'status' => 'invalid sort_method', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45164, 'status' => 'invalid offset', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45165, 'status' => 'invalid limit', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45166, 'status' => 'invalid content', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 45167, 'status' => 'invalid voip call key', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45168, 'status' => 'keyword in blacklist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 45501, 'status' => 'part or whole of the requests from the very app is temporary blocked by supervisor', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 46001, 'status' => 'media data no exist', 'reason' => '不存在媒体数据', 'explain' => '不存在媒体数据，media_id 不存在'],
        ['code' => 412, 'errcode' => 46002, 'status' => 'menu version no exist', 'reason' => '菜单版本不存在', 'explain' => '不存在的菜单版本'],
        ['code' => 412, 'errcode' => 46003, 'status' => 'menu no exist', 'reason' => '不存在的菜单数据', 'explain' => '不存在的菜单数据'],
        ['code' => 412, 'errcode' => 46004, 'status' => 'user no exist', 'reason' => '不存在的用户', 'explain' => '不存在的用户'],
        ['code' => 400, 'errcode' => 46005, 'status' => 'poi no exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 46006, 'status' => 'voip file not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 46007, 'status' => 'file being transcoded, please try later', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 46008, 'status' => 'result id not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 46009, 'status' => 'there is no user data', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 46101, 'status' => 'this api have been not supported since 2020-01-11 00:00:00, please use new api(subscribemessage)!', 'reason' => '',
         'explain' => ''],
        ['code' => 500, 'errcode' => 47001, 'status' => 'data parse error', 'reason' => '解析 json/xml 内容错误', 'explain' => '解析 json/xml 内容错误'],
        ['code' => 400, 'errcode' => 47002, 'status' => 'data format error, do not use json unicode encode (\uxxxx\uxxxx), please use utf8 encoded text!', 'reason' => '',
         'explain' => ''],
        ['code' => 412, 'errcode' => 47003, 'status' => 'argument invalid!', 'reason' => '模板参数不准确', 'explain' => '模板参数不准确，可能为空或者不满足规则，errmsg会提示具体是哪个字段出错'],
        ['code' => 400, 'errcode' => 47004, 'status' => 'submit pages count more than each quota', 'reason' => '每次提交的页面数超过1000',
         'explain' => '每次提交的页面数超过1000（备注：每次提交页面数应小于或等于1000）'],
        ['code' => 400, 'errcode' => 47005, 'status' => 'tabbar no exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 47006, 'status' => 'submit pages count reach daily limit, please try tomorrow', 'reason' => '当天提交页面数达到了配额上限，请明天再试',
         'explain' => '当天提交页面数达到了配额上限，请明天再试'],
        ['code' => 400, 'errcode' => 47101, 'status' => 'search results count more than limit', 'reason' => '搜索结果总数超过了1000条', 'explain' => '搜索结果总数超过了1000条'],
        ['code' => 400, 'errcode' => 47102, 'status' => 'next_page_info error', 'reason' => 'next_page_info参数错误', 'explain' => 'next_page_info参数错误'],
        ['code' => 400, 'errcode' => 47501, 'status' => 'activity_id error', 'reason' => '参数 activity_id 错误', 'explain' => '参数 activity_id 错误'],
        ['code' => 400, 'errcode' => 47502, 'status' => 'target_state error', 'reason' => '参数 target_state 错误', 'explain' => '参数 target_state 错误'],
        ['code' => 400, 'errcode' => 47503, 'status' => 'version_type error', 'reason' => '参数 version_type 错误', 'explain' => '参数 version_type 错误'],
        ['code' => 400, 'errcode' => 47504, 'status' => 'activity_id expired time', 'reason' => 'activity_id过期时间', 'explain' => 'activity_id'],
        ['code' => 407, 'errcode' => 48001, 'status' => 'api unauthorized', 'reason' => 'api 功能未授权', 'explain' => 'api 功能未授权，请确认公众号已获得该接口，可以在公众平台官网 - 开发者中心页中查看接口权限'],
        ['code' => 412, 'errcode' => 48002, 'status' => 'user block receive message', 'reason' => '粉丝拒收消息', 'explain' => '粉丝拒收消息（粉丝在公众号选项中，关闭了【接收消息 】）'],
        ['code' => 400, 'errcode' => 48003, 'status' => 'user not agree mass-send protocol', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 48004, 'status' => 'api forbidden for irregularities, view detail on mp.weixin.qq.com', 'reason' => 'api 接口被封禁',
         'explain' => 'api 接口被封禁，请登录 mp.weixin.qq.com 查看详情'],
        ['code' => 400, 'errcode' => 48005, 'status' => 'forbid to delete material used by auto-reply or menu', 'reason' => 'api 禁止删除被自动回复和自定义菜单引用的素材',
         'explain' => 'api 禁止删除被自动回复和自定义菜单引用的素材'],
        ['code' => 400, 'errcode' => 48006, 'status' => 'forbid to clear quota because of reaching the limit', 'reason' => 'api 禁止清零调用次数，因为清零次数达到上限',
         'explain' => 'api 禁止清零调用次数，因为清零次数达到上限'],
        ['code' => 400, 'errcode' => 48007, 'status' => 'forbid to use others voip call key', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 48008, 'status' => 'no permission for this msgtype', 'reason' => '没有该类型消息的发送权限', 'explain' => '没有该类型消息的发送权限'],
        ['code' => 400, 'errcode' => 48009, 'status' => 'this api is expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 48010, 'status' => 'forbid to modify the material， please see more information on mp.weixin.qq.com', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 48011, 'status' => 'disabled template id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 48012, 'status' => 'invalid token', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 48013, 'status' => '', 'reason' => '该视频非新接口上传，不能用于视频消息群发', 'explain' => '该视频非新接口上传，不能用于视频消息群发'],
        ['code' => 400, 'errcode' => 48014, 'status' => '', 'reason' => '该视频审核状态异常，请检查后重试', 'explain' => '该视频审核状态异常，请检查后重试'],
        ['code' => 400, 'errcode' => 48015, 'status' => '', 'reason' => '该账号无留言功能权限', 'explain' => '该账号无留言功能权限'],
        ['code' => 400, 'errcode' => 48016, 'status' => '', 'reason' => '该账号不满足智能配置“观看更多”视频条件', 'explain' => '该账号不满足智能配置“观看更多”视频条件'],
        ['code' => 400, 'errcode' => 49001, 'status' => 'not same appid with appid of access_token', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 49002, 'status' => 'empty openid or transid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 49003, 'status' => 'not match openid with appid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 49004, 'status' => 'not match signature', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 49005, 'status' => 'not existed transid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 49006, 'status' => 'missing arg two_dim_code', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 49007, 'status' => 'invalid two_dim_code', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 49008, 'status' => 'invalid qrcode', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 49009, 'status' => 'missing arg qrcode', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 49010, 'status' => 'invalid partner id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 49300, 'status' => 'not existed feedbackid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 49301, 'status' => 'feedback exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 49302, 'status' => 'feedback status already changed', 'reason' => '', 'explain' => ''],
        ['code' => 407, 'errcode' => 50001, 'status' => 'api unauthorized or user unauthorized', 'reason' => '用户未授权该 api', 'explain' => '用户未授权该 api'],
        ['code' => 400, 'errcode' => 50002, 'status' => 'user limited', 'reason' => '用户受限', 'explain' => '用户受限，可能是用户账号违规后接口被封禁冻结或注销'],
        ['code' => 400, 'errcode' => 50003, 'status' => 'user unexpected, maybe not in white list', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 50004, 'status' => 'user not allow to use accesstoken, maybe for punishment', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 50005, 'status' => 'user is unsubscribed', 'reason' => '用户未关注公众号', 'explain' => '用户未关注公众号'],
        ['code' => 400, 'errcode' => 50006, 'status' => 'user has switched off friends authorization', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51000, 'status' => 'enterprise father account not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51001, 'status' => 'enterprise child account not belong to the father', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51002, 'status' => 'enterprise verify message not correct', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 51003, 'status' => 'invalid enterprise child list size', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51004, 'status' => 'not a enterprise father account', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51005, 'status' => 'not a enterprise child account', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 51006, 'status' => 'invalid nick name', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51007, 'status' => 'not a enterprise account', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 51008, 'status' => 'invalid email', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 51009, 'status' => 'invalid pwd', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51010, 'status' => 'repeated email', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51011, 'status' => 'access deny', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51012, 'status' => 'need verify code', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51013, 'status' => 'wrong verify code', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51014, 'status' => 'need modify pwd', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51015, 'status' => 'user not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51020, 'status' => 'tv info not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51021, 'status' => 'stamp crossed', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 51022, 'status' => 'invalid stamp range', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51023, 'status' => 'stamp not match date', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51024, 'status' => 'empty program name', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51025, 'status' => 'empty action url', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51026, 'status' => 'program name size out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51027, 'status' => 'action url size out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 51028, 'status' => 'invalid program name', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 51029, 'status' => 'invalid action url', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 51030, 'status' => 'invalid action id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 51031, 'status' => 'invalid action offset', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51032, 'status' => 'empty action title', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51033, 'status' => 'action title size out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51034, 'status' => 'empty action icon url', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 51035, 'status' => 'action icon url out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52000, 'status' => 'pic is not from cdn', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52001, 'status' => 'wechat price is not less than origin price', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52002, 'status' => 'category/sku is wrong', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52003, 'status' => 'product id not existed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52004, 'status' => 'category id is not exist, or doesnt has sub category', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52005, 'status' => 'quantity is zero', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 52006, 'status' => 'area code is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52007, 'status' => 'express template param is error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52008, 'status' => 'express template id is not existed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52009, 'status' => 'group name is empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52010, 'status' => 'group id is not existed', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 52011, 'status' => 'mod_action is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52012, 'status' => 'shelf components count is greater than 20', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52013, 'status' => 'shelf component is empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52014, 'status' => 'shelf id is not existed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52015, 'status' => 'order id is not existed', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 52016, 'status' => 'order filter param is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 52017, 'status' => 'order express param is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 52018, 'status' => 'order delivery param is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 52019, 'status' => 'brand name empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 53000, 'status' => 'principal limit exceed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 53001, 'status' => 'principal in black list', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 53002, 'status' => 'mobile limit exceed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 53003, 'status' => 'idcard limit exceed', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 53010, 'status' => 'nickname invalid', 'reason' => '名称格式不合法', 'explain' => '名称格式不合法'],
        ['code' => 400, 'errcode' => 53011, 'status' => 'check nickname too frequently', 'reason' => '名称检测命中频率限制', 'explain' => '名称检测命中频率限制'],
        ['code' => 400, 'errcode' => 53012, 'status' => 'nickname ban', 'reason' => '禁止使用该名称', 'explain' => '禁止使用该名称'],
        ['code' => 400, 'errcode' => 53013, 'status' => 'nickname has been occupied', 'reason' => '', 'explain' => '公众号：名称与已有公众号名称重复;小程序：该名称与已有小程序名称重复'],
        ['code' => 400, 'errcode' => 53014, 'status' => '', 'reason' => '',
         'explain' => '公众号：公众号已有{名称 a+}时，需与该账号相同主体才可申请{名称 a};小程序：小程序已有{名称 a+}时，需与该账号相同主体才可申请{名称 a}'],
        ['code' => 400, 'errcode' => 53015, 'status' => '', 'reason' => '',
         'explain' => '公众号：该名称与已有小程序名称重复，需与该小程序账号相同主体才可申请;小程序：该名称与已有公众号名称重复，需与该公众号账号相同主体才可申请'],
        ['code' => 400, 'errcode' => 53016, 'status' => '', 'reason' => '', 'explain' => '公众号：该名称与已有多个小程序名称重复，暂不支持申请;小程序：该名称与已有多个公众号名称重复，暂不支持申请'],
        ['code' => 400, 'errcode' => 53017, 'status' => '', 'reason' => '',
         'explain' => '公众号：小程序已有{名称 a+}时，需与该账号相同主体才可申请{名称 a};小程序：公众号已有{名称 a+}时，需与该账号相同主体才可申请{名称 a}'],
        ['code' => 400, 'errcode' => 53018, 'status' => 'nickname hit alias', 'reason' => '名称命中微信号', 'explain' => '名称命中微信号'],
        ['code' => 400, 'errcode' => 53019, 'status' => 'nickname protected by infringement', 'reason' => '名称在保护期内', 'explain' => '名称在保护期内'],
        ['code' => 400, 'errcode' => 53100, 'status' => 'order not found', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 53101, 'status' => 'order already paid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 53102, 'status' => 'already has checking order, can not apply', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 53103, 'status' => 'order can not do refill', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 53200, 'status' => 'modify signature quota limit exceed', 'reason' => '本月功能介绍修改次数已用完', 'explain' => '本月功能介绍修改次数已用完'],
        ['code' => 400, 'errcode' => 53201, 'status' => 'signature in black list, can not use', 'reason' => '功能介绍内容命中黑名单关键字', 'explain' => '功能介绍内容命中黑名单关键字'],
        ['code' => 400, 'errcode' => 53202, 'status' => 'modify avatar quota limit exceed', 'reason' => '本月头像修改次数已用完', 'explain' => '本月头像修改次数已用完'],
        ['code' => 400, 'errcode' => 53203, 'status' => 'cant be modified for the time being', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 53204, 'status' => 'signature invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 53300, 'status' => '', 'reason' => '超出每月次数限制', 'explain' => '超出每月次数限制'],
        ['code' => 400, 'errcode' => 53301, 'status' => '', 'reason' => '超出可配置类目总数限制', 'explain' => '超出可配置类目总数限制'],
        ['code' => 400, 'errcode' => 53302, 'status' => '', 'reason' => '当前账号主体类型不允许设置此种类目', 'explain' => '当前账号主体类型不允许设置此种类目'],
        ['code' => 400, 'errcode' => 53303, 'status' => '', 'reason' => '提交的参数不合法', 'explain' => '提交的参数不合法'],
        ['code' => 400, 'errcode' => 53304, 'status' => '', 'reason' => '与已有类目重复', 'explain' => '与已有类目重复'],
        ['code' => 400, 'errcode' => 53305, 'status' => '', 'reason' => '包含未通过 ipc 校验的类目', 'explain' => '包含未通过 ipc 校验的类目'],
        ['code' => 400, 'errcode' => 53306, 'status' => '', 'reason' => '不允许修改类目id', 'explain' => '修改类目只允许修改类目资质，不允许修改类目id'],
        ['code' => 400, 'errcode' => 53307, 'status' => '', 'reason' => '只有审核失败的类目允许修改', 'explain' => '只有审核失败的类目允许修改'],
        ['code' => 400, 'errcode' => 53308, 'status' => '', 'reason' => '审核中的类目不允许删除', 'explain' => '审核中的类目不允许删除'],
        ['code' => 400, 'errcode' => 53309, 'status' => '', 'reason' => '社交红包不允许删除', 'explain' => '社交红包不允许删除'],
        ['code' => 400, 'errcode' => 53310, 'status' => '', 'reason' => '类目超过上限', 'explain' => '类目超过上限，但是可以添加apply_reason参数申请更多类目'],
        ['code' => 400, 'errcode' => 53311, 'status' => '', 'reason' => '需要提交资料信息', 'explain' => '需要提交资料信息'],
        ['code' => 412, 'errcode' => 53500, 'status' => '', 'reason' => '发布功能被封禁', 'explain' => ''],
        ['code' => 429, 'errcode' => 53501, 'status' => '', 'reason' => '频繁请求发布', 'explain' => '频繁请求发布'],
        ['code' => 412, 'errcode' => 53502, 'status' => 'invalid Publish ID', 'reason' => 'Publish ID 无效', 'explain' => 'publish id 无效'],
        ['code' => 412, 'errcode' => 53600, 'status' => 'invalid Article ID', 'reason' => 'Article ID 无效', 'explain' => 'article id 无效'],
        ['code' => 400, 'errcode' => 60005, 'status' => 'empty jsapi name', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 60006, 'status' => 'user cancel the auth', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61000, 'status' => 'invalid component type', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61001, 'status' => 'component type and component appid is not match', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61002, 'status' => 'the third appid is not open kf', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61003, 'status' => 'component is not authorized by this account', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61004, 'status' => 'access clientip is not registered', 'reason' => 'api 功能未授权',
         'explain' => 'api 功能未授权，请确认公众号/小程序已获得该接口，可以在公众平台官网 - 开发者中心页中查看接口权限'],
        ['code' => 400, 'errcode' => 61005, 'status' => 'component ticket is expired', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61006, 'status' => 'component ticket is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61007, 'status' => 'api is unauthorized to component', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61008, 'status' => 'component req key is duplicated', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61009, 'status' => 'code is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61010, 'status' => 'code is expired', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61011, 'status' => 'invalid component', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61012, 'status' => 'invalid option name', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61013, 'status' => 'invalid option value', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61014, 'status' => 'must use component token for component api', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61015, 'status' => 'must use biz account token for not component api', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61016, 'status' => 'function category of api need be confirmed by component', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61017, 'status' => 'function category is not authorized', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61018, 'status' => 'already confirm', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61019, 'status' => 'not need confirm', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61020, 'status' => 'error parameter', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61021, 'status' => 'cant confirm', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61022, 'status' => 'cant resubmit', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61023, 'status' => 'refresh_token is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61024, 'status' => 'must use api(api_component_token) to get token for component acct', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61025, 'status' => 'read-only option', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61026, 'status' => 'register access deny', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61027, 'status' => 'register limit exceed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61028, 'status' => 'component is unpublished', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61029, 'status' => 'component need republish with base category', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61030, 'status' => 'component cancel authorization not allowed', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61051, 'status' => 'invalid realname type', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61052, 'status' => 'need to be certified', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61053, 'status' => 'realname exceed limits', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61054, 'status' => 'realname in black list', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61055, 'status' => 'exceed quota per month', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61056, 'status' => 'copy_wx_verify is required option', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61058, 'status' => 'invalid ticket', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61061, 'status' => 'overseas access deny', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61063, 'status' => 'admin exceed limits', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61064, 'status' => 'admin in black list', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61065, 'status' => 'idcard exceed limits', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61066, 'status' => 'idcard in black list', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61067, 'status' => 'mobile exceed limits', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61068, 'status' => 'mobile in black list', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61069, 'status' => 'invalid admin', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61070, 'status' => 'name, idcard, wechat name not in accordance', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61100, 'status' => 'invalid url', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61101, 'status' => 'invalid openid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61102, 'status' => 'share relation not existed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61200, 'status' => 'product wording not set', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61300, 'status' => 'invalid base info', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61301, 'status' => 'invalid detail info', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61302, 'status' => 'invalid action info', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61303, 'status' => 'brand info not exist', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61304, 'status' => 'invalid product id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61305, 'status' => 'invalid key info', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61306, 'status' => 'invalid appid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61307, 'status' => 'invalid card id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61308, 'status' => 'base info not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61309, 'status' => 'detail info not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61310, 'status' => 'action info not exist', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61311, 'status' => 'invalid media info', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61312, 'status' => 'invalid buffer size', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61313, 'status' => 'invalid buffer', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61314, 'status' => 'invalid qrcode extinfo', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61315, 'status' => 'invalid local ext info', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61316, 'status' => 'key conflict', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61317, 'status' => 'ticket invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61318, 'status' => 'verify not pass', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61319, 'status' => 'category invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61320, 'status' => 'merchant info not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61321, 'status' => 'cate id is a leaf node', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61322, 'status' => 'category id no permision', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61323, 'status' => 'barcode no permision', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61324, 'status' => 'exceed max action num', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61325, 'status' => 'brandinfo invalid store mgr type', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61326, 'status' => 'anti-spam blocked', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61327, 'status' => 'comment reach limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61328, 'status' => 'comment data is not the newest', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61329, 'status' => 'comment hit ban word', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61330, 'status' => 'image already add', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61331, 'status' => 'image never add', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61332, 'status' => 'warning, image quanlity too low', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61333, 'status' => 'warning, image simility too high', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61334, 'status' => 'product not exists', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61335, 'status' => 'key apply fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61336, 'status' => 'check status fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61337, 'status' => 'product already exists', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61338, 'status' => 'forbid delete', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61339, 'status' => 'firmcode claimed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61340, 'status' => 'check firm info fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61341, 'status' => 'too many white list uin', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61342, 'status' => 'keystandard not match', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61343, 'status' => 'keystandard error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61344, 'status' => 'id map not exists', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61345, 'status' => 'invalid action code', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61346, 'status' => 'invalid actioninfo store', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61347, 'status' => 'invalid actioninfo media', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61348, 'status' => 'invalid actioninfo text', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61350, 'status' => 'invalid input data', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61351, 'status' => 'input data exceed max size', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61400, 'status' => 'kf_account error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61401, 'status' => 'kf system alredy transfer', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 61450, 'status' => 'system error', 'reason' => '系统错误', 'explain' => ''],
        ['code' => 412, 'errcode' => 61451, 'status' => 'invalid parameter', 'reason' => '参数错误', 'explain' => ''],
        ['code' => 412, 'errcode' => 61452, 'status' => 'invalid kf_account', 'reason' => '无效客服账号', 'explain' => '无效客服账号'],
        ['code' => 400, 'errcode' => 61453, 'status' => 'kf_account exsited', 'reason' => '客服账号已存在', 'explain' => '客服账号已存在'],
        ['code' => 412, 'errcode' => 61454, 'status' => 'invalid kf_acount length', 'reason' => '客服账号名长度超过限制', 'explain' => '客服账号名长度超过限制 ( 仅允许 10 个英文字符，不包括 @ 及 @ 后的公众号的微信号)'],
        ['code' => 400, 'errcode' => 61455, 'status' => 'illegal character in kf_account', 'reason' => '客服账号名包含非法字符', 'explain' => '客服账号名包含非法字符 (仅允许英文 + 数字)'],
        ['code' => 400, 'errcode' => 61456, 'status' => 'kf_account count exceeded', 'reason' => '客服账号个数超过限制', 'explain' => '客服账号个数超过限制 (10 个客服账号)'],
        ['code' => 412, 'errcode' => 61457, 'status' => 'invalid file type', 'reason' => '无效头像文件类型', 'explain' => '无效头像文件类型'],
        ['code' => 400, 'errcode' => 61500, 'status' => 'date format error', 'reason' => '日期格式错误', 'explain' => '日期格式错误'],
        ['code' => 400, 'errcode' => 61501, 'status' => 'date range error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61502, 'status' => 'this is game miniprogram, data api is not supported', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 61503, 'status' => 'data not ready, please try later', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 62001, 'status' => 'trying to access others app', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 62002, 'status' => 'app name already exists', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 62003, 'status' => 'please provide at least one platform', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 62004, 'status' => 'invalid app name', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 62005, 'status' => 'invalid app id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63001, 'status' => 'some arguments is empty', 'reason' => '部分参数为空', 'explain' => '部分参数为空'],
        ['code' => 412, 'errcode' => 63002, 'status' => 'invalid signature', 'reason' => '无效的签名', 'explain' => '无效的签名'],
        ['code' => 412, 'errcode' => 63003, 'status' => 'invalid signature method', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63004, 'status' => 'no authroize', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63149, 'status' => 'gen ticket fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63152, 'status' => 'set ticket fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63153, 'status' => 'shortid decode fail', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 63154, 'status' => 'invalid status', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 63155, 'status' => 'invalid color', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 63156, 'status' => 'invalid tag', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 63157, 'status' => 'invalid recommend', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63158, 'status' => 'branditem out of limits', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63159, 'status' => 'retail_price empty', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 63160, 'status' => 'priceinfo invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63161, 'status' => 'antifake module num limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63162, 'status' => 'antifake native_type err', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63163, 'status' => 'antifake link not exists', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63164, 'status' => 'module type not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63165, 'status' => 'module info not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63166, 'status' => 'item is beding verified', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63167, 'status' => 'item not published', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63168, 'status' => 'verify not pass', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63169, 'status' => 'already published', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63170, 'status' => 'only banner or media', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63171, 'status' => 'card num limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63172, 'status' => 'user num limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63173, 'status' => 'text num limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63174, 'status' => 'link card user sum limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63175, 'status' => 'detail info error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63176, 'status' => 'not this type', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63177, 'status' => 'src or secretkey or version or expired_time is wrong', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63178, 'status' => 'appid wrong', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63179, 'status' => 'openid num limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63180, 'status' => 'this app msg not found', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63181, 'status' => 'get history app msg end', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 63182, 'status' => 'openid_list empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65001, 'status' => 'unknown deeplink type', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65002, 'status' => 'deeplink unauthorized', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65003, 'status' => 'bad deeplink', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65004, 'status' => 'deeplinks of the very type are supposed to have short-life', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65104, 'status' => 'invalid categories', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65105, 'status' => 'invalid photo url', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65106, 'status' => 'poi audit state must be approved', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65107, 'status' => 'poi not allowed modify now', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65109, 'status' => 'invalid business name', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65110, 'status' => 'invalid address', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65111, 'status' => 'invalid telephone', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65112, 'status' => 'invalid city', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65113, 'status' => 'invalid province', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65114, 'status' => 'photo list empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65115, 'status' => 'poi_id is not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65116, 'status' => 'poi has been deleted', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65117, 'status' => 'cannot delete poi', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65118, 'status' => 'store status is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65119, 'status' => 'lack of qualification for relevant principals', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65120, 'status' => 'category info is not found', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65201, 'status' => 'room_name is empty, please check your input', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65202, 'status' => 'user_id is empty, please check your input', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65203, 'status' => 'invalid check ticket', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65204, 'status' => 'invalid check ticket opt code', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65205, 'status' => 'check ticket out of time', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65301, 'status' => 'this menu is not conditionalmenu', 'reason' => '不存在此 menuid 对应的个性化菜单', 'explain' => '不存在此 menuid 对应的个性化菜单'],
        ['code' => 412, 'errcode' => 65302, 'status' => 'User not found', 'reason' => '没有相应的用户', 'explain' => '没有相应的用户'],
        ['code' => 400, 'errcode' => 65303, 'status' => 'there is no selfmenu, please create selfmenu first', 'reason' => '请先创建默认菜单', 'explain' => '没有默认菜单，不能创建个性化菜单'],
        ['code' => 400, 'errcode' => 65304, 'status' => 'match rule empty', 'reason' => 'matchrule 信息为空', 'explain' => 'matchrule 信息为空'],
        ['code' => 400, 'errcode' => 65305, 'status' => 'menu count limit', 'reason' => '个性化菜单数量受限', 'explain' => '个性化菜单数量受限'],
        ['code' => 400, 'errcode' => 65306, 'status' => 'conditional menu not support', 'reason' => '不支持个性化菜单的账号', 'explain' => '不支持个性化菜单的账号'],
        ['code' => 400, 'errcode' => 65307, 'status' => 'conditional menu is empty', 'reason' => '个性化菜单信息为空', 'explain' => '个性化菜单信息为空'],
        ['code' => 400, 'errcode' => 65308, 'status' => 'exist empty button act', 'reason' => '包含没有响应类型的 button', 'explain' => '包含没有响应类型的 button'],
        ['code' => 400, 'errcode' => 65309, 'status' => 'conditional menu switch is closed', 'reason' => '个性化菜单开关处于关闭状态', 'explain' => '个性化菜单开关处于关闭状态'],
        ['code' => 400, 'errcode' => 65310, 'status' => 'country is empty', 'reason' => '国家信息不能为空', 'explain' => '填写了省份或城市信息，国家信息不能为空'],
        ['code' => 400, 'errcode' => 65311, 'status' => 'province is empty', 'reason' => '省份信息不能为空', 'explain' => '填写了城市信息，省份信息不能为空'],
        ['code' => 412, 'errcode' => 65312, 'status' => 'invalid country info', 'reason' => '不合法的国家信息', 'explain' => '不合法的国家信息'],
        ['code' => 412, 'errcode' => 65313, 'status' => 'invalid province info', 'reason' => '不合法的省份信息', 'explain' => '不合法的省份信息'],
        ['code' => 412, 'errcode' => 65314, 'status' => 'invalid city info', 'reason' => '非法的城市信息', 'explain' => '不合法的城市信息'],
        ['code' => 400, 'errcode' => 65315, 'status' => 'not fans', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65316, 'status' => 'domain count reach limit', 'reason' => '域名外跳过多', 'explain' => '该公众号的菜单设置了过多的域名外跳（最多跳转到 3 个域名的链接）'],
        ['code' => 412, 'errcode' => 65317, 'status' => 'contain invalid url', 'reason' => '不合法的 url', 'explain' => '不合法的 url'],
        ['code' => 400, 'errcode' => 65318, 'status' => 'must use utf-8 charset', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65319, 'status' => 'not allow to create menu', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65400, 'status' => 'please enable new custom service, or wait for a while if you have enabled', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65401, 'status' => 'invalid custom service account', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65402, 'status' => 'the custom service account need to bind a wechat user', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65403, 'status' => 'illegal nickname', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65404, 'status' => 'illegal custom service account', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65405, 'status' => 'custom service account number reach limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65406, 'status' => 'custom service account exists', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65407, 'status' => 'the wechat user have been one of your workers', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65408, 'status' => 'you have already invited the wechat user', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65409, 'status' => 'wechat account invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65410, 'status' => 'too many custom service accounts bound by the worker', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65411, 'status' => 'a effective invitation to bind the custom service account exists', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65412, 'status' => 'the custom service account have been bound by a wechat user', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65413, 'status' => 'no effective session for the customer', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65414, 'status' => 'another worker is serving the customer', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65415, 'status' => 'the worker is not online', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 65416, 'status' => 'param invalid, please check', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65417, 'status' => 'it is too long from the starttime to endtime', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 65450, 'status' => 'homepage not exists', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68002, 'status' => 'invalid store type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68003, 'status' => 'invalid store name', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68004, 'status' => 'invalid store wxa path', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68005, 'status' => 'miss store wxa path', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68006, 'status' => 'invalid kefu type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68007, 'status' => 'invalid kefu wxa path', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68008, 'status' => 'invalid kefu phone number', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68009, 'status' => 'invalid sub mch id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68010, 'status' => 'store id has exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68011, 'status' => 'miss store name', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68012, 'status' => 'miss create time', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68013, 'status' => 'invalid status', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68014, 'status' => 'invalid receiver info', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68015, 'status' => 'invalid product', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68016, 'status' => 'invalid pay type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68017, 'status' => 'invalid fast mail no', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68018, 'status' => 'invalid busi id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68019, 'status' => 'miss product sku', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68020, 'status' => 'invalid service type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68021, 'status' => 'invalid service status', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68022, 'status' => 'invalid service_id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68023, 'status' => 'service_id has exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68024, 'status' => 'miss service wxa path', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68025, 'status' => 'invalid product sku', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68026, 'status' => 'invalid product spu', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68027, 'status' => 'miss product spu', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68028, 'status' => 'can not find product spu and spu in order list', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68029, 'status' => 'sku and spu duplicated', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68030, 'status' => 'busi_id has exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68031, 'status' => 'update fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68032, 'status' => 'busi_id not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68033, 'status' => 'store no exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68034, 'status' => 'miss product number', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68035, 'status' => 'miss wxa order detail path', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68036, 'status' => 'there is no enough products to refund', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68037, 'status' => 'invalid refund info', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68038, 'status' => 'shipped but no fast mail info', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68039, 'status' => 'invalid wechat pay no', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68040, 'status' => 'all product has been refunded, the order can not be finished', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68041, 'status' => 'invalid service create time, it must bigger than the time of order', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68042, 'status' => 'invalid total cost, it must be smaller than the sum of product and shipping cost', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68043, 'status' => 'invalid role', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68044, 'status' => 'invalid service_available args', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68045, 'status' => 'invalid order type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68046, 'status' => 'invalid order deliver type', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 68500, 'status' => 'require store_id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 68501, 'status' => 'invalid store_id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 71001, 'status' => 'invalid parameter, parameter is zero or missing', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 71002, 'status' => 'invalid orderid, may be the other parameter not fit with orderid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 71003, 'status' => 'coin not enough', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 71004, 'status' => 'card is expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 71005, 'status' => 'limit exe count', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 71006, 'status' => 'limit coin count, 1 <= coin_count <= 100000', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 71007, 'status' => 'order finish', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 71008, 'status' => 'order time out', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72001, 'status' => 'no match card', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72002, 'status' => 'mchid is not bind appid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 72003, 'status' => 'invalid card type, need member card', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72004, 'status' => 'mchid is occupied by the other appid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72005, 'status' => 'out of mchid size limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72006, 'status' => 'invald title', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 72007, 'status' => 'invalid reduce cost, can not less than 100', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 72008, 'status' => 'invalid least cost, most larger than reduce cost', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 72009, 'status' => 'invalid get limit, can not over 50', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 72010, 'status' => 'invalid mchid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 72011, 'status' => 'invalid activate_ticket.maybe this ticket is not belong this appid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72012, 'status' => 'activate_ticket has been expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72013, 'status' => 'unauthorized order_id or authorization is expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72014, 'status' => 'task card share stock can not modify stock', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72015, 'status' => 'unauthorized create invoice', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72016, 'status' => 'unauthorized create member card', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 72017, 'status' => 'invalid invoice title', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72018, 'status' => 'duplicate order id, invoice had inserted to user', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72019, 'status' => 'limit msg operation card list size, must <= 5', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72020, 'status' => 'limit consume in use limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72021, 'status' => 'unauthorized create general card', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72022, 'status' => 'user unexpected, please add user to white list', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72023, 'status' => 'invoice has been lock by others', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72024, 'status' => 'invoice status error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72025, 'status' => 'invoice token error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72026, 'status' => 'need set wx_activate true', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72027, 'status' => 'invoice action error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72028, 'status' => 'invoice never set pay mch info', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72029, 'status' => 'invoice never set auth field', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 72030, 'status' => 'invalid mchid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 72031, 'status' => 'invalid params', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72032, 'status' => 'pay gift card rule expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72033, 'status' => 'pay gift card rule status err', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72034, 'status' => 'invlid rule id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72035, 'status' => 'biz reject insert', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72036, 'status' => 'invoice is busy, try again please', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72037, 'status' => 'invoice owner error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72038, 'status' => 'invoice order never auth', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72039, 'status' => 'invoice must be lock first', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72040, 'status' => 'invoice pdf error', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 72041, 'status' => 'billing_code and billing_no invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72042, 'status' => 'billing_code and billing_no repeated', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72043, 'status' => 'billing_code or billing_no size error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72044, 'status' => 'scan text out of time', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72045, 'status' => 'check_code is empty', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 72046, 'status' => 'pdf_url is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72047, 'status' => 'pdf billing_code or pdf billing_no is error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72048, 'status' => 'insert too many invoice, need auth again', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72049, 'status' => 'never auth', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72050, 'status' => 'auth expired, need auth again', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72051, 'status' => 'app type error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72052, 'status' => 'get too many invoice', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72053, 'status' => 'user never auth', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72054, 'status' => 'invoices is inserting, wait a moment please', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72055, 'status' => 'too many invoices', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72056, 'status' => 'order_id repeated, please check order_id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72057, 'status' => 'today insert limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72058, 'status' => 'callback biz error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72059, 'status' => 'this invoice is giving to others, wait a moment please', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72060, 'status' => 'this invoice has been cancelled, check the reimburse_status please', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72061, 'status' => 'this invoice has been closed, check the reimburse_status please', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72062, 'status' => 'this code_auth_key is limited, try other code_auth_key please', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72063, 'status' => 'biz contact is empty, set contact first please', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72064, 'status' => 'tbc error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72065, 'status' => 'tbc logic error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72066, 'status' => 'the card is send for advertisement, not allow modify time and budget', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72067, 'status' => 'batch insert auth key expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72068, 'status' => 'batch insert auth key_owner', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72069, 'status' => 'batch task run_error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72070, 'status' => 'biz_title_key_out_time', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72071, 'status' => 'discern_gaopeng_error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72072, 'status' => 'discern_type_error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72073, 'status' => 'fee_error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72074, 'status' => 'has_auth', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72075, 'status' => 'has_send', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72076, 'status' => 'invoicesign', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72077, 'status' => 'key_deleted', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72078, 'status' => 'key_expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72079, 'status' => 'mount_error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72080, 'status' => 'no_found', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72081, 'status' => 'no_pull_pdf', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72082, 'status' => 'pdf_check_error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72083, 'status' => 'pull_pdf_fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72084, 'status' => 'push_biz_empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72085, 'status' => 'sdk appid error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72086, 'status' => 'sdk biz error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72087, 'status' => 'sdk_url_error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72088, 'status' => 'search_title_fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72089, 'status' => 'title_busy', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72090, 'status' => 'title_no_found', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72091, 'status' => 'token_err', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72092, 'status' => 'user_title_not_found', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 72093, 'status' => 'verify_3rd_fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73000, 'status' => 'sys error make out invoice failed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73001, 'status' => 'wxopenid error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73002, 'status' => 'ddh orderid empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73003, 'status' => 'wxopenid empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73004, 'status' => 'fpqqlsh empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73005, 'status' => 'not a commercial', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73006, 'status' => 'kplx empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73007, 'status' => 'nsrmc empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73008, 'status' => 'nsrdz empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73009, 'status' => 'nsrdh empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73010, 'status' => 'ghfmc empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73011, 'status' => 'kpr empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73012, 'status' => 'jshj empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73013, 'status' => 'hjje empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73014, 'status' => 'hjse empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73015, 'status' => 'hylx empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73016, 'status' => 'nsrsbh empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73100, 'status' => 'kaipiao plat error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73101, 'status' => 'nsrsbh not cmp', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 73103, 'status' => 'invalid wxa appid in url_cell, wxa appid is need to bind biz appid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73104, 'status' => 'reach frequency limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73105, 'status' => 'kp plat make invoice timeout, please try again with the same fpqqlsh', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73106, 'status' => 'fpqqlsh exist with different ddh', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73107, 'status' => 'fpqqlsh is processing, please wait and query later', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73108, 'status' => 'this ddh with other fpqqlsh already exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73109, 'status' => 'this fpqqlsh not exist in kpplat', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73200, 'status' => 'get card detail by card id and code fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73201, 'status' => 'get cloud invoice record fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73202, 'status' => 'get appinfo fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73203, 'status' => 'get invoice category or rule kv error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73204, 'status' => 'request card not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73205, 'status' => '', 'reason' => '朋友的券玩法升级中，当前暂停创建，请创建其他类型卡券', 'explain' => '朋友的券玩法升级中，当前暂停创建，请创建其他类型卡券'],
        ['code' => 400, 'errcode' => 73206, 'status' => '', 'reason' => '朋友的券玩法升级中，当前暂停券点充值，请创建其他类型卡券', 'explain' => '朋友的券玩法升级中，当前暂停券点充值，请创建其他类型卡券'],
        ['code' => 400, 'errcode' => 73207, 'status' => '', 'reason' => '朋友的券玩法升级中，当前暂停开通券点账户', 'explain' => '朋友的券玩法升级中，当前暂停开通券点账户'],
        ['code' => 400, 'errcode' => 73208, 'status' => '', 'reason' => '朋友的券玩法升级中，当前不支持修改库存', 'explain' => '朋友的券玩法升级中，当前不支持修改库存'],
        ['code' => 400, 'errcode' => 73209, 'status' => '', 'reason' => '朋友的券玩法升级中，当前不支持修改有效期', 'explain' => '朋友的券玩法升级中，当前不支持修改有效期'],
        ['code' => 400, 'errcode' => 73210, 'status' => '', 'reason' => '当前批次不支持修改卡券批次库存', 'explain' => '当前批次不支持修改卡券批次库存'],
        ['code' => 400, 'errcode' => 73211, 'status' => '', 'reason' => '不再支持配置网页链接跳转，请选择小程序替代', 'explain' => '不再支持配置网页链接跳转，请选择小程序替代'],
        ['code' => 400, 'errcode' => 73212, 'status' => 'unauthorized backup member', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 73213, 'status' => 'invalid code type', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73214, 'status' => 'the user is already a member', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 73215, 'status' => '', 'reason' => '请直接使用微信支付代金券api',
         'explain' => '支付打通券能力已下线，请直接使用微信支付代金券api：https://pay.weixin.qq.com/wiki/doc/apiv3/wxpay/marketing/convention/chapter1_1.shtml'],
        ['code' => 400, 'errcode' => 73216, 'status' => '', 'reason' => '不合法的按钮名字', 'explain' => '不合法的按钮名字，请从中选择一个:使用礼品卡/立即使用/去点外卖'],
        ['code' => 400, 'errcode' => 73217, 'status' => '', 'reason' => '礼品卡本身没有设置 appname 和path，不允许在修改接口设置', 'explain' => '礼品卡本身没有设置 appname 和path，不允许在修改接口设置'],
        ['code' => 400, 'errcode' => 73218, 'status' => '', 'reason' => '未授权使用礼品卡落地页跳转小程序功能', 'explain' => '未授权使用礼品卡落地页跳转小程序功能'],
        ['code' => 400, 'errcode' => 74000, 'status' => 'not find this wx_hotel_id info', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74001, 'status' => 'request some param empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74002, 'status' => 'request some param error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74003, 'status' => 'request some param error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74004, 'status' => 'datetime error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74005, 'status' => 'checkin mode error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74007, 'status' => 'carid from error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74008, 'status' => 'this hotel routecode not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74009, 'status' => 'this hotel routecode info error contract developer', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74010, 'status' => 'maybe not support report mode', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74011, 'status' => 'pic deocde not ok maybe its not good picdata', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74021, 'status' => 'verify sys erro', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74022, 'status' => 'inner police erro', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74023, 'status' => 'unable to detect the face', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74040, 'status' => 'report checkin 2 lvye sys erro', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 74041, 'status' => 'report checkou 2 lvye sys erro', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75001, 'status' => 'some param emtpy please check', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75002, 'status' => 'param illegal please check', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75003, 'status' => 'sys error kv store error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75004, 'status' => 'sys error kvstring store error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75005, 'status' => 'product not exist please check your product_id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75006, 'status' => 'order not exist please check order_id and buyer_appid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75007, 'status' => 'do not allow this status to change please check this order_id status now', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75008, 'status' => 'product has exist please use new id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75009, 'status' => 'notify order status failed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75010, 'status' => 'buyer bussiness info not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75011, 'status' => 'you had registered', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75012, 'status' => 'store image key to kv error, please try again', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75013, 'status' => 'get image fail, please check you image key', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75014, 'status' => 'this key is not belong to you', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75015, 'status' => 'this key is expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75016, 'status' => 'encrypt decode key fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75017, 'status' => 'encrypt encode key fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75018, 'status' => 'bind buyer business info fail please contact us', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 75019, 'status' => 'this key is empty, user may not upload file', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 80000, 'status' => '', 'reason' => '系统错误，请稍后再试', 'explain' => '系统错误，请稍后再试'],
        ['code' => 400, 'errcode' => 80001, 'status' => '', 'reason' => '参数格式校验错误', 'explain' => '参数格式校验错误'],
        ['code' => 400, 'errcode' => 80002, 'status' => '', 'reason' => '签名失败', 'explain' => '签名失败'],
        ['code' => 400, 'errcode' => 80003, 'status' => '', 'reason' => '该日期订单未生成', 'explain' => '该日期订单未生成'],
        ['code' => 400, 'errcode' => 80004, 'status' => '', 'reason' => '用户未绑卡', 'explain' => '用户未绑卡'],
        ['code' => 400, 'errcode' => 80005, 'status' => '', 'reason' => '姓名不符', 'explain' => '姓名不符'],
        ['code' => 400, 'errcode' => 80006, 'status' => '', 'reason' => '身份证不符', 'explain' => '身份证不符'],
        ['code' => 400, 'errcode' => 80007, 'status' => '', 'reason' => '获取城市信息失败', 'explain' => '获取城市信息失败'],
        ['code' => 400, 'errcode' => 80008, 'status' => '', 'reason' => '未找到指定少儿信息', 'explain' => '未找到指定少儿信息'],
        ['code' => 400, 'errcode' => 80009, 'status' => '', 'reason' => '少儿身份证不符', 'explain' => '少儿身份证不符'],
        ['code' => 400, 'errcode' => 80010, 'status' => '', 'reason' => '少儿未绑定', 'explain' => '少儿未绑定'],
        ['code' => 400, 'errcode' => 80011, 'status' => '', 'reason' => '签约号不符', 'explain' => '签约号不符'],
        ['code' => 400, 'errcode' => 80012, 'status' => '', 'reason' => '该地区局方配置不存在', 'explain' => '该地区局方配置不存在'],
        ['code' => 400, 'errcode' => 80013, 'status' => '', 'reason' => '调用方 appid 与局方配置不匹配', 'explain' => '调用方 appid 与局方配置不匹配'],
        ['code' => 400, 'errcode' => 80014, 'status' => '', 'reason' => '获取消息账号失败', 'explain' => '获取消息账号失败'],
        ['code' => 400, 'errcode' => 80066, 'status' => '', 'reason' => '非法的插件版本', 'explain' => '非法的插件版本'],
        ['code' => 400, 'errcode' => 80067, 'status' => '', 'reason' => '找不到使用的插件', 'explain' => '找不到使用的插件'],
        ['code' => 400, 'errcode' => 80082, 'status' => '', 'reason' => '没有权限使用该插件', 'explain' => '没有权限使用该插件'],
        ['code' => 400, 'errcode' => 80101, 'status' => '', 'reason' => '商家未接入', 'explain' => '商家未接入'],
        ['code' => 400, 'errcode' => 80111, 'status' => '', 'reason' => '实名校验 code 不存在', 'explain' => '实名校验 code 不存在'],
        ['code' => 400, 'errcode' => 80112, 'status' => '', 'reason' => 'code并发冲突', 'explain' => 'code并发冲突'],
        ['code' => 400, 'errcode' => 80113, 'status' => '', 'reason' => '无效code', 'explain' => '无效code'],
        ['code' => 412, 'errcode' => 80201, 'status' => 'invalid report_type', 'reason' => 'report_type无效', 'explain' => 'report_type无效'],
        ['code' => 412, 'errcode' => 80202, 'status' => 'invalid service_type', 'reason' => 'service_type无效', 'explain' => 'service_type无效'],
        ['code' => 400, 'errcode' => 80300, 'status' => '', 'reason' => '申请单不存在', 'explain' => '申请单不存在'],
        ['code' => 400, 'errcode' => 80301, 'status' => '', 'reason' => '申请单不属于该账号', 'explain' => '申请单不属于该账号'],
        ['code' => 400, 'errcode' => 80302, 'status' => '', 'reason' => '激活号段有重叠', 'explain' => '激活号段有重叠'],
        ['code' => 400, 'errcode' => 80303, 'status' => '', 'reason' => '码格式错误', 'explain' => '码格式错误'],
        ['code' => 400, 'errcode' => 80304, 'status' => '', 'reason' => '该码未激活', 'explain' => '该码未激活'],
        ['code' => 400, 'errcode' => 80305, 'status' => '', 'reason' => '激活失败', 'explain' => '激活失败'],
        ['code' => 400, 'errcode' => 80306, 'status' => '', 'reason' => '码索引超出申请范围', 'explain' => '码索引超出申请范围'],
        ['code' => 400, 'errcode' => 80307, 'status' => '', 'reason' => '申请已存在', 'explain' => '申请已存在'],
        ['code' => 400, 'errcode' => 80308, 'status' => '', 'reason' => '子任务未完成', 'explain' => '子任务未完成'],
        ['code' => 400, 'errcode' => 80309, 'status' => '', 'reason' => '子任务文件过期', 'explain' => '子任务文件过期'],
        ['code' => 400, 'errcode' => 80310, 'status' => '', 'reason' => '子任务不存在', 'explain' => '子任务不存在'],
        ['code' => 400, 'errcode' => 80311, 'status' => '', 'reason' => '获取文件失败', 'explain' => '获取文件失败'],
        ['code' => 400, 'errcode' => 80312, 'status' => '', 'reason' => '加密数据失败', 'explain' => '加密数据失败'],
        ['code' => 400, 'errcode' => 80313, 'status' => '', 'reason' => '加密数据密钥不存在', 'explain' => '加密数据密钥不存在，请联系接口人申请'],
        ['code' => 400, 'errcode' => 81000, 'status' => 'can not set page_id in addgiftcardpage', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81001, 'status' => 'card_list is empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81002, 'status' => 'card_id is not giftcard', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81004, 'status' => 'banner_pic_url is empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81005, 'status' => 'banner_pic_url is not from cdn', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81006, 'status' => 'giftcard_wrap_pic_url_list is empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81007, 'status' => 'giftcard_wrap_pic_url_list is not from cdn', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81008, 'status' => 'address is empty', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 81009, 'status' => 'service_phone is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81010, 'status' => 'biz_description is empty', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 81011, 'status' => 'invalid page_id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 81012, 'status' => 'invalid order_id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 81013, 'status' => 'invalid time_range, begin_time + 31day must less than end_time', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 81014, 'status' => 'invalid count! count must equal or less than 100', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 81015, 'status' => 'invalid category_index or category.title is empty or is_banner but has_category_index', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81016, 'status' => 'is_banner is more than 1', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81017, 'status' => 'order status error, please check pay status or gifting_status', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81018, 'status' => 'refund reduplicate, the order is already refunded', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81019, 'status' => 'lock order fail! the order is refunding by another request', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 81020, 'status' => 'invalid args! page_id.size!=0 but all==true, or page_id.size==0 but all==false.', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81021, 'status' => 'empty theme_pic_url.', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81022, 'status' => 'empty theme.title.', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81023, 'status' => 'empty theme.title_title.', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81024, 'status' => 'empty theme.item_list.', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81025, 'status' => 'empty theme.pic_item_list.', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 81026, 'status' => 'invalid theme.title.length .', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81027, 'status' => 'empty background_pic_url.', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81028, 'status' => 'empty default_gifting_msg.', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81029, 'status' => 'duplicate order_id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81030, 'status' => 'prealloc code fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 81031, 'status' => 'too many theme participate_activity', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82000, 'status' => 'biz_template_id not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82001, 'status' => 'result_page_style_id not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82002, 'status' => 'deal_msg_style_id not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82003, 'status' => 'card_style_id not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82010, 'status' => 'biz template not audit ok', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82011, 'status' => 'biz template banned', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82020, 'status' => 'user not use service first', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82021, 'status' => 'exceed long period', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82022, 'status' => 'exceed long period max send cnt', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82023, 'status' => 'exceed short period max send cnt', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82024, 'status' => 'exceed data size limit', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 82025, 'status' => 'invalid url', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82026, 'status' => 'service disabled', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 82027, 'status' => 'invalid miniprogram appid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82100, 'status' => 'wx_cs_code should not be empty.', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 82101, 'status' => 'wx_cs_code is invalid.', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82102, 'status' => 'wx_cs_code is expire.', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82103, 'status' => 'user_ip should not be empty.', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 82200, 'status' => '', 'reason' => '公众平台账号与服务 id 不匹配', 'explain' => '公众平台账号与服务 id 不匹配'],
        ['code' => 400, 'errcode' => 82201, 'status' => '', 'reason' => '请勿重复添加停车场', 'explain' => '该停车场已存在，请勿重复添加'],
        ['code' => 400, 'errcode' => 82202, 'status' => '', 'reason' => '请先导入停车场信息', 'explain' => '该停车场信息不存在，请先导入'],
        ['code' => 400, 'errcode' => 82203, 'status' => '', 'reason' => '停车场价格格式不正确', 'explain' => '停车场价格格式不正确'],
        ['code' => 400, 'errcode' => 82204, 'status' => '', 'reason' => 'appid与 code 不匹配', 'explain' => 'appid与 code 不匹配'],
        ['code' => 400, 'errcode' => 82205, 'status' => '', 'reason' => 'wx_park_code字段为空', 'explain' => 'wx_park_code字段为空'],
        ['code' => 400, 'errcode' => 82206, 'status' => '', 'reason' => 'wx_park_code无效或已过期', 'explain' => 'wx_park_code无效或已过期'],
        ['code' => 400, 'errcode' => 82207, 'status' => '', 'reason' => '电话字段为空', 'explain' => '电话字段为空'],
        ['code' => 400, 'errcode' => 82208, 'status' => '', 'reason' => '关闭时间格式不正确', 'explain' => '关闭时间格式不正确'],
        ['code' => 400, 'errcode' => 82300, 'status' => '', 'reason' => '该 appid 不支持开通城市服务插件', 'explain' => '该 appid 不支持开通城市服务插件'],
        ['code' => 400, 'errcode' => 82301, 'status' => '', 'reason' => '添加插件失败', 'explain' => '添加插件失败'],
        ['code' => 400, 'errcode' => 82302, 'status' => '', 'reason' => '未添加城市服务插件', 'explain' => '未添加城市服务插件'],
        ['code' => 412, 'errcode' => 82303, 'status' => 'invalid fileid', 'reason' => 'fileid无效', 'explain' => 'fileid无效'],
        ['code' => 400, 'errcode' => 82304, 'status' => '', 'reason' => '临时文件过期', 'explain' => '临时文件过期'],
        ['code' => 400, 'errcode' => 83000, 'status' => 'there is some param not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83001, 'status' => 'system error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83002, 'status' => 'create_url_sence_failed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83003, 'status' => 'appid maybe error or retry', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83004, 'status' => 'create appid auth failed or retry', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83005, 'status' => 'wxwebencrytoken errro', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83006, 'status' => 'wxwebencrytoken expired or no exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83007, 'status' => 'wxwebencrytoken expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83008, 'status' => 'wxwebencrytoken no auth', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83009, 'status' => 'wxwebencrytoken not the mate with openid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83200, 'status' => 'no exist service', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83201, 'status' => 'uin has not the service', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83202, 'status' => 'params is not json or not json array', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83203, 'status' => 'params num exceed 10', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83204, 'status' => 'object has not key', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83205, 'status' => 'key is not string', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83206, 'status' => 'object has not value', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83207, 'status' => 'value is not string', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83208, 'status' => 'key or value is empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 83209, 'status' => 'key exist repeated', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 84001, 'status' => 'invalid identify id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 84002, 'status' => 'user data expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 84003, 'status' => 'user data not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 84004, 'status' => 'video upload fail!', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 84005, 'status' => 'video download fail! please try again', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 84006, 'status' => 'name or id_card_number empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85001, 'status' => 'user not exist or user cannot be searched', 'reason' => '微信号不存在或微信号设置为不可搜索', 'explain' => '微信号不存在或微信号设置为不可搜索'],
        ['code' => 400, 'errcode' => 85002, 'status' => 'number of tester reach bind limit', 'reason' => '小程序绑定的体验者数量达到上限', 'explain' => '小程序绑定的体验者数量达到上限'],
        ['code' => 400, 'errcode' => 85003, 'status' => 'user already bind too many weapps', 'reason' => '微信号绑定的小程序体验者达到上限', 'explain' => '微信号绑定的小程序体验者达到上限'],
        ['code' => 400, 'errcode' => 85004, 'status' => 'user already bind', 'reason' => '微信号已经绑定', 'explain' => '微信号已经绑定'],
        ['code' => 400, 'errcode' => 85005, 'status' => 'appid not bind weapp', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 85006, 'status' => 'tag is in invalid format', 'reason' => '标签格式错误', 'explain' => '标签格式错误'],
        ['code' => 412, 'errcode' => 85007, 'status' => 'page is in invalid format', 'reason' => '页面路径错误', 'explain' => '页面路径错误'],
        ['code' => 412, 'errcode' => 85008, 'status' => 'category is in invalid format', 'reason' => '请添加类目成功后重试', 'explain' => '当前小程序没有已经审核通过的类目，请添加类目成功后重试'],
        ['code' => 400, 'errcode' => 85009, 'status' => 'already submit a version under auditing', 'reason' => '已经有正在审核的版本', 'explain' => '已经有正在审核的版本'],
        ['code' => 400, 'errcode' => 85010, 'status' => 'missing required data', 'reason' => 'item_list 有项目为空', 'explain' => 'item_list 有项目为空'],
        ['code' => 412, 'errcode' => 85011, 'status' => 'title is in invalid format', 'reason' => '标题填写错误', 'explain' => '标题填写错误'],
        ['code' => 412, 'errcode' => 85012, 'status' => 'invalid audit id', 'reason' => '无效的审核 id', 'explain' => '无效的审核 id'],
        ['code' => 412, 'errcode' => 85013, 'status' => 'invalid ext_json, parse fail or containing invalid path', 'reason' => '无效的自定义配置', 'explain' => '无效的自定义配置'],
        ['code' => 400, 'errcode' => 85014, 'status' => 'template not exist', 'reason' => '无效的模板编号', 'explain' => '无效的模板编号'],
        ['code' => 400, 'errcode' => 85015, 'status' => '', 'reason' => '该账号不是小程序账号', 'explain' => '该账号不是小程序账号'],
        ['code' => 400, 'errcode' => 85016, 'status' => 'exceed valid domain count', 'reason' => '域名数量超过限制', 'explain' => '域名数量超过限制 ，总数不能超过1000'],
        ['code' => 400, 'errcode' => 85017, 'status' => 'no domain to modify after filtered, please confirm the domain has been set in miniprogram or open', 'reason' => '',
         'explain' => '没有新增域名，请确认小程序已经添加了域名或该域名是否没有在第三方平台添加'],
        ['code' => 400, 'errcode' => 85018, 'status' => '', 'reason' => '域名没有在第三方平台设置', 'explain' => '域名没有在第三方平台设置'],
        ['code' => 400, 'errcode' => 85019, 'status' => 'no version is under auditing', 'reason' => '没有审核版本', 'explain' => '没有审核版本'],
        ['code' => 400, 'errcode' => 85020, 'status' => 'status not allowed', 'reason' => '审核状态未满足发布', 'explain' => '审核状态未满足发布'],
        ['code' => 400, 'errcode' => 85021, 'status' => 'status not allowed', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 85022, 'status' => 'invalid action', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85023, 'status' => 'item size is not in valid range', 'reason' => '审核列表填写的项目数不在 1-5 以内', 'explain' => '审核列表填写的项目数不在 1-5 以内'],
        ['code' => 400, 'errcode' => 85024, 'status' => 'need complete material', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85025, 'status' => 'this phone reach bind limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85026, 'status' => 'this wechat account reach bind limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85027, 'status' => 'this idcard reach bind limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85028, 'status' => 'this contractor reach bind limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85029, 'status' => 'nickname has used', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 85030, 'status' => 'invalid nickname size(4-30)', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85031, 'status' => 'nickname is forbidden', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85032, 'status' => 'nickname is complained', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85033, 'status' => 'nickname is illegal', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85034, 'status' => 'nickname is protected', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85035, 'status' => 'nickname is forbidden for different contractor', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85036, 'status' => 'introduction is illegal', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85038, 'status' => 'store has added', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85039, 'status' => 'store has added by others', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85040, 'status' => 'store has added by yourseld', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85041, 'status' => 'credential has used', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85042, 'status' => 'nearby reach limit', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 85043, 'status' => 'invalid template, something wrong?', 'reason' => '模板错误', 'explain' => '模板错误'],
        ['code' => 400, 'errcode' => 85044, 'status' => 'package exceed max limit', 'reason' => '代码包超过大小限制', 'explain' => '代码包超过大小限制'],
        ['code' => 400, 'errcode' => 85045, 'status' => 'some path in ext_json not exist', 'reason' => 'ext_json 有不存在的路径', 'explain' => 'ext_json 有不存在的路径'],
        ['code' => 400, 'errcode' => 85046, 'status' => 'page path missing in tabbar list', 'reason' => 'tabbar 中缺少 path', 'explain' => 'tabbar 中缺少 path'],
        ['code' => 400, 'errcode' => 85047, 'status' => 'pages are empty', 'reason' => 'pages 字段为空', 'explain' => 'pages 字段为空'],
        ['code' => 400, 'errcode' => 85048, 'status' => 'parse ext_json fail', 'reason' => 'ext_json 解析失败', 'explain' => 'ext_json 解析失败'],
        ['code' => 400, 'errcode' => 85049, 'status' => 'reach headimg or introduction quota limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85050, 'status' => 'verifying, dont apply again', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85051, 'status' => 'data too large', 'reason' => 'version_desc或者preview_info超限', 'explain' => 'version_desc或者preview_info超限'],
        ['code' => 400, 'errcode' => 85052, 'status' => 'app is already released', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85053, 'status' => 'please apply merchant first', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85054, 'status' => 'poi_id is null, please upgrade first', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 85055, 'status' => 'map_poi_id is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 85056, 'status' => 'mediaid is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 85057, 'status' => 'invalid widget data format', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85058, 'status' => 'no valid audit_id exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85059, 'status' => 'overseas access deny', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 85060, 'status' => 'invalid taskid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85061, 'status' => 'this phone reach bind limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85062, 'status' => 'this phone in black list', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85063, 'status' => 'idcard in black list', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85064, 'status' => 'template not found', 'reason' => '找不到模板', 'explain' => '找不到模板'],
        ['code' => 400, 'errcode' => 85065, 'status' => 'template list is full', 'reason' => '模板库已满', 'explain' => '模板库已满'],
        ['code' => 400, 'errcode' => 85066, 'status' => 'illegal prefix', 'reason' => '链接错误', 'explain' => '链接错误'],
        ['code' => 400, 'errcode' => 85067, 'status' => 'input data error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85068, 'status' => 'test url is not the sub prefix', 'reason' => '测试链接不是子链接', 'explain' => '测试链接不是子链接'],
        ['code' => 400, 'errcode' => 85069, 'status' => 'check confirm file fail', 'reason' => '校验文件失败', 'explain' => '校验文件失败'],
        ['code' => 400, 'errcode' => 85070, 'status' => 'prefix in black list', 'reason' => '个人类型小程序无法设置二维码规则', 'explain' => '个人类型小程序无法设置二维码规则'],
        ['code' => 400, 'errcode' => 85071, 'status' => 'prefix added repeated', 'reason' => '请勿重复添加链接', 'explain' => '已添加该链接，请勿重复添加'],
        ['code' => 400, 'errcode' => 85072, 'status' => 'prefix owned by other', 'reason' => '该链接已被占用', 'explain' => '该链接已被占用'],
        ['code' => 400, 'errcode' => 85073, 'status' => 'prefix beyond limit', 'reason' => '二维码规则已满', 'explain' => '二维码规则已满'],
        ['code' => 400, 'errcode' => 85074, 'status' => 'not published', 'reason' => '小程序未发布', 'explain' => '小程序未发布, 小程序必须先发布代码才可以发布二维码跳转规则'],
        ['code' => 400, 'errcode' => 85075, 'status' => 'can not access', 'reason' => '个人类型小程序无法设置二维码规则', 'explain' => '个人类型小程序无法设置二维码规则'],
        ['code' => 400, 'errcode' => 85077, 'status' => 'some category you choose is no longger supported, please choose other category',
         'reason' => '小程序类目信息失效', 'explain' => '小程序类目信息失效（类目中含有官方下架的类目，请重新选择类目）'],
        ['code' => 400, 'errcode' => 85078, 'status' => 'operator info error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85079, 'status' => 'miniprogram has no online release', 'reason' => '小程序没有线上版本，不能进行灰度', 'explain' => '小程序没有线上版本，不能进行灰度'],
        ['code' => 400, 'errcode' => 85080, 'status' => 'miniprogram commit not approved', 'reason' => '小程序审核未通过', 'explain' => '小程序提交的审核未审核通过'],
        ['code' => 412, 'errcode' => 85081, 'status' => 'invalid gray percentage', 'reason' => '无效的发布比例', 'explain' => '无效的发布比例'],
        ['code' => 400, 'errcode' => 85082, 'status' => 'gray percentage too low', 'reason' => '当前的发布比例需要比之前设置的高', 'explain' => '当前的发布比例需要比之前设置的高'],
        ['code' => 400, 'errcode' => 85083, 'status' => 'search status is banned', 'reason' => '搜索标记位被封禁，无法修改', 'explain' => '搜索标记位被封禁，无法修改'],
        ['code' => 412, 'errcode' => 85084, 'status' => 'search status invalid', 'reason' => '非法的 status 值', 'explain' => '非法的 status 值，只能填 0 或者 1'],
        ['code' => 400, 'errcode' => 85085, 'status' => 'submit audit reach limit pleasetry later', 'reason' => '小程序提审数量已达本月上限',
         'explain' => '小程序提审数量已达本月上限，请点击查看《自助临时申请额度》'],
        ['code' => 400, 'errcode' => 85086, 'status' => 'must commit before submit audit', 'reason' => '提交代码审核之前需提前上传代码', 'explain' => '提交代码审核之前需提前上传代码'],
        ['code' => 400, 'errcode' => 85087, 'status' => 'navigatetominiprogram appid list empty', 'reason' => '小程序已使用 api navigatetominiprogram',
         'explain' => '小程序已使用 api navigatetominiprogram，请声明跳转 appid 列表后再次提交'],
        ['code' => 400, 'errcode' => 85088, 'status' => 'no qbase privilege', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85089, 'status' => 'config not found', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85090, 'status' => 'wait and commit for this exappid later', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 85091, 'status' => 'search status was turned off', 'reason' => '小程序的搜索开关被关闭。请访问设置页面打开开关再重试',
         'explain' => '小程序的搜索开关被关闭。请访问设置页面打开开关再重试'],
        ['code' => 412, 'errcode' => 85092, 'status' => 'invalid preview_info format', 'reason' => 'preview_info格式错误', 'explain' => 'preview_info格式错误'],
        ['code' => 412, 'errcode' => 85093, 'status' => 'invalid preview_info size', 'reason' => 'preview_info 视频或者图片个数超限', 'explain' => 'preview_info 视频或者图片个数超限'],
        ['code' => 400, 'errcode' => 85094, 'status' => 'need add ugc declare', 'reason' => '需提供审核机制说明信息', 'explain' => '需提供审核机制说明信息'],
        ['code' => 400, 'errcode' => 85101, 'status' => '', 'reason' => '小程序不能发送该运动类型或运动类型不存在', 'explain' => '小程序不能发送该运动类型或运动类型不存在'],
        ['code' => 400, 'errcode' => 85102, 'status' => '', 'reason' => '数值异常', 'explain' => '数值异常'],
        ['code' => 400, 'errcode' => 86000, 'status' => 'should be called only from third party', 'reason' => '不是由第三方代小程序进行调用', 'explain' => '不是由第三方代小程序进行调用'],
        ['code' => 400, 'errcode' => 86001, 'status' => 'component experience version not exists', 'reason' => '不存在第三方的已经提交的代码', 'explain' => '不存在第三方的已经提交的代码'],
        ['code' => 400, 'errcode' => 86002, 'status' => 'miniprogram have not completed init procedure', 'reason' => '小程序还未设置昵称、头像、简介。请先设置完后再重新提交',
         'explain' => '小程序还未设置昵称、头像、简介。请先设置完后再重新提交'],
        ['code' => 400, 'errcode' => 86003, 'status' => 'component do not has category mall', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 86004, 'status' => 'invalid wechat', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 86005, 'status' => 'wechat limit frequency', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 86006, 'status' => 'has no quota to send group msg', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 86007, 'status' => '', 'reason' => '小程序禁止提交', 'explain' => '小程序禁止提交'],
        ['code' => 400, 'errcode' => 86008, 'status' => '', 'reason' => '服务商被处罚，限制全部代码提审能力', 'explain' => '服务商被处罚，限制全部代码提审能力'],
        ['code' => 400, 'errcode' => 86009, 'status' => '', 'reason' => '服务商新增小程序代码提审能力被限制', 'explain' => '服务商新增小程序代码提审能力被限制'],
        ['code' => 400, 'errcode' => 86010, 'status' => '', 'reason' => '服务商迭代小程序代码提审能力被限制', 'explain' => '服务商迭代小程序代码提审能力被限制'],
        ['code' => 400, 'errcode' => 87006, 'status' => 'this is game miniprogram, submit audit is forbidden', 'reason' => '小游戏不能提交', 'explain' => '小游戏不能提交'],
        ['code' => 400, 'errcode' => 87007, 'status' => 'session_key is not existd or expired', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 87008, 'status' => 'invalid sig_method', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 87009, 'status' => 'Invalid signature', 'reason' => '无效的签名', 'explain' => ''],
        ['code' => 412, 'errcode' => 87010, 'status' => 'invalid buffer size', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 87011, 'status' => 'wxa has a gray release plan, forbid revert release', 'reason' => '现网已经在灰度发布，不能进行版本回退', 'explain' => '现网已经在灰度发布，不能进行版本回退'],
        ['code' => 400, 'errcode' => 87012, 'status' => 'forbid revert this version release', 'reason' => '该版本不能回退',
         'explain' => '该版本不能回退，可能的原因：1:无上一个线上版用于回退 2:此版本为已回退版本，不能回退 3:此版本为回退功能上线之前的版本，不能回退'],
        ['code' => 400, 'errcode' => 87013, 'status' => 'no quota to undo code', 'reason' => '撤回次数达到上限', 'explain' => '撤回次数达到上限（每天5次，每个月 10 次）'],
        ['code' => 400, 'errcode' => 87014, 'status' => 'risky content', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 87015, 'status' => 'query timeout, try a content with less size', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 87016, 'status' => 'some key-value in list meet length exceed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 87017, 'status' => 'user storage size exceed, delete some keys and try again', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 87018, 'status' => 'user has stored too much keys. delete some keys and try again', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 87019, 'status' => 'some keys in list meet length exceed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 87080, 'status' => 'need friend', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 87081, 'status' => 'invalid openid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 87082, 'status' => 'invalid key', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 87083, 'status' => 'invalid operation', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 87084, 'status' => 'invalid opnum', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 87085, 'status' => 'check fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 88000, 'status' => 'without comment privilege', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 88001, 'status' => 'msg_data is not exists', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 88002, 'status' => 'the article is limit for safety', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 88003, 'status' => 'elected comment upper limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 88004, 'status' => 'comment was deleted by user', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 88005, 'status' => 'already reply', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 88007, 'status' => 'reply content beyond max len or content len is zero', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 88008, 'status' => 'comment is not exists', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 88009, 'status' => 'reply is not exists', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 88010, 'status' => 'count range error. cout <= 0 or count > 50', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 88011, 'status' => 'the article is limit for safety', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89000, 'status' => 'account has bound open', 'reason' => '该公众号/小程序已经绑定了开放平台账号', 'explain' => '该公众号/小程序已经绑定了开放平台账号'],
        ['code' => 400, 'errcode' => 89001, 'status' => 'not same contractor', 'reason' => 'authorizer 与开放平台账号主体不相同', 'explain' => 'authorizer 与开放平台账号主体不相同'],
        ['code' => 400, 'errcode' => 89002, 'status' => 'open not exists', 'reason' => '该公众号/小程序未绑定微信开放平台账号。', 'explain' => '该公众号/小程序未绑定微信开放平台账号。'],
        ['code' => 400, 'errcode' => 89003, 'status' => 'open is not created by api', 'reason' => '该开放平台账号并非通过 api 创建，不允许操作', 'explain' => '该开放平台账号并非通过 api 创建，不允许操作'],
        ['code' => 400, 'errcode' => 89004, 'status' => '', 'reason' => '该开放平台账号所绑定的公众号/小程序已达上限', 'explain' => '该开放平台账号所绑定的公众号/小程序已达上限（100 个）'],
        ['code' => 400, 'errcode' => 89005, 'status' => 'without add video ability, the ability was banned', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89006, 'status' => 'without upload video ability, the ability was banned', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89007, 'status' => 'wxa quota limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89008, 'status' => 'overseas account can not link', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89009, 'status' => 'wxa reach link limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89010, 'status' => 'link message has sent', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89011, 'status' => 'can not unlink nearby wxa', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89012, 'status' => 'can not unlink store or mall', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89013, 'status' => 'wxa is banned', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89014, 'status' => 'support version error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89015, 'status' => 'has linked wxa', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89016, 'status' => 'reach same realname quota', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89017, 'status' => 'reach different realname quota', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89018, 'status' => 'unlink message has sent', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89019, 'status' => 'webview domain not change', 'reason' => '请勿重复设置业务域名', 'explain' => '业务域名无更改，无需重复设置'],
        ['code' => 400, 'errcode' => 89020, 'status' => 'opens webview domain is null! need to set opens webview domain first!', 'reason' => '未设置小程序业务域名',
         'explain' => '尚未设置小程序业务域名，请先在第三方平台中设置小程序业务域名后在调用本接口'],
        ['code' => 400, 'errcode' => 89021, 'status' => 'request domain is not opens webview domain!', 'reason' => '请求保存的域名不是第三方平台中已设置的小程序业务域名或子域名',
         'explain' => '请求保存的域名不是第三方平台中已设置的小程序业务域名或子域名'],
        ['code' => 400, 'errcode' => 89022, 'status' => 'delete domain is not exist!', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89029, 'status' => 'webview domain exceed limit', 'reason' => '业务域名数量超过限制', 'explain' => '业务域名数量超过限制，最多可以添加100个业务域名'],
        ['code' => 400, 'errcode' => 89030, 'status' => 'operation reach month limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89031, 'status' => 'user bind reach limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89032, 'status' => 'weapp bind members reach limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89033, 'status' => 'empty wx or openid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 89034, 'status' => 'userstr is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89035, 'status' => 'linking from mp', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89231, 'status' => 'not support single', 'reason' => '个人小程序不支持调用 setwebviewdomain 接口', 'explain' => ''],
        ['code' => 400, 'errcode' => 89235, 'status' => 'hit black contractor', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89236, 'status' => 'this plugin can not apply', 'reason' => '该插件不能申请', 'explain' => '该插件不能申请'],
        ['code' => 400, 'errcode' => 89237, 'status' => 'plugin has send apply message or already bind', 'reason' => '已经添加该插件', 'explain' => '已经添加该插件'],
        ['code' => 400, 'errcode' => 89238, 'status' => 'plugin count reach limit', 'reason' => '申请或使用的插件已经达到上限', 'explain' => '申请或使用的插件已经达到上限'],
        ['code' => 400, 'errcode' => 89239, 'status' => 'plugin no exist', 'reason' => '该插件不存在', 'explain' => '该插件不存在'],
        ['code' => 400, 'errcode' => 89240, 'status' => 'only applying status can be agreed or refused', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89241, 'status' => 'only refused status can be deleted, please refused first', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89242, 'status' => 'appid is no in the apply list, make sure appid is right', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89243, 'status' => 'can not delete apply request in 24 hours', 'reason' => '该申请为“待确认”状态，不可删除', 'explain' => '该申请为“待确认”状态，不可删除'],
        ['code' => 400, 'errcode' => 89244, 'status' => 'plugin appid is no in the plugin list', 'reason' => '不存在该插件 appid', 'explain' => ''],
        ['code' => 400, 'errcode' => 89245, 'status' => 'mini program forbidden to link', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89246, 'status' => 'plugins with special category are used only by specific apps', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89247, 'status' => 'inner error, retry after some while', 'reason' => '系统内部错误', 'explain' => '系统内部错误'],
        ['code' => 412, 'errcode' => 89248, 'status' => 'invalid code type', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89249, 'status' => 'task running', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 89250, 'status' => 'inner error, retry after some while', 'reason' => '内部错误', 'explain' => '内部错误'],
        ['code' => 400, 'errcode' => 89251, 'status' => 'legal person checking', 'reason' => '模板消息已下发，待法人人脸核身校验', 'explain' => '模板消息已下发，待法人人脸核身校验'],
        ['code' => 400, 'errcode' => 89253, 'status' => 'front checking', 'reason' => '法人&企业信息一致性校验中', 'explain' => '法人&企业信息一致性校验中'],
        ['code' => 400, 'errcode' => 89254, 'status' => 'lack of some component rights', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 89255, 'status' => 'enterprise code invalid', 'reason' => 'code参数无效',
         'explain' => 'code参数无效，请检查 code 长度以及内容是否正确；注意code_type的值不同需要传的 code 长度不一样'],
        ['code' => 400, 'errcode' => 89256, 'status' => 'no component info', 'reason' => 'token 信息有误', 'explain' => 'token 信息有误'],
        ['code' => 400, 'errcode' => 89257, 'status' => 'no such version', 'reason' => '该插件版本不支持快速更新', 'explain' => '该插件版本不支持快速更新'],
        ['code' => 400, 'errcode' => 89258, 'status' => 'code is gray online', 'reason' => '当前小程序账号存在灰度发布中的版本，不可操作快速更新', 'explain' => '当前小程序账号存在灰度发布中的版本，不可操作快速更新'],
        ['code' => 400, 'errcode' => 89259, 'status' => 'zhibo plugin is not allow to delete', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 89300, 'status' => 'invalid trade', 'reason' => '无效的订单', 'explain' => '订单无效'],
        ['code' => 400, 'errcode' => 89401, 'status' => '', 'reason' => '系统不稳定，请稍后再试', 'explain' => '系统不稳定，请稍后再试，如多次失败请通过社区反馈'],
        ['code' => 400, 'errcode' => 89402, 'status' => '', 'reason' => '该小程序不在待审核队列', 'explain' => '该小程序不在待审核队列，请检查是否已提交审核或已审完'],
        ['code' => 400, 'errcode' => 89403, 'status' => '', 'reason' => '请等待正常审核流程', 'explain' => '本单属于平台不支持加急种类，请等待正常审核流程'],
        ['code' => 400, 'errcode' => 89404, 'status' => '', 'reason' => '请勿重复提交', 'explain' => '本单已加速成功，请勿重复提交'],
        ['code' => 400, 'errcode' => 89405, 'status' => '', 'reason' => '本月加急额度已用完，请提高提审质量以获取更多额度', 'explain' => '本月加急额度已用完，请提高提审质量以获取更多额度'],
        ['code' => 400, 'errcode' => 89501, 'status' => '', 'reason' => '公众号有未处理的确认请求，请稍候重试', 'explain' => '公众号有未处理的确认请求，请稍候重试'],
        ['code' => 400, 'errcode' => 89502, 'status' => '', 'reason' => '请耐心等待管理员确认', 'explain' => '请耐心等待管理员确认'],
        ['code' => 400, 'errcode' => 89503, 'status' => '', 'reason' => '此次调用需要管理员确认，请耐心等候', 'explain' => '此次调用需要管理员确认，请耐心等候'],
        ['code' => 400, 'errcode' => 89504, 'status' => '', 'reason' => '正在等管理员确认，请耐心等待', 'explain' => '正在等管理员确认，请耐心等待'],
        ['code' => 400, 'errcode' => 89505, 'status' => '', 'reason' => '正在等管理员确认，请稍候重试', 'explain' => '正在等管理员确认，请稍候重试'],
        ['code' => 400, 'errcode' => 89506, 'status' => '', 'reason' => '该 ip 调用求请求已被公众号管理员拒绝', 'explain' => '该 ip 调用求请求已被公众号管理员拒绝，请24小时后再试，建议调用前与管理员沟通确认'],
        ['code' => 400, 'errcode' => 89507, 'status' => '', 'reason' => '该 ip 调用求请求已被公众号管理员拒绝', 'explain' => '该 ip 调用求请求已被公众号管理员拒绝，请1小时后再试，建议调用前与管理员沟通确认'],
        ['code' => 412, 'errcode' => 90001, 'status' => 'invalid order id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 90002, 'status' => 'invalid busi id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 90003, 'status' => 'invalid bill date', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 90004, 'status' => 'invalid bill type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 90005, 'status' => 'invalid platform', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 90006, 'status' => 'bill not exists', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 90007, 'status' => 'invalid openid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 90009, 'status' => 'mp_sig error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 90010, 'status' => 'no session', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 90011, 'status' => 'sig error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 90012, 'status' => 'order exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 90013, 'status' => 'balance not enough', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 90014, 'status' => 'order has been confirmed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 90015, 'status' => 'order has been canceled', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 90016, 'status' => 'order is being processed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 90017, 'status' => 'no privilege', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 90018, 'status' => 'invalid parameter', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 91001, 'status' => 'not fast register', 'reason' => '不是复用公众号接口快速创建的小程序', 'explain' => '不是复用公众号接口快速创建的小程序 ，也不是通过快速注册企业小程序 api 创建的小程序'],
        ['code' => 400, 'errcode' => 91002, 'status' => 'has published', 'reason' => '小程序发布后不可改名', 'explain' => '小程序发布后不可改名'],
        ['code' => 412, 'errcode' => 91003, 'status' => 'invalid change stat', 'reason' => '改名状态不合法', 'explain' => '改名状态不合法'],
        ['code' => 412, 'errcode' => 91004, 'status' => 'invalid nickname', 'reason' => '昵称不合法', 'explain' => '昵称不合法'],
        ['code' => 400, 'errcode' => 91005, 'status' => 'nickname protected', 'reason' => '昵称 15 天主体保护', 'explain' => '昵称 15 天主体保护'],
        ['code' => 400, 'errcode' => 91006, 'status' => 'nickname used by username', 'reason' => '昵称命中微信号', 'explain' => '昵称命中微信号'],
        ['code' => 400, 'errcode' => 91007, 'status' => 'nickname used', 'reason' => '昵称已被占用', 'explain' => '昵称已被占用'],
        ['code' => 400, 'errcode' => 91008, 'status' => 'nickname protected', 'reason' => '昵称命中 7 天侵权保护期', 'explain' => '昵称命中 7 天侵权保护期'],
        ['code' => 400, 'errcode' => 91009, 'status' => 'nickname need proof', 'reason' => '需要提交材料', 'explain' => '需要提交材料'],
        ['code' => 400, 'errcode' => 91010, 'status' => '', 'reason' => '其他错误', 'explain' => '其他错误'],
        ['code' => 400, 'errcode' => 91011, 'status' => '', 'reason' => '查不到昵称修改审核单信息', 'explain' => '查不到昵称修改审核单信息'],
        ['code' => 400, 'errcode' => 91012, 'status' => '', 'reason' => '其它错误', 'explain' => '其它错误'],
        ['code' => 400, 'errcode' => 91013, 'status' => 'lock name too more', 'reason' => '占用名字过多', 'explain' => '占用名字过多'],
        ['code' => 400, 'errcode' => 91014, 'status' => 'diff master plus', 'reason' => '同一类型关联名主体不一致', 'explain' => '+号规则 同一类型关联名主体不一致'],
        ['code' => 400, 'errcode' => 91015, 'status' => 'diff master', 'reason' => '原始名不同类型主体不一致', 'explain' => '原始名不同类型主体不一致'],
        ['code' => 400, 'errcode' => 91016, 'status' => 'name more owner', 'reason' => '名称占用者 ≥2', 'explain' => '名称占用者 ≥2'],
        ['code' => 400, 'errcode' => 91017, 'status' => 'other diff master plus', 'reason' => '不同类型关联名主体不一致', 'explain' => '+号规则 不同类型关联名主体不一致'],
        ['code' => 400, 'errcode' => 91018, 'status' => '', 'reason' => '请重新认证改名', 'explain' => '组织类型小程序发布后，侵权被清空昵称，需走认证改名'],
        ['code' => 400, 'errcode' => 91019, 'status' => '', 'reason' => '小程序正在审核中', 'explain' => '小程序正在审核中'],
        ['code' => 400, 'errcode' => 92000, 'status' => '', 'reason' => '请勿重复添加经营资质', 'explain' => '该经营资质已添加，请勿重复添加'],
        ['code' => 400, 'errcode' => 92002, 'status' => '', 'reason' => '附近地点添加数量达到上线，无法继续添加', 'explain' => '附近地点添加数量达到上线，无法继续添加'],
        ['code' => 400, 'errcode' => 92003, 'status' => '', 'reason' => '地点已被其它小程序占用', 'explain' => '地点已被其它小程序占用'],
        ['code' => 400, 'errcode' => 92004, 'status' => '', 'reason' => '附近功能被封禁', 'explain' => '附近功能被封禁'],
        ['code' => 400, 'errcode' => 92005, 'status' => '', 'reason' => '地点正在审核中', 'explain' => '地点正在审核中'],
        ['code' => 400, 'errcode' => 92006, 'status' => '', 'reason' => '地点正在展示小程序', 'explain' => '地点正在展示小程序'],
        ['code' => 400, 'errcode' => 92007, 'status' => '', 'reason' => '地点审核失败', 'explain' => '地点审核失败'],
        ['code' => 400, 'errcode' => 92008, 'status' => '', 'reason' => '小程序未展示在该地点', 'explain' => '小程序未展示在该地点'],
        ['code' => 400, 'errcode' => 93009, 'status' => '', 'reason' => '小程序未上架', 'explain' => '小程序未上架或不可见'],
        ['code' => 400, 'errcode' => 93010, 'status' => '', 'reason' => '地点不存在', 'explain' => '地点不存在'],
        ['code' => 400, 'errcode' => 93011, 'status' => '', 'reason' => '个人类型小程序不可用', 'explain' => '个人类型小程序不可用'],
        ['code' => 400, 'errcode' => 93012, 'status' => '', 'reason' => '非普通类型小程序', 'explain' => '非普通类型小程序（门店小程序、小店小程序等）不可用'],
        ['code' => 400, 'errcode' => 93013, 'status' => '', 'reason' => '从腾讯地图获取地址详细信息失败', 'explain' => '从腾讯地图获取地址详细信息失败'],
        ['code' => 400, 'errcode' => 93014, 'status' => '', 'reason' => '请勿重复添加同一资质证件号', 'explain' => '同一资质证件号重复添加'],
        ['code' => 400, 'errcode' => 93015, 'status' => '', 'reason' => '附近类目审核中', 'explain' => '附近类目审核中'],
        ['code' => 400, 'errcode' => 93016, 'status' => '', 'reason' => '服务标签个数超限制', 'explain' => '服务标签个数超限制（官方最多5个，自定义最多4个）'],
        ['code' => 400, 'errcode' => 93017, 'status' => '', 'reason' => '服务标签或者客服的名字不符合要求', 'explain' => '服务标签或者客服的名字不符合要求'],
        ['code' => 400, 'errcode' => 93018, 'status' => '', 'reason' => '服务能力中填写的小程序 appid 不是同主体小程序', 'explain' => '服务能力中填写的小程序 appid 不是同主体小程序'],
        ['code' => 400, 'errcode' => 93019, 'status' => '', 'reason' => '申请类目之后才能添加附近地点', 'explain' => '申请类目之后才能添加附近地点'],
        ['code' => 400, 'errcode' => 93020, 'status' => '', 'reason' => 'qualification_list无效', 'explain' => 'qualification_list无效'],
        ['code' => 400, 'errcode' => 93021, 'status' => '', 'reason' => 'company_name字段为空', 'explain' => 'company_name字段为空'],
        ['code' => 400, 'errcode' => 93022, 'status' => '', 'reason' => 'credential字段为空', 'explain' => 'credential字段为空'],
        ['code' => 400, 'errcode' => 93023, 'status' => 'address field is empty', 'reason' => 'address字段为空', 'explain' => 'address字段为空'],
        ['code' => 400, 'errcode' => 93024, 'status' => '', 'reason' => 'qualification_list字段为空', 'explain' => 'qualification_list字段为空'],
        ['code' => 400, 'errcode' => 93025, 'status' => '', 'reason' => '服务 appid 对应的 path 不存在', 'explain' => '服务 appid 对应的 path 不存在'],
        ['code' => 400, 'errcode' => 94001, 'status' => 'missing cert_serialno', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 94002, 'status' => 'use not register wechat pay', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 94003, 'status' => 'invalid sign', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 94004, 'status' => 'user do not has real name info', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 94005, 'status' => 'invalid user token', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 94006, 'status' => 'appid unauthorized', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 94007, 'status' => 'appid unbind mchid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 94008, 'status' => 'invalid timestamp', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 94009, 'status' => 'invalid cert_serialno, cert_serialnos size should be 40', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 94010, 'status' => 'invalid mch_id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 94011, 'status' => 'timestamp expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 94012, 'status' => '', 'reason' => 'appid和商户号的绑定关系不存在', 'explain' => 'appid和商户号的绑定关系不存在'],
        ['code' => 400, 'errcode' => 95001, 'status' => 'wxcode decode fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 95002, 'status' => 'wxcode recognize unautuorized', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 95101, 'status' => 'get product by page args invalid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 95102, 'status' => 'get product materials by cond args invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 95103, 'status' => 'material id list size out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 95104, 'status' => 'import product frequence out of limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 95105, 'status' => 'mp is importing products, api is rejected to import', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 95106, 'status' => 'api is rejected to import, need to set commission ratio on mp first', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 101000, 'status' => 'invalid image url', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 101001, 'status' => 'certificate not found', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 101002, 'status' => 'not enough market quota', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 200002, 'status' => '', 'reason' => '入参错误', 'explain' => '入参错误'],
        ['code' => 400, 'errcode' => 200011, 'status' => '', 'reason' => '此账号已被封禁，无法操作', 'explain' => '此账号已被封禁，无法操作'],
        ['code' => 400, 'errcode' => 200012, 'status' => '', 'reason' => '个人模板数已达上限', 'explain' => '个人模板数已达上限，上限25个'],
        ['code' => 400, 'errcode' => 200013, 'status' => '', 'reason' => '此模板已被封禁，无法选用', 'explain' => '此模板已被封禁，无法选用'],
        ['code' => 400, 'errcode' => 200014, 'status' => '', 'reason' => '模板 tid 参数错误', 'explain' => '模板 tid 参数错误'],
        ['code' => 400, 'errcode' => 200016, 'status' => '', 'reason' => 'start 参数错误', 'explain' => 'start 参数错误'],
        ['code' => 400, 'errcode' => 200017, 'status' => 'Limit parameter error', 'reason' => 'limit 参数错误', 'explain' => 'limit 参数错误'],
        ['code' => 400, 'errcode' => 200018, 'status' => '', 'reason' => '类目 ids 缺失', 'explain' => '类目 ids 缺失'],
        ['code' => 400, 'errcode' => 200019, 'status' => '', 'reason' => '类目 ids 不合法', 'explain' => '类目 ids 不合法'],
        ['code' => 400, 'errcode' => 200020, 'status' => '', 'reason' => '关键词列表 kidlist 参数错误', 'explain' => '关键词列表 kidlist 参数错误'],
        ['code' => 400, 'errcode' => 200021, 'status' => '', 'reason' => '场景描述 scenedesc 参数错误', 'explain' => '场景描述 scenedesc 参数错误'],
        ['code' => 400, 'errcode' => 300001, 'status' => '', 'reason' => '禁止创建/更新商品', 'explain' => '禁止创建/更新商品（如商品创建功能被封禁） 或 禁止编辑&更新房间'],
        ['code' => 400, 'errcode' => 300002, 'status' => '', 'reason' => '名称长度不符合规则', 'explain' => '名称长度不符合规则'],
        ['code' => 400, 'errcode' => 300003, 'status' => '', 'reason' => '价格输入不合规', 'explain' => '价格输入不合规（如现价比原价大、传入价格非数字等）'],
        ['code' => 400, 'errcode' => 300004, 'status' => '', 'reason' => '商品名称存在违规违法内容', 'explain' => '商品名称存在违规违法内容'],
        ['code' => 400, 'errcode' => 300005, 'status' => '', 'reason' => '商品图片存在违规违法内容', 'explain' => '商品图片存在违规违法内容'],
        ['code' => 400, 'errcode' => 300006, 'status' => '', 'reason' => '图片上传失败', 'explain' => '图片上传失败（如 mediaid 过期）'],
        ['code' => 400, 'errcode' => 300007, 'status' => '', 'reason' => '线上小程序版本不存在该链接', 'explain' => '线上小程序版本不存在该链接'],
        ['code' => 400, 'errcode' => 300008, 'status' => '', 'reason' => '添加商品失败', 'explain' => '添加商品失败'],
        ['code' => 400, 'errcode' => 300009, 'status' => '', 'reason' => '商品审核撤回失败', 'explain' => '商品审核撤回失败'],
        ['code' => 400, 'errcode' => 300010, 'status' => '', 'reason' => '商品审核状态不对', 'explain' => '商品审核状态不对（如商品审核中）'],
        ['code' => 400, 'errcode' => 300011, 'status' => '', 'reason' => '操作非法', 'explain' => '操作非法（api不允许操作非 api 创建的商品）'],
        ['code' => 400, 'errcode' => 300012, 'status' => '', 'reason' => '没有提审额度', 'explain' => '没有提审额度（每天500次提审额度）'],
        ['code' => 400, 'errcode' => 300013, 'status' => '', 'reason' => '提审失败', 'explain' => '提审失败'],
        ['code' => 400, 'errcode' => 300014, 'status' => '', 'reason' => '审核中，无法删除', 'explain' => '审核中，无法删除（非零代表失败）'],
        ['code' => 400, 'errcode' => 300017, 'status' => '', 'reason' => '商品未提审', 'explain' => '商品未提审'],
        ['code' => 400, 'errcode' => 300021, 'status' => '', 'reason' => '商品添加成功，审核失败', 'explain' => '商品添加成功，审核失败'],
        ['code' => 400, 'errcode' => 300022, 'status' => '', 'reason' => '此房间号不存在', 'explain' => '此房间号不存在'],
        ['code' => 400, 'errcode' => 300023, 'status' => '', 'reason' => '房间状态已被拦截', 'explain' => '房间状态 拦截（当前房间状态不允许此操作）'],
        ['code' => 400, 'errcode' => 300024, 'status' => '', 'reason' => '商品不存在', 'explain' => '商品不存在'],
        ['code' => 400, 'errcode' => 300025, 'status' => '', 'reason' => '商品审核未通过', 'explain' => '商品审核未通过'],
        ['code' => 400, 'errcode' => 300026, 'status' => '', 'reason' => '房间商品数量已经满额', 'explain' => '房间商品数量已经满额'],
        ['code' => 400, 'errcode' => 300027, 'status' => '', 'reason' => '导入商品失败', 'explain' => '导入商品失败'],
        ['code' => 400, 'errcode' => 300028, 'status' => '', 'reason' => '房间名称违规', 'explain' => '房间名称违规'],
        ['code' => 400, 'errcode' => 300029, 'status' => '', 'reason' => '主播昵称违规', 'explain' => '主播昵称违规'],
        ['code' => 400, 'errcode' => 300030, 'status' => '', 'reason' => '主播微信号不合法', 'explain' => '主播微信号不合法'],
        ['code' => 400, 'errcode' => 300031, 'status' => '', 'reason' => '直播间封面图不合规', 'explain' => '直播间封面图不合规'],
        ['code' => 400, 'errcode' => 300032, 'status' => '', 'reason' => '直播间分享图违规', 'explain' => '直播间分享图违规'],
        ['code' => 400, 'errcode' => 300033, 'status' => '', 'reason' => '添加商品超过直播间上限', 'explain' => '添加商品超过直播间上限'],
        ['code' => 400, 'errcode' => 300034, 'status' => '', 'reason' => '主播微信昵称长度不符合要求', 'explain' => '主播微信昵称长度不符合要求'],
        ['code' => 400, 'errcode' => 300035, 'status' => '', 'reason' => '主播微信号不存在', 'explain' => '主播微信号不存在'],
        ['code' => 400, 'errcode' => 300036, 'status' => '', 'reason' => '主播微信号未实名认证', 'explain' => '主播微信号未实名认证'],
        ['code' => 412, 'errcode' => 600001, 'status' => 'invalid file name', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 600002, 'status' => 'no permission to upload file', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 600003, 'status' => 'invalid size of source', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 800059, 'status' => 'error: xxxx, file not found', 'reason' => '参数中的 tabbar 的pagepath中传入的路径，找不到该文件',
         'explain' => '参数中的 tabbar 的pagepath中传入的路径，找不到该文件'],
        ['code' => 400, 'errcode' => 928000, 'status' => '', 'reason' => '票据已存在', 'explain' => '票据已存在'],
        ['code' => 400, 'errcode' => 928001, 'status' => '', 'reason' => '票据不存在', 'explain' => '票据不存在'],
        ['code' => 400, 'errcode' => 930555, 'status' => 'sysem error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 930556, 'status' => 'delivery timeout', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 930557, 'status' => 'delivery system error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 930558, 'status' => 'delivery logic error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 930559, 'status' => 'invaild openid', 'reason' => '沙盒环境 openid 无效', 'explain' => '沙盒环境 openid 无效'],
        ['code' => 400, 'errcode' => 930560, 'status' => 'shopid need bind first', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 930561, 'status' => 'args error', 'reason' => '参数错误', 'explain' => '参数错误'],
        ['code' => 400, 'errcode' => 930562, 'status' => 'order already exists', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 930563, 'status' => 'order not exists', 'reason' => '订单不存在', 'explain' => '订单不存在'],
        ['code' => 400, 'errcode' => 930564, 'status' => 'quota run out, try next day', 'reason' => '沙盒环境调用无配额', 'explain' => '沙盒环境调用无配额'],
        ['code' => 400, 'errcode' => 930565, 'status' => 'order finished', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 930566, 'status' => 'not support, plz auth at mp.weixin.qq.com', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 930567, 'status' => 'shop arg error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 930568, 'status' => 'not personal account', 'reason' => '不支持个人类型小程序', 'explain' => '不支持个人类型小程序'],
        ['code' => 400, 'errcode' => 930569, 'status' => 'already open', 'reason' => '请勿重复开通', 'explain' => '已经开通不需要再开通'],
        ['code' => 412, 'errcode' => 930570, 'status' => 'cargo_first_class or cargo_second_class invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 930571, 'status' => '', 'reason' => '该商户没有内测权限', 'explain' => '该商户没有内测权限，请先申请权限: https://wj.qq.com/s2/7243532/fcfb/'],
        ['code' => 400, 'errcode' => 931010, 'status' => 'fee already set', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 6000100, 'status' => 'unbind download url', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 6000101, 'status' => 'no response data', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 6000102, 'status' => 'response data too big', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9001001, 'status' => 'Bad Post param', 'reason' => '非法的 POST 数据参数', 'explain' => 'post 数据参数不合法'],
        ['code' => 410, 'errcode' => 9001002, 'status' => 'Remote service unavailable', 'reason' => '远端服务不可用', 'explain' => ''],
        ['code' => 412, 'errcode' => 9001003, 'status' => 'Bad Ticket', 'reason' => '非法的Ticket', 'explain' => ''],
        ['code' => 412, 'errcode' => 9001004, 'status' => 'get nearby user info failure', 'reason' => '获取摇周边用户信息失败', 'explain' => ''],
        ['code' => 412, 'errcode' => 9001005, 'status' => 'Failed to obtain merchant info', 'reason' => '获取商户信息失败', 'explain' => ''],
        ['code' => 412, 'errcode' => 9001006, 'status' => 'Failed to get openid', 'reason' => '获取 OpenID 失败', 'explain' => ''],
        ['code' => 412, 'errcode' => 9001007, 'status' => 'Upload file missing', 'reason' => '上传文件缺失', 'explain' => ''],
        ['code' => 412, 'errcode' => 9001008, 'status' => '', 'reason' => '非法的上传素材的文件类型', 'explain' => '上传素材的文件类型不合法'],
        ['code' => 412, 'errcode' => 9001009, 'status' => '', 'reason' => '非法的上传素材的文件尺寸', 'explain' => '上传素材的文件尺寸不合法'],
        ['code' => 412, 'errcode' => 9001010, 'status' => 'Upload failed', 'reason' => '上传失败', 'explain' => ''],
        ['code' => 412, 'errcode' => 9001020, 'status' => 'Bad account', 'reason' => '非法的账号', 'explain' => '账号不合法'],
        ['code' => 412, 'errcode' => 9001021, 'status' => 'Low activation rate', 'reason' => '已有设备激活率低于 50% ，不能新增设备', 'explain' => '已有设备激活率低于 50% ，不能新增设备'],
        ['code' => 412, 'errcode' => 9001022, 'status' => 'Bad device apply quantity', 'reason' => '非法的设备申请数，必须为大于 0 的数字', 'explain' => '设备申请数不合法，必须为大于 0 的数字'],
        ['code' => 400, 'errcode' => 9001023, 'status' => '', 'reason' => '已存在审核中的设备 id 申请', 'explain' => '已存在审核中的设备 id 申请'],
        ['code' => 400, 'errcode' => 9001024, 'status' => '', 'reason' => '一次查询设备 id 数量不能超过 50', 'explain' => '一次查询设备 id 数量不能超过 50'],
        ['code' => 412, 'errcode' => 9001025, 'status' => 'Bad device ID', 'reason' => '非法的设备 ID', 'explain' => ''],
        ['code' => 412, 'errcode' => 9001026, 'status' => 'Bad page ID', 'reason' => '非法的页面 ID', 'explain' => '页面 id 不合法'],
        ['code' => 412, 'errcode' => 9001027, 'status' => 'Bad device param', 'reason' => '非法的页面参数', 'explain' => '页面参数不合法'],
        ['code' => 400, 'errcode' => 9001028, 'status' => '', 'reason' => '一次删除页面 id 数量不能超过 10', 'explain' => '一次删除页面 id 数量不能超过 10'],
        ['code' => 400, 'errcode' => 9001029, 'status' => '', 'reason' => '页面已应用在设备中', 'explain' => '页面已应用在设备中，请先解除应用关系再删除'],
        ['code' => 400, 'errcode' => 9001030, 'status' => '', 'reason' => '一次查询页面 id 数量不能超过 50', 'explain' => '一次查询页面 id 数量不能超过 50'],
        ['code' => 412, 'errcode' => 9001031, 'status' => 'Bad time interval', 'reason' => '非法的时间区间', 'explain' => '时间区间不合法'],
        ['code' => 412, 'errcode' => 9001032, 'status' => 'Device binding relationship param error', 'reason' => '保存设备与页面的绑定关系参数错误', 'explain' => ''],
        ['code' => 412, 'errcode' => 9001033, 'status' => 'Bad store ID', 'reason' => '非法的门店 ID', 'explain' => '门店 id 不合法'],
        ['code' => 412, 'errcode' => 9001034, 'status' => 'Device remark Too long', 'reason' => '设备备注信息过长', 'explain' => ''],
        ['code' => 412, 'errcode' => 9001035, 'status' => 'Bad device apply param', 'reason' => '非法的设备申请参数', 'explain' => '设备申请参数不合法'],
        ['code' => 412, 'errcode' => 9001036, 'status' => 'Bad query start value', 'reason' => '非法的查询起始值 begin', 'explain' => '查询起始值 begin 不合法'],
        ['code' => 412, 'errcode' => 9002008, 'status' => 'params invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9002009, 'status' => 'shop id not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9002010, 'status' => 'ssid or password should start with wx', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9002011, 'status' => 'ssid can not include chinese', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9002012, 'status' => 'passsword can not include chinese', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9002013, 'status' => 'password must be between 8 and 24 characters', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9002016, 'status' => 'device exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9002017, 'status' => 'device not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9002026, 'status' => 'the size of query list reach limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9002041, 'status' => 'not allowed to modify, ensure you are an certified or component account', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9002044, 'status' => 'invalid ssid, can not include none utf8 characters, and should be between 1 and 32 bytes', 'reason' => '',
         'explain' => ''],
        ['code' => 400, 'errcode' => 9002052, 'status' => 'shop id has not be audited, this bar type is not enable', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9007003, 'status' => 'protocol type is not same with the exist device', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9007004, 'status' => 'ssid not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9007005, 'status' => 'device count limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9008001, 'status' => 'card info not exist', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9008002, 'status' => 'card expiration time is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9008003, 'status' => 'url size invalid, keep less than 255', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9008004, 'status' => 'url can not include chinese', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200001, 'status' => 'order_id not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200002, 'status' => 'order of other biz', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200003, 'status' => 'blocked', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200211, 'status' => 'payment notice disabled', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200231, 'status' => 'payment notice not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200232, 'status' => 'payment notice paid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200233, 'status' => 'payment notice canceled', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200235, 'status' => 'payment notice expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200236, 'status' => 'bank not allow', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200295, 'status' => 'freq limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200297, 'status' => 'suspend payment at current time', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200298, 'status' => '3rd resp decrypt error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200299, 'status' => '3rd resp system error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9200300, 'status' => '3rd resp sign error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201000, 'status' => 'desc empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201001, 'status' => 'fee not equal items', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201002, 'status' => 'payment info incorrect', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201003, 'status' => 'fee is 0', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201004, 'status' => 'payment expire date format error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201005, 'status' => 'appid error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201006, 'status' => 'payment order id error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201007, 'status' => 'openid error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201008, 'status' => 'return_url error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201009, 'status' => 'ip error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201010, 'status' => 'order_id error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201011, 'status' => 'reason error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201012, 'status' => 'mch_id error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201013, 'status' => 'bill_date error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201014, 'status' => 'bill_type error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201015, 'status' => 'trade_type error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201016, 'status' => 'bank_id error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201017, 'status' => 'bank_account error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201018, 'status' => 'payment_notice_no error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201019, 'status' => 'department_code error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201020, 'status' => 'payment_notice_type error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201021, 'status' => 'region_code error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201022, 'status' => 'department_name error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201023, 'status' => 'fee not equal finances', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201024, 'status' => 'refund_out_id error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201026, 'status' => 'not combined order_id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201027, 'status' => 'partial sub order is test', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201029, 'status' => 'desc too long', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201031, 'status' => 'sub order list size error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201032, 'status' => 'sub order repeated', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201033, 'status' => 'auth_code empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201034, 'status' => 'bank_id empty but mch_id not empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9201035, 'status' => 'sum of other fees exceed total fee', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202000, 'status' => 'other user paying', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202001, 'status' => 'pay process not finish', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202002, 'status' => 'no refund permission', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202003, 'status' => 'ip limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202004, 'status' => 'freq limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202005, 'status' => 'user weixin account abnormal', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202006, 'status' => 'account balance not enough', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202010, 'status' => 'refund request repeated', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202011, 'status' => 'has refunded', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202012, 'status' => 'refund exceed total fee', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202013, 'status' => 'busi_id dup', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202016, 'status' => 'not check sign', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202017, 'status' => 'check sign failed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202018, 'status' => 'sub order error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202020, 'status' => 'order status error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9202021, 'status' => 'unified order repeatedly', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9203000, 'status' => 'request to notification url fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9203001, 'status' => 'http request fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9203002, 'status' => 'http response data error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9203003, 'status' => 'http response data rsa decrypt fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9203004, 'status' => 'http response data aes decrypt fail', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9203999, 'status' => 'system busy, please try again later', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9204000, 'status' => 'getrealname token error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9204001, 'status' => 'getrealname user or token error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9204002, 'status' => 'getrealname appid or token error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9205000, 'status' => 'finance conf not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9205001, 'status' => 'bank conf not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9205002, 'status' => 'wei ban ju conf not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9205010, 'status' => 'symmetric key conf not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9205101, 'status' => 'out order id not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9205201, 'status' => 'bill not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9206000, 'status' => '3rd resp pay_channel empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9206001, 'status' => '3rd resp order_id empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9206002, 'status' => '3rd resp bill_type_code empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9206003, 'status' => '3rd resp bill_no empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9206200, 'status' => '3rd resp empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9206201, 'status' => '3rd resp not json', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9206900, 'status' => 'connect 3rd error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9206901, 'status' => 'connect 3rd timeout', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9206910, 'status' => 'read 3rd resp error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9206911, 'status' => 'read 3rd resp timeout', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207000, 'status' => 'boss error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207001, 'status' => 'wechat pay error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207002, 'status' => 'boss param error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207003, 'status' => 'pay error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207004, 'status' => 'auth_code expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207005, 'status' => 'user balance not enough', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207006, 'status' => 'card not support', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207007, 'status' => 'order reversed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207008, 'status' => 'user paying, need input password', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207009, 'status' => 'auth_code error', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9207010, 'status' => 'auth_code invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207011, 'status' => 'not allow to reverse when user paying', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207012, 'status' => 'order paid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207013, 'status' => 'order closed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207028, 'status' => 'vehicle not exists', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207029, 'status' => 'vehicle request blocked', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207030, 'status' => 'vehicle auth error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207031, 'status' => 'contract over limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207032, 'status' => 'trade error', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9207033, 'status' => 'trade time invalid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9207034, 'status' => 'channel type invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9207050, 'status' => 'expire_time range error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9210000, 'status' => 'query finance error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9291000, 'status' => 'openid error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9291001, 'status' => 'openid appid not match', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9291002, 'status' => 'app_appid not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9291003, 'status' => 'app_appid not app', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9291004, 'status' => 'appid empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9291005, 'status' => 'appid not match access_token', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9291006, 'status' => 'invalid sign', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9299999, 'status' => 'backend logic error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300001, 'status' => 'begin_time can not before now', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300002, 'status' => 'end_time can not before now', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300003, 'status' => 'begin_time must less than end_time', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300004, 'status' => 'end_time - begin_time > 1year', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300005, 'status' => 'invalid max_partic_times', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300006, 'status' => 'invalid activity status', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300007, 'status' => 'gift_num must >0 and <=15', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300008, 'status' => 'invalid tiny appid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300009, 'status' => 'activity can not finish', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300010, 'status' => 'card_info_list must >= 2', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300011, 'status' => 'invalid card_id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300012, 'status' => 'card_id must belong this appid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300013, 'status' => 'card_id is not swipe_card or pay.cash', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300014, 'status' => 'some card_id is out of stock', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300015, 'status' => 'some card_id is invalid status', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300016, 'status' => 'membership or new/old tinyapp user only support one', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300017, 'status' => 'invalid logic for membership', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300018, 'status' => 'invalid logic for tinyapp new/old user', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300019, 'status' => 'invalid activity type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300020, 'status' => 'invalid activity_id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300021, 'status' => 'invalid help_max_times', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300022, 'status' => 'invalid cover_url', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300023, 'status' => 'invalid gen_limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300024, 'status' => 'cards end_time cannot early than acts end_time', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300501, 'status' => 'delivery side error', 'reason' => '快递侧逻辑错误', 'explain' => '快递侧逻辑错误，详细原因需要看 delivery result code, 请先确认一下编码方式。'],
        ['code' => 400, 'errcode' => 9300502, 'status' => 'delivery side sys error', 'reason' => '快递公司系统错误', 'explain' => '快递公司系统错误'],
        ['code' => 400, 'errcode' => 9300503, 'status' => 'specified delivery id is not registerred', 'reason' => 'delivery_id 不存在', 'explain' => 'delivery_id 不存在'],
        ['code' => 400, 'errcode' => 9300504, 'status' => 'specified delivery id has beed banned', 'reason' => 'service_type 不存在', 'explain' => 'service_type 不存在'],
        ['code' => 400, 'errcode' => 9300505, 'status' => 'shop banned', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300506, 'status' => 'order cant cancel', 'reason' => '运单 id 已经存在轨迹，不可取消', 'explain' => '运单 id 已经存在轨迹，不可取消'],
        ['code' => 412, 'errcode' => 9300507, 'status' => 'invalid token, cant decryption or decryption result is different from the plaintext',
         'reason' => 'token 不正确', 'explain' => 'token 不正确'],
        ['code' => 400, 'errcode' => 9300508, 'status' => 'order id has been used', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300509, 'status' => 'speed limit, retry too fast', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300510, 'status' => 'invalid service type', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300511, 'status' => 'invalid branch id', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300512, 'status' => 'invalid waybill template format', 'reason' => '模板格式错误，渲染失败', 'explain' => '模板格式错误，渲染失败'],
        ['code' => 400, 'errcode' => 9300513, 'status' => 'out of quota', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300514, 'status' => 'add net branch fail, try update branch api', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300515, 'status' => 'wxa appid not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300516, 'status' => 'wxa appid and current bizuin is not linked or not the same owner', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300517, 'status' => 'invalid update_type, please use [bind] or [unbind]', 'reason' => 'update_type 不正确',
         'explain' => 'update_type 不正确,请使用bind 或者unbind'],
        ['code' => 412, 'errcode' => 9300520, 'status' => 'invalid delivery id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300521, 'status' => 'the orderid is in our system, and waybill is generating', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300522, 'status' => 'this orderid is repeated', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300523, 'status' => 'quota is not enough; go to charge please', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300524, 'status' => 'order already canceled', 'reason' => '订单已取消', 'explain' => '订单已取消（一般为重复取消订单）'],
        ['code' => 400, 'errcode' => 9300525, 'status' => 'biz id not bind', 'reason' => 'bizid未绑定', 'explain' => 'bizid未绑定'],
        ['code' => 400, 'errcode' => 9300526, 'status' => 'arg size exceed limit', 'reason' => '参数字段长度不正确', 'explain' => '参数字段长度不正确'],
        ['code' => 400, 'errcode' => 9300527, 'status' => 'delivery does not support quota', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300528, 'status' => 'invalid waybill_id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300529, 'status' => 'biz_id already binded', 'reason' => '账号已绑定过', 'explain' => '账号已绑定过'],
        ['code' => 400, 'errcode' => 9300530, 'status' => 'biz_id is not exist', 'reason' => '解绑的biz_id不存在', 'explain' => '解绑的biz_id不存在'],
        ['code' => 412, 'errcode' => 9300531, 'status' => 'invalid biz_id or password', 'reason' => 'bizid无效 或者密码错误', 'explain' => 'bizid无效 或者密码错误'],
        ['code' => 400, 'errcode' => 9300532, 'status' => 'bind submit, and is checking', 'reason' => '绑定已提交，审核中', 'explain' => '绑定已提交，审核中'],
        ['code' => 412, 'errcode' => 9300533, 'status' => 'invalid tagid_list', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300534, 'status' => 'invalid appid, not same body', 'reason' => 'add_source=2时，wx_appid和当前小程序不同主体',
         'explain' => 'add_source=2时，wx_appid和当前小程序不同主体'],
        ['code' => 412, 'errcode' => 9300535, 'status' => 'invalid shop arg', 'reason' => '无效的商品参数', 'explain' => 'shop字段商品缩略图 url、商品名称为空或者非法，或者商品数量为0'],
        ['code' => 412, 'errcode' => 9300536, 'status' => 'invalid wxa_appid', 'reason' => 'add_source=2时，wx_appid无效', 'explain' => 'add_source=2时，wx_appid无效'],
        ['code' => 400, 'errcode' => 9300537, 'status' => 'freq limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300538, 'status' => 'input task empty', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300539, 'status' => 'too many task', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300540, 'status' => 'task not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300541, 'status' => 'delivery callback error', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300601, 'status' => 'id_card_no is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300602, 'status' => 'name is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300603, 'status' => 'plate_no is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300604, 'status' => 'auth_key decode error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300605, 'status' => 'auth_key is expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300606, 'status' => 'auth_key and appinfo not match', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300607, 'status' => 'user not confirm', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300608, 'status' => 'user confirm is expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300609, 'status' => 'api exceed limit', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300610, 'status' => 'car license info is invalid', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300611, 'status' => 'varification type not support', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300701, 'status' => 'input param error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300702, 'status' => 'this code has been used', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9300703, 'status' => 'invalid date', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300704, 'status' => 'not currently available', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300705, 'status' => 'code not exist or expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300706, 'status' => 'code not exist or expired', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300707, 'status' => 'wxpay error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300708, 'status' => 'wxpay overlimit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9300801, 'status' => '', 'reason' => '无效的微信号', 'explain' => '无效的微信号'],
        ['code' => 400, 'errcode' => 9300802, 'status' => '', 'reason' => '服务号未开通导购功能', 'explain' => '服务号未开通导购功能'],
        ['code' => 400, 'errcode' => 9300803, 'status' => '', 'reason' => '微信号已经绑定为导购', 'explain' => '微信号已经绑定为导购'],
        ['code' => 400, 'errcode' => 9300804, 'status' => '', 'reason' => '该微信号不是导购', 'explain' => '该微信号不是导购'],
        ['code' => 400, 'errcode' => 9300805, 'status' => '', 'reason' => '微信号已经被其他账号绑定为导购', 'explain' => '微信号已经被其他账号绑定为导购'],
        ['code' => 400, 'errcode' => 9300806, 'status' => '', 'reason' => '粉丝和导购不存在绑定关系', 'explain' => '粉丝和导购不存在绑定关系'],
        ['code' => 400, 'errcode' => 9300807, 'status' => '', 'reason' => '标签值无效，不是可选标签值', 'explain' => '标签值无效，不是可选标签值'],
        ['code' => 400, 'errcode' => 9300808, 'status' => '', 'reason' => '标签值不存在', 'explain' => '标签值不存在'],
        ['code' => 400, 'errcode' => 9300809, 'status' => '', 'reason' => '展示标签值不存在', 'explain' => '展示标签值不存在'],
        ['code' => 400, 'errcode' => 9300810, 'status' => '', 'reason' => '导购昵称太长', 'explain' => '导购昵称太长，最多16个字符'],
        ['code' => 400, 'errcode' => 9300811, 'status' => '', 'reason' => '只支持 mmbiz.qpic.cn 域名的图片', 'explain' => '只支持 mmbiz.qpic.cn 域名的图片'],
        ['code' => 400, 'errcode' => 9300812, 'status' => '', 'reason' => '达到导购绑定个数限制', 'explain' => '达到导购绑定个数限制'],
        ['code' => 400, 'errcode' => 9300813, 'status' => '', 'reason' => '达到导购粉丝绑定个数限制', 'explain' => '达到导购粉丝绑定个数限制'],
        ['code' => 400, 'errcode' => 9300814, 'status' => '', 'reason' => '敏感词个数超过上限', 'explain' => '敏感词个数超过上限'],
        ['code' => 400, 'errcode' => 9300815, 'status' => '', 'reason' => '快捷回复个数超过上限', 'explain' => '快捷回复个数超过上限'],
        ['code' => 400, 'errcode' => 9300816, 'status' => '', 'reason' => '文字素材个数超过上限', 'explain' => '文字素材个数超过上限'],
        ['code' => 400, 'errcode' => 9300817, 'status' => '', 'reason' => '小程序卡片素材个数超过上限', 'explain' => '小程序卡片素材个数超过上限'],
        ['code' => 400, 'errcode' => 9300818, 'status' => '', 'reason' => '图片素材个数超过上限', 'explain' => '图片素材个数超过上限'],
        ['code' => 400, 'errcode' => 9300819, 'status' => 'mediaid error', 'reason' => 'mediaid 有误', 'explain' => 'mediaid 有误'],
        ['code' => 400, 'errcode' => 9300820, 'status' => '', 'reason' => '可查询标签类别超过上限', 'explain' => '可查询标签类别超过上限'],
        ['code' => 400, 'errcode' => 9300821, 'status' => '', 'reason' => '小程序卡片内 appid 不符合要求', 'explain' => '小程序卡片内 appid 不符合要求'],
        ['code' => 400, 'errcode' => 9300822, 'status' => '', 'reason' => '标签类别的名字无效', 'explain' => '标签类别的名字无效'],
        ['code' => 400, 'errcode' => 9300823, 'status' => '', 'reason' => '查询聊天记录时间参数有误', 'explain' => '查询聊天记录时间参数有误'],
        ['code' => 400, 'errcode' => 9300824, 'status' => '', 'reason' => '自动回复字数太长', 'explain' => '自动回复字数太长'],
        ['code' => 400, 'errcode' => 9300825, 'status' => '', 'reason' => '导购群组 id 错误', 'explain' => '导购群组 id 错误'],
        ['code' => 400, 'errcode' => 9300826, 'status' => '', 'reason' => '维护中', 'explain' => '维护中'],
        ['code' => 412, 'errcode' => 9301001, 'status' => 'invalid parameter', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9301002, 'status' => 'call api service failed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9301003, 'status' => 'internal exception', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9301004, 'status' => 'save data error', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9301006, 'status' => 'invalid appid', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9301007, 'status' => 'invalid api config', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9301008, 'status' => 'invalid api info', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9301009, 'status' => 'add result check failed', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9301010, 'status' => 'consumption failure', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9301011, 'status' => 'frequency limit reached', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9301012, 'status' => 'service timeout', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9400001, 'status' => '', 'reason' => '该开发小程序已开通小程序直播权限', 'explain' => '该开发小程序已开通小程序直播权限，不支持发布版本。如需发版，请解绑开发小程序后再操作。'],
        ['code' => 400, 'errcode' => 9401001, 'status' => '', 'reason' => '商品已存在', 'explain' => '商品已存在'],
        ['code' => 400, 'errcode' => 9401002, 'status' => '', 'reason' => '商品不存在', 'explain' => '商品不存在'],
        ['code' => 400, 'errcode' => 9401003, 'status' => '', 'reason' => '类目已存在', 'explain' => '类目已存在'],
        ['code' => 400, 'errcode' => 9401004, 'status' => '', 'reason' => '类目不存在', 'explain' => '类目不存在'],
        ['code' => 400, 'errcode' => 9401005, 'status' => '', 'reason' => 'sku已存在', 'explain' => 'sku已存在'],
        ['code' => 400, 'errcode' => 9401006, 'status' => '', 'reason' => 'sku不存在', 'explain' => 'sku不存在'],
        ['code' => 400, 'errcode' => 9401007, 'status' => '', 'reason' => '属性已存在', 'explain' => '属性已存在'],
        ['code' => 400, 'errcode' => 9401008, 'status' => '', 'reason' => '属性不存在', 'explain' => '属性不存在'],
        ['code' => 400, 'errcode' => 9401020, 'status' => '', 'reason' => '非法参数', 'explain' => '非法参数'],
        ['code' => 400, 'errcode' => 9401021, 'status' => '', 'reason' => '没有商品权限', 'explain' => '没有商品权限'],
        ['code' => 400, 'errcode' => 9401022, 'status' => 'spu not allow', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9401023, 'status' => 'spu_not_allow_edit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9401024, 'status' => 'sku not allow', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9401025, 'status' => 'sku_not_allow_edit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402001, 'status' => 'limit too large', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402002, 'status' => 'single send been blocked', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402003, 'status' => 'all send been blocked', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9402004, 'status' => 'invalid msg id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402005, 'status' => 'send msg too quick', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402006, 'status' => 'send to single user too quick', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402007, 'status' => 'send to all user too quick', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402008, 'status' => 'send type error', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402009, 'status' => 'can not send this msg', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402010, 'status' => 'content too long or no content', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402011, 'status' => 'path not exist', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402012, 'status' => 'contain evil word', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402013, 'status' => 'path need html suffix', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402014, 'status' => 'not open to personal body type', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402015, 'status' => 'not open to violation body type', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402016, 'status' => 'not open to low quality provider', 'reason' => '', 'explain' => ''],
        ['code' => 412, 'errcode' => 9402101, 'status' => 'invalid product_id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402102, 'status' => 'device_id count more than limit', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9402202, 'status' => 'concurrent limit', 'reason' => '请勿频繁提交', 'explain' => '请勿频繁提交，待上一次操作完成后再提交'],
        ['code' => 400, 'errcode' => 9402301, 'status' => 'user not book this ad id', 'reason' => '', 'explain' => ''],
        ['code' => 400, 'errcode' => 9403000, 'status' => '', 'reason' => '消息类型错误', 'explain' => '消息类型错误!'],
        ['code' => 400, 'errcode' => 9403001, 'status' => '', 'reason' => '消息字段的内容过长', 'explain' => '消息字段的内容过长!'],
        ['code' => 400, 'errcode' => 9403002, 'status' => '', 'reason' => '消息字段的内容违规', 'explain' => '消息字段的内容违规!'],
        ['code' => 400, 'errcode' => 9403003, 'status' => '', 'reason' => '发送的微信号太多', 'explain' => '发送的微信号太多!'],
        ['code' => 400, 'errcode' => 9403004, 'status' => '', 'reason' => '存在错误的微信号', 'explain' => '存在错误的微信号!'],
        ['code' => 400, 'errcode' => 9410000, 'status' => 'live room not exsits', 'reason' => '直播间列表为空', 'explain' => '直播间列表为空'],
        ['code' => 400, 'errcode' => 9410001, 'status' => 'inner error: get room fail', 'reason' => '获取房间失败', 'explain' => '获取房间失败'],
        ['code' => 400, 'errcode' => 9410002, 'status' => 'inner error: get goods fail', 'reason' => '获取商品失败', 'explain' => '获取商品失败'],
        ['code' => 400, 'errcode' => 9410003, 'status' => 'inner error: get replay url fail', 'reason' => '获取回放失败', 'explain' => '获取回放失败']];

    /**
     * 响应代码说明
     *
     * @link https://developers.weixin.qq.com/doc/offiaccount/Getting_Started/Global_Return_Code.html
     * @link https://developers.weixin.qq.com/doc/oplatform/Return_codes/Return_code_descriptions_new.html
     *
     * @param int $code 响应代码
     *
     * @return array
     */
    public static function ResponseCode($code = 0): array {
        $column = array_column(self::$response_codes, 'errcode');
        $flip = array_flip($column);

        return self::$response_codes[$flip[$code] ?? ''] ?? [];
    }

    /**
     * 微信小程序_场景值列表
     *
     * @link https://developers.weixin.qq.com/miniprogram/dev/reference/scene-list.html
     * @var array
     */
    public static $scenes = [
        0 => '未知场景',
        1000 => '其他',
        1001 => '发现页小程序「最近使用」列表（基础库2.2.4-2.29.0版本包含「我的小程序」列表，2.29.1版本起仅为「最近使用」列表）',
        1005 => '微信首页顶部搜索框的搜索结果页',
        1006 => '发现栏小程序主入口搜索框的搜索结果页',
        1007 => '单人聊天会话中的小程序消息卡片',
        1008 => '群聊会话中的小程序消息卡片',
        1010 => '收藏夹',
        1011 => '扫描二维码',
        1012 => '长按图片识别二维码',
        1013 => '扫描手机相册中选取的二维码',
        1014 => '小程序订阅消息（与1107相同）',
        1017 => '前往小程序体验版的入口页',
        1019 => '微信钱包（微信客户端7.0.0版本改为支付入口）',
        1020 => '公众号 profile 页相关小程序列表（已废弃）',
        1022 => '聊天顶部置顶小程序入口（微信客户端6.6.1版本起废弃）',
        1023 => '安卓系统桌面图标',
        1024 => '小程序 profile 页',
        1025 => '扫描一维码',
        1026 => '发现栏小程序主入口，「附近的小程序」列表',
        1027 => '微信首页顶部搜索框搜索结果页「使用过的小程序」列表',
        1028 => '我的卡包',
        1029 => '小程序中的卡券详情页',
        1030 => '自动化测试下打开小程序',
        1031 => '长按图片识别一维码',
        1032 => '扫描手机相册中选取的一维码',
        1034 => '微信支付完成页',
        1035 => '公众号自定义菜单',
        1036 => 'App 分享消息卡片',
        1037 => '小程序打开小程序',
        1038 => '从另一个小程序返回',
        1039 => '摇电视',
        1042 => '添加好友搜索框的搜索结果页',
        1043 => '公众号模板消息',
        1044 => '带 shareTicket 的小程序消息卡片',
        1045 => '朋友圈广告',
        1046 => '朋友圈广告详情页',
        1047 => '扫描小程序码',
        1048 => '长按图片识别小程序码',
        1049 => '扫描手机相册中选取的小程序码',
        1052 => '卡券的适用门店列表',
        1053 => '搜一搜的结果页',
        1054 => '顶部搜索框小程序快捷入口（微信客户端版本6.7.4起废弃）',
        1056 => '聊天顶部音乐播放器右上角菜单',
        1057 => '钱包中的银行卡详情页',
        1058 => '公众号文章',
        1059 => '体验版小程序绑定邀请页',
        1060 => '微信支付完成页（与1034相同）',
        1064 => '微信首页连Wi-Fi状态栏',
        1065 => 'URL scheme',
        1067 => '公众号文章广告',
        1068 => '附近小程序列表广告（已废弃）',
        1069 => '移动应用通过openSDK进入微信，打开小程序',
        1071 => '钱包中的银行卡列表页',
        1072 => '二维码收款页面',
        1073 => '客服消息列表下发的小程序消息卡片',
        1074 => '公众号会话下发的小程序消息卡片',
        1077 => '摇周边',
        1078 => '微信连Wi-Fi成功提示页',
        1079 => '微信游戏中心',
        1081 => '客服消息下发的文字链',
        1082 => '公众号会话下发的文字链',
        1084 => '朋友圈广告原生页',
        1088 => '会话中查看系统消息，打开小程序',
        1089 => '微信聊天主界面下拉，「最近使用」栏（基础库2.2.4-2.29.0版本包含「我的小程序」栏，2.29.1版本起仅为「最近使用」栏',
        1090 => '长按小程序右上角菜单唤出最近使用历史',
        1091 => '公众号文章商品卡片',
        1092 => '城市服务入口',
        1095 => '小程序广告组件',
        1096 => '聊天记录，打开小程序',
        1097 => '微信支付签约原生页，打开小程序',
        1099 => '页面内嵌插件',
        1100 => '红包封面详情页打开小程序',
        1101 => '远程调试热更新（开发者工具中，预览 -> 自动预览 -> 编译并预览）',
        1102 => '公众号 profile 页服务预览',
        1103 => '发现页小程序「我的小程序」列表（基础库2.2.4-2.29.0版本废弃，2.29.1版本起生效）',
        1104 => '微信聊天主界面下拉，「我的小程序」栏（基础库2.2.4-2.29.0版本废弃，2.29.1版本起生效）',
        1106 => '聊天主界面下拉，从顶部搜索结果页，打开小程序',
        1107 => '订阅消息，打开小程序',
        1113 => '安卓手机负一屏，打开小程序（三星）',
        1114 => '安卓手机侧边栏，打开小程序（三星）',
        1119 => '【企业微信】工作台内打开小程序',
        1120 => '【企业微信】个人资料页内打开小程序',
        1121 => '【企业微信】聊天加号附件框内打开小程序',
        1124 => '扫“一物一码”打开小程序',
        1125 => '长按图片识别“一物一码”',
        1126 => '扫描手机相册中选取的“一物一码”',
        1129 => '微信爬虫访问',
        1131 => '浮窗（8.0版本起仅包含被动浮窗）',
        1133 => '硬件设备打开小程序',
        1135 => '小程序profile页相关小程序列表，打开小程序',
        1144 => '公众号文章 - 视频贴片',
        1145 => '发现栏 - 发现小程序',
        1146 => '地理位置信息打开出行类小程序',
        1148 => '卡包-交通卡，打开小程序',
        1150 => '扫一扫商品条码结果页打开小程序',
        1151 => '发现栏 - 我的订单',
        1152 => '订阅号视频打开小程序',
        1153 => '“识物”结果页打开小程序',
        1154 => '朋友圈内打开“单页模式”',
        1155 => '“单页模式”打开小程序',
        1157 => '服务号会话页打开小程序',
        1158 => '群工具打开小程序',
        1160 => '群待办',
        1167 => 'H5 通过开放标签打开小程序',
        1168 => '移动/网站应用直接运行小程序',
        1169 => '发现栏小程序主入口，各个生活服务入口（例如快递服务、出行服务等）',
        1171 => '微信运动记录（仅安卓）',
        1173 => '聊天素材用小程序打开',
        1175 => '视频号主页商店入口',
        1176 => '视频号直播间主播打开小程序',
        1177 => '视频号直播商品',
        1178 => '在电脑打开手机上打开的小程序',
        1179 => '#话题页打开小程序',
        1181 => '网站应用打开PC小程序',
        1183 => 'PC微信 - 小程序面板 - 发现小程序 - 搜索',
        1184 => '视频号链接打开小程序',
        1185 => '群公告',
        1186 => '收藏 - 笔记',
        1187 => '浮窗（8.0版本起）',
        1189 => '表情雨广告',
        1191 => '视频号活动',
        1192 => '企业微信联系人profile页',
        1193 => '视频号主页服务菜单打开小程序',
        1194 => 'URL Link',
        1195 => '视频号主页商品tab',
        1196 => '个人状态打开小程序',
        1197 => '视频号主播从直播间返回小游戏',
        1198 => '视频号开播界面打开小游戏',
        1200 => '视频号广告打开小程序',
        1201 => '视频号广告详情页打开小程序',
        1202 => '企微客服号会话打开小程序卡片',
        1203 => '微信小程序压测工具的请求',
        1206 => '视频号小游戏直播间打开小游戏',
        1207 => '企微客服号会话打开小程序文字链',
        1208 => '聊天打开商品卡片',
        1212 => '青少年模式申请页打开小程序',
        1215 => '广告预约打开小程序',
        1216 => '视频号订单中心打开小程序',
        1223 => '安卓桌面Widget打开小程序',
        1228 => '视频号原生广告组件打开小程序',
        1230 => '订阅号H5广告进入小程序',
        1231 => '动态消息提醒入口打开小程序',
        1242 => '小程序发现页门店快送模块频道页进入小程序',
        1245 => '小程序发现页门店快送搜索结果页进入小程序',
        1252 => '搜一搜小程序搜索页「小功能」模块进入小程序'];

    /**
     * 获取微信场景信息
     *
     * @param int $scene 场景值
     *
     * @return array
     */
    public static function getScene($scene = 0): array {
        if (array_key_exists($scene, self::$scenes)) {
            return self::$scenes[$scene] ?? array();
        }

        return self::$scenes[array_key_exists($scene, self::$scenes) ? $scene : 0];
    }
}