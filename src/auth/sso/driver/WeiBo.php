<?php
declare (strict_types=1);

namespace mark\auth\sso\driver;

use mark\auth\Authorize;
use mark\auth\sso\Sso;
use mark\http\Client\Client;
use mark\response\Response;
use mark\system\Os;
use Psr\SimpleCache\InvalidArgumentException as CacheInvalidArgumentException;

/**
 * Class WeiBo
 *
 * @description 微博登录包括身份认证、用户关系以及内容传播。允许用户使用微博账号登录访问第三方网站，分享内容，同步信息
 * @author      Mark<mark@tianfuunion.cn>
 * @package     mark\auth\sso\driver
 */
class WeiBo extends Sso {

    public const GET_AUTH_CODE_URL = 'https://api.weibo.com/oauth2/authorize';
    public const GET_ACCESS_TOKEN_URL = 'https://api.weibo.com/oauth2/access_token';
    public const GET_REFRESH_TOKEN_URL = '';
    public const GET_OPENID_URL = '';
    public const GET_USER_INFO_URL = 'https://api.weibo.com/oauth2/get_token_info';
    public const GET_VERIFY_TOKEN_URL = '';

    private static $instance;

    /**
     * 创建Facade实例。
     *
     * @param bool $origin
     *
     * @return static
     */
    public static function getInstance(bool $origin = false): self {
        if (!(self::$instance instanceof self) || $origin) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * 第一步：用户同意授权，获取code
     *
     * @param string $redirect_uri 授权回调地址，站外应用需与设置的回调地址一致，站内应用需填写canvas page的地址。
     * @param string $scope        申请scope权限所需参数，可一次申请多个scope权限，用逗号分隔。使用文档
     * @param string $state        用于保持请求和回调的状态，在回调时，会在Query Parameter中回传该参数。开发者可以用这个参数验证请求有效性，也可以记录用户请求授权页前的位置。这个参数可用于防止跨站请求伪造（CSRF）攻击
     *                             display    false    string    授权页面的终端类型，取值见下面的说明。
     *                             forcelogin    false    boolean    是否强制用户重新登录，true：是，false：否。默认false。
     *                             language    false    string    授权页语言，缺省为中文简体版，en为英文版。英文版测试中，开发者任何意见可反馈至 @微博API
     *
     * @return \mark\response\Response
     */
    public function getCode(string $redirect_uri, $scope = '', $state = ''): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($redirect_uri)) {
            return Response::create('', 412, '', '缺少redirect_uri');
        }
        if (empty($scope)) {
            $scope = 'follow_app_official_microblog';
        }

        if (empty($state)) {
            $state = $this->generateState('md5', true);
        }

        $url = self::GET_AUTH_CODE_URL
            . '?response_type=code'
            . '&client_id=' . $this->appid
            . '&redirect_uri=' . rawurlencode($redirect_uri)
            . '&scope=' . $scope
            . '&state=' . $state;

        if (Os::isMobile()) {
            $url .= '&display=mobile';
        }

        // header('HTTP/1.1 301 Moved Permanently');
        // header('Location: ' . $url);
        return Response::create($url, 302);
    }

    /**
     * 第二步：通过code换取网页授权access_token
     *
     * @param string $code
     * @param string $state
     *
     * @return \mark\response\Response
     */
    public function getAccessToken(string $code = '', string $state = ''): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        $cacheKey = 'auth:sso:weibo:access_token:appid:' . $this->appid . ':agent_md5:' . md5(Os::getAgent() . Os::getIpvs());

        if (empty($code)) {
            // throw new InvalidArgumentException('缺少Code参数');
        }

        if (!$this->checkState($state) && !empty($state)) {
            Authorize::getInstance()->getLogcat()->warning('OAuth.WeiBo::getAccessToken(checkState)' . $state . '=>' . $this->getState());
            if (Authorize::getInstance()->isStrict()) {
                return Response::create('', 425, 'The state does not match. You may be a victim of CSRF');
            }
        }

        try {
            $redirect_uri = Authorize::getInstance()->getCacheHandle()->get($state);
            if (empty($redirect_uri)) {
                return Response::create('', 412, '', '无效的redirect_uri参数');
            }
        } catch (CacheInvalidArgumentException $e) {
            Authorize::getInstance()->getLogcat()->warning('OAuth::WeiBo::getAccessToken::CacheInvalidArgumentException.get(' . $e->getCode() . ')' . $e->getMessage());
            return Response::create('', $e->getCode() ?: 412, $e->getMessage(), '无效的redirect_uri参数');
        }

        if (empty($this->secret)) {
            return Response::create('', 412, '', '缺少AppSecret参数');
        }

        $token_url = self::GET_ACCESS_TOKEN_URL
            . '?grant_type=' . 'authorization_code'
            . '&client_id=' . $this->appid
            . '&client_secret=' . $this->secret
            . '&code=' . $code
            . '&redirect_uri=' . rawurlencode($redirect_uri);

        return Client::getInstance()
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setExpire(7000)
                     ->setCache(empty($code))
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['error_code']) && $data['error_code'] != 0) {
                             Authorize::getInstance()->getLogcat()->warning('OAuth.WeiBo::getAccessToken()' . json_encode($data));
                             $response->setResponseCode($data['error_code'] ?: 404, false);
                             $response->setResponseStatus($data['error_description'] ?: '');
                             $response->setResponseReason($data['error_description'] ?: '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->post($token_url, 'json');
    }

    /**
     * 第三步：刷新access_token（如果需要）
     *
     * @param string $refresh_token
     *
     * @return \mark\response\Response
     */
    public function refreshToken(string $refresh_token): Response {
        return Response::create('', 404);
    }

    /**
     * 第四步：通过code换取网页授权openid
     *
     * @param string $code
     *
     * @return \mark\response\Response
     */
    public function getOpenID(string $code): Response {
        return Response::create('', 404);
    }

    /**
     * 第四步：拉取用户信息(需scope为 snsapi_userinfo)
     *
     * @param string $access_token
     * @param string $openid
     * @param string $lang
     *
     * @return \mark\response\Response
     */
    public function getUserInfo(string $access_token, string $openid, string $lang = 'zh-cn'): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少AccessToken参数');
        }
        $graph_url = self::GET_USER_INFO_URL . '?access_token=' . $access_token;
        return Client::getInstance()
                     ->setCallback(function (Response $response, Client $client) {
                         $data = $response->toArray();
                         if (isset($data['error_code']) && $data['error_code'] != 0) {
                             Authorize::getInstance()->getLogcat()->warning('OAuth.WeiBo::getUserInfo()' . json_encode($data));
                             $response->setResponseCode($data['error_code'] ?: 404, false);
                             $response->setResponseStatus($data['error_description'] ?: '');
                             $response->setResponseReason($data['error_description'] ?: '');
                             $client->setCache(false);
                             $client->clearCache();
                             // throw new OAuthException($data['errmsg'] ?: '', $data['errcode'] ?: 404);
                         }

                         return $response;
                     })
                     ->post($graph_url, 'json');
    }

    /**
     * 附：检验授权凭证（access_token）是否有效
     *
     * @param string $access_token
     * @param string $openid
     *
     * @return \mark\response\Response
     */
    public function verifyToken(string $access_token, string $openid): Response {
        return Response::create('', 404);
    }
}