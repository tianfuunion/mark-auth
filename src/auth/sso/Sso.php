<?php
declare (strict_types=1);

namespace mark\auth\sso;

use Exception;
use mark\auth\Authorize;
use mark\system\Os;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Class Sso
 *
 * @package mark\auth\sso
 */
abstract class Sso implements OAuthInterface {

    /**
     * @var string consumer_key app_id client_id
     */
    protected $appid;

    /**
     * @var string consumer_secret app_secret client_secret
     */
    protected $secret;

    /**
     * Sso constructor.
     */
    protected function __construct() { }

    /**
     * 创建Facade实例。
     *
     * @param bool $origin
     *
     * @return static
     */
    abstract public static function getInstance(bool $origin = false);

    /**
     * @param $appid
     *
     * @return $this
     */
    public function setAppID($appid): self {
        if (!empty($appid)) {
            $this->appid = $appid;
        }

        return $this;
    }

    /**
     * @param string $secret
     *
     * @return $this
     */
    public function setSecret(string $secret): self {
        if (!empty($secret)) {
            $this->secret = $secret;
        }

        return $this;
    }

    /**
     * 生成唯一随机串防CSRF攻击
     *
     * @param string $algo
     * @param bool   $origin
     *
     * @return string
     */
    public function generateState(string $algo = 'md5', bool $origin = false): string {
        $cacheKey = 'auth:sso:state:appid:' . $this->appid;

        $unique = Os::getAgent() . '_' . Os::getIpvs();
        switch ($algo ?: '') {
            case 'sha256':
                $cacheKey .= ':agent_sha256:' . hash('sha256', $unique);
                break;
            case 'md5':
            default:
                $cacheKey .= ':agent_md5:' . md5($unique);
                break;
        }

        try {
            $state = Authorize::getInstance()->getCacheHandle()->get($cacheKey);
            if (!empty($state) && !is_true($origin)) {
                // return $state ?: '';
            }
        } catch (InvalidArgumentException $e) {
            Authorize::getInstance()->getLogcat()->error('OAuth::Sso::generateState::InvalidArgumentException.get(' . $e->getCode() . ')' . $e->getMessage());
        }

        try {
            $uniqid = uniqid((string)(Os::getAgent() . '_' . Os::getIpvs() . '_' . time() . '_' . random_int(100000, 999999)), true);
        } catch (Exception $e) {
            Authorize::getInstance()->getLogcat()->error('OAuth::Sso::generateState::Exception.set(' . $e->getCode() . ')' . $e->getMessage());
            $uniqid = uniqid((string)(Os::getAgent() . '_' . Os::getIpvs() . '_' . time() . '_' . rand(100000, 999999)), true);
        }

        switch ($algo ?: '') {
            case 'sha256':
                $state = hash('sha256', $uniqid);
                break;
            case 'md5':
            default:
                $state = md5($uniqid);
                break;
        }

        try {
            Authorize::getInstance()->getCacheHandle()->set($cacheKey, $state, 600);
        } catch (InvalidArgumentException $e) {
            Authorize::getInstance()->getLogcat()->error('OAuth::Sso::generateState::InvalidArgumentException.set(' . $e->getCode() . ')' . $e->getMessage());
        }

        return $state ?: '';
    }

    /**
     * 获取缓存的唯一随机串
     *
     * @param string $algo
     *
     * @return string
     */
    public function getState(string $algo = 'md5'): string {
        try {
            $cacheKey = 'auth:sso:state:appid:' . $this->appid;
            $unique = Os::getAgent() . '_' . Os::getIpvs();
            switch ($algo ?: '') {
                case 'sha256':
                    $cacheKey .= ':agent_sha256:' . hash('sha256', $unique);
                    break;
                case 'md5':
                default:
                    $cacheKey .= ':agent_md5:' . md5($unique);
                    break;
            }

            if (!Authorize::getInstance()->getCacheHandle()->has($cacheKey)) {
                Authorize::getInstance()->getLogcat()->warning('sso:getState.has(unique) =>' . $unique);
                return '';
            }

            $origin = Authorize::getInstance()->getCacheHandle()->get($cacheKey);
            if (empty($origin)) {
                Authorize::getInstance()->getLogcat()->warning('sso:getState.get(origin)=>' . $origin);
                return '';
            }

            return $origin;
        } catch (InvalidArgumentException $e) {
            Authorize::getInstance()->getLogcat()->error('sso:getState.get(InvalidArgumentException)' . $e->getMessage());
        }

        return '';
    }

    /**
     * 校验唯一随机串防CSRF攻击
     *
     * @param string $state
     * @param string $algo
     * @param bool   $strict
     *
     * @return bool
     */
    public function checkState(string $state, string $algo = 'md5', bool $strict = false): bool {
        if (empty($state)) {
            return false;
        }

        $origin = $this->getState($algo);
        if (empty($origin) || $origin == false) {
            Authorize::getInstance()->getLogcat()->warning('sso:checkState.get(state vs origin)state=>' . $state . ',origin=>' . $origin);
            return false;
        }

        if ($strict) {
            return strcmp($origin, $state) == 0;
        }

        return strcasecmp($origin, $state) == 0;
    }

}