<?php
declare (strict_types=1);

namespace mark\auth\sso;

use mark\auth\Authorize;
use mark\auth\entity\AuthInfo;
use mark\response\Response;
use mark\system\Os;

/**
 * Class Client
 *
 * @author      Mark<mark@tianfuunion.cn>
 * @description 如果用户在微信客户端中访问第三方网页，公众号可以通过微信网页授权机制，来获取用户基本信息，进而实现业务逻辑。
 * @package     mark\auth\sso
 */
class Client extends Sso {
    public const GET_AUTH_CODE_URL = 'https://auth.tianfu.ink/auth.php/oauth2/authorize';
    public const GET_ACCESS_TOKEN_URL = 'https://auth.tianfu.ink/auth.php/oauth2/access_token';
    public const GET_REFRESH_TOKEN_URL = 'https://auth.tianfu.ink/auth.php/oauth2/refresh_token';
    public const GET_VERIFY_TOKEN_URL = 'https://auth.tianfu.ink/auth.php/oauth2/verify_token';
    public const GET_OPENID_URL = 'https://auth.tianfu.ink/auth.php/oauth2/user';
    public const GET_USER_INFO_URL = 'https://auth.tianfu.ink/auth.php/oauth2/userinfo';

    private $corpid;
    private $mchid;
    private $projectid;

    private static $instance;

    /**
     * 创建Facade实例。
     *
     * @param bool $origin
     *
     * @return static
     */
    public static function getInstance(bool $origin = false): self {
        if (!(self::$instance instanceof self) || $origin) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param $corpid
     *
     * @return $this
     */
    public function setCorpid($corpid): self {
        if (!empty($corpid)) {
            $this->corpid = $corpid;
        }

        return $this;
    }

    /**
     * @param $mchid
     *
     * @return $this
     */
    public function setMchid($mchid): self {
        if (!empty($mchid)) {
            $this->mchid = $mchid;
        }

        return $this;
    }

    /**
     * @param string $projectid
     *
     * @return $this
     */
    public function setProjectid($projectid = ''): self {
        if (!empty($projectid)) {
            $this->projectid = $projectid;
        }

        return $this;
    }

    /**
     * 获取用户授权信息
     * 当前节点为主要节点，并且未跨域,直接跳转到登录界面，
     * Url应该统一为一个地址，具体显示应该由登录控制器根据参数输出
     *
     * @param string $redirect_uri
     * @param string $code
     * @param string $state
     * @param string $scope
     * @param string $lang
     *
     * @return \mark\response\Response
     */
    public function authorize(string $redirect_uri, $code = '', $state = '', $scope = 'auth_base', $lang = 'zh-cn'): Response {
        // 1、第一步：用户同意授权，获取code
        if (empty($code)) {
            return $this->getCode($redirect_uri, $scope ?: AuthInfo::$auth_base, $state);
        }

        //2、第二步：通过code换取网页授权access_token
        $token = $this->getAccessToken($code, $state)->toArray();
        if ($scope === AuthInfo::$auth_base) {
            // return $token ?: array();
        }

        //4、第四步：拉取用户信息(需scope为 auth_userinfo)
        return $this->getUserInfo($token['access_token'] ?? '', $token['openid'] ?? '', $lang);
    }

    /**
     * 第一步：用户同意授权，获取code
     * Authorize constructor.
     *
     * @explain      GET：{'code':'081PxY4C06i5ki2MNv5C0LCC4C0PxY4A','state':'5ae74940188ff277cb3ea5021d543ea0'}
     *
     * @param string $redirect_uri  授权后重定向的回调链接地址， 请使用 urlEncode 对链接进行处理
     * @param string $scope         应用授权作用域，
     *                              auth_base （不弹出授权页面，直接跳转，只能获取用户openid），
     *                              auth_userinfo （弹出授权页面，可通过openid拿到昵称、性别、所在地。
     *                              auth_union (弹出授权页面，可通过OpenID获取到用户包括角色，群组在内的信息)
     *                              并且， 即使在未关注的情况下，只要用户授权，也能获取其信息 ）
     * @param string $state         重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
     *
     * @return \mark\response\Response
     */
    public function getCode(string $redirect_uri, $scope = 'auth_base', $state = ''): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($redirect_uri)) {
            return Response::create('', 412, '', '缺少redirect_uri参数');
        }

        if (empty($scope)) {
            $scope = AuthInfo::$auth_base;
        }

        if (empty($state)) {
            $state = $this->generateState('md5', true);
        }

        // $auth_url = self::GET_AUTH_CODE_URL . '?response_type=code&appid=' . $this->appid;
        $auth_url = Authorize::getInstance()->getHost() . '/auth.php/oauth2/authorize?response_type=code&appid=' . $this->appid;

        if (!empty($this->corpid)) {
            $auth_url .= '&corpid=' . $this->corpid;
        }
        if (!empty($this->mchid)) {
            $auth_url .= '&mchid=' . $this->mchid;
        }
        if (!empty($this->projectid)) {
            $auth_url .= '&projectid=' . $this->projectid;
        }

        $auth_url .= '&pattern=' . Authorize::getInstance()->getPattern();
        $auth_url .= '&scope=' . $scope . '&state=' . $state;
        $auth_url .= '&redirect_uri=' . rawurlencode($redirect_uri);
        $auth_url .= '#auth_redirect';

        // header('HTTP/1.1 301 Moved Permanently');
        // header('Location: ' . $auth_url);

        return Response::create($auth_url, 302);
    }

    /**
     * 第二步：通过code换取网页授权access_token
     *
     * @explain JSON：{
     * 'access_token':'ACCESS_TOKEN',
     * 'expires_in':7200,
     * 'refresh_token':'REFRESH_TOKEN',
     * 'openid':'OPENID',
     * 'scope':'SCOPE'}
     *
     * @param string $code 用于换取access_token的code
     * @param string $state
     *
     * @return \mark\response\Response
     */
    public function getAccessToken(string $code = '', string $state = ''): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }

        if (empty($code)) {
            return Response::create('', 412, '', '缺少Code参数');
        }

        if (!$this->checkState($state) && !empty($state)) {
            Authorize::getInstance()->getLogcat()->warning('OAuth.Client::getAccessToken(checkState)' . $state . '=>' . $this->getState());
            if (Authorize::getInstance()->isStrict()) {
                return Response::create('', 425, 'The state does not match. You may be a victim of CSRF');
            }
        }

        if (empty($this->secret)) {
            return Response::create('', 412, '', '缺少AppSecret参数');
        }

        $cacheKey = 'auth:sso:client:access_token:appid:' . $this->appid . ':agent_md5:' . md5(Os::getAgent() . Os::getIpvs());
        $token_url = self::GET_ACCESS_TOKEN_URL . '?appid=' . $this->appid . '&secret=' . $this->secret . '&code=' . $code . '&grant_type=authorization_code';
        $token_url = Authorize::getInstance()->getHost() . '/auth.php/oauth2/access_token?appid=' . $this->appid . '&secret=' . $this->secret . '&code=' . $code .
            '&grant_type=authorization_code';

        $client = \mark\http\Client\Client::getInstance()
                                          ->addHeader('pattern', Authorize::getInstance()->getPattern())
                                          ->addHeader('auth-type', Authorize::$_type)
                                          ->addHeader('auth-version', Authorize::$_version)
            // ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
            // ->setCacheKey($cacheKey)
            // ->setExpire(7000)
                                          ->setCache(false)
                                          ->get($token_url, 'json')
                                          ->api_decode();

        if ($client->getResponseCode() != 200 || $client->isEmpty()) {
            // throw new OAuthException($client->getResponseReason(), $client->getResponseCode() ?: 400);
        }

        return $client;
    }

    /**
     * 3、第三步：刷新access_token（如果需要）
     *
     * @param string $refresh_token 填写通过access_token获取到的refresh_token参数
     *
     * @return \mark\response\Response
     */
    public function refreshToken(string $refresh_token): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($refresh_token)) {
            return Response::create('', 412, '', '缺少RefreshToken参数');
        }

        $token_url = self::GET_REFRESH_TOKEN_URL . '?appid=' . $this->appid . '&grant_type=refresh_token&refresh_token=' . $refresh_token;
        $token_url = Authorize::getInstance()->getHost() . '?appid=' . $this->appid . '&grant_type=refresh_token&refresh_token=' . $refresh_token;

        $client = \mark\http\Client\Client::getInstance()
                                          ->addHeader('pattern', Authorize::getInstance()->getPattern())
                                          ->addHeader('auth-type', Authorize::$_type)
                                          ->addHeader('auth-version', Authorize::$_version)
                                          ->setCache(false)
                                          ->get($token_url, 'json')
                                          ->api_decode();

        if ($client->getResponseCode() != 200 || $client->isEmpty()) {
            // throw new OAuthException($client->getResponseReason(), $client->getResponseCode() ?: 404);
        }

        return $client;
    }

    /**
     * @param string $code
     *
     * @return \mark\response\Response
     */
    public function getOpenID(string $code): Response {
        return self::getAccessToken();
    }

    /**
     * 4、第四步：拉取用户信息(需scope为 auth_userinfo)
     *
     * @param string $access_token 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
     * @param string $openid       用户的唯一标识
     * @param string $lang         返回国家地区语言版本，zh-cn 简体，zh-TW 繁体，en 英语
     *
     * @return \mark\response\Response
     */
    public function getUserInfo(string $access_token, string $openid, string $lang = 'zh-cn'): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少Access_Token参数');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '缺少OpenID参数');
        }
        if (empty($lang)) {
            $lang = 'zh-cn';
        }

        $cacheKey = 'auth:sso:client:userinfo:appid:' . $this->appid . ':openid:' . $openid . ':lang:' . $lang;
        $userinfo_url = self::GET_USER_INFO_URL . '?access_token=' . $access_token . '&openid=' . $openid . '&lang=' . $lang;
        $userinfo_url = Authorize::getInstance()->getHost() . '/auth.php/oauth2/userinfo?access_token=' . $access_token . '&openid=' . $openid . '&lang=' . $lang;

        $client = \mark\http\Client\Client::getInstance()
                                          ->addHeader('pattern', Authorize::getInstance()->getPattern())
                                          ->addHeader('auth-type', Authorize::$_type)
                                          ->addHeader('auth-version', Authorize::$_version)
            // ->setExpire(Authorize::getInstance()->getExpire())
            // ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
            // ->setCacheKey($cacheKey)
                                          ->setCache(false)
                                          ->get($userinfo_url, 'json')
                                          ->api_decode();

        if ($client->getResponseCode() != 200 || $client->isEmpty()) {
            // throw new OAuthException($client->getResponseReason(), $client->getResponseCode() ?: 404);
        }

        return $client;
    }

    /**
     * 5、附：检验授权凭证（access_token）是否有效 { 'code':0,'msg':'ok'}
     *
     * @param string $access_token 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
     * @param string $openid       用户的唯一标识
     *
     * @return \mark\response\Response
     */
    public function verifyToken(string $access_token, string $openid): Response {
        if (empty($access_token)) {
            return Response::create('', 412, '', '缺少Access_Token参数');
        }
        if (empty($openid)) {
            return Response::create('', 412, '', '缺少OpenID参数');
        }

        $token_url = self::GET_VERIFY_TOKEN_URL . '?access_token=' . $access_token . '&openid=' . $openid;
        $token_url = Authorize::getInstance()->getHost() . '/auth.php/oauth2/verify_token?access_token=' . $access_token . '&openid=' . $openid;

        $client = \mark\http\Client\Client::getInstance()
                                          ->addHeader('pattern', Authorize::getInstance()->getPattern())
                                          ->addHeader('auth-type', Authorize::$_type)
                                          ->addHeader('auth-version', Authorize::$_version)
                                          ->setCache(false)
                                          ->get($token_url, 'json')
                                          ->api_decode();

        if ($client->getResponseCode() != 200 || $client->isEmpty()) {
            // throw new OAuthException($client->getResponseReason(), $client->getResponseCode() ?: 404);
        }

        return $client;
    }

    /**
     * 获取微信基础AccessToken
     *
     * @param bool $cache
     *
     * @return \mark\response\Response
     */
    public function getBaseAccessToken(bool $cache = true): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($this->secret)) {
            return Response::create('', 412, '', '缺少AppSecret参数');
        }

        $cacheKey = 'auth:sso:client:base_access_token:appid:' . $this->appid;
        // $token_url = Authorize::$host . '/api.php/union/token?grant_type=client_credential&appid=' . $this->appid . '&secret=' . $this->secret;
        $token_url = Authorize::getInstance()->getHost() . '/api.php/union/token?grant_type=client_credential&appid=' . $this->appid . '&secret=' . $this->secret;
        $client = \mark\http\Client\Client::getInstance()
                                          ->addHeader('pattern', Authorize::getInstance()->getPattern())
                                          ->addHeader('auth-type', Authorize::$_type)
                                          ->addHeader('auth-version', Authorize::$_version)
                                          ->setExpire(7000)
                                          ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                                          ->setCacheKey($cacheKey)
                                          ->setCache($cache)
                                          ->setCallback(function (Response $response, \mark\http\Client\Client $client) {
                                              if ($response->api_decode()->getResponseCode() != 200) {
                                                  $client->setCache(false);
                                                  $client->clearCache();
                                              }

                                              return $response;
                                          })
                                          ->get($token_url, 'json')
                                          ->api_decode();

        if ($client->getResponseCode() != 200 || $client->isEmpty()) {
            // throw new OAuthException($client->getResponseReason(), $client->getResponseCode() ?: 404);
        }

        return $client;
    }
}