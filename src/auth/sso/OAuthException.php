<?php
declare (strict_types=1);

namespace mark\auth\sso;

/**
 * Class OAuthException
 *
 * @package mark\auth\sso
 */
class OAuthException extends \OAuthException {

    /**
     * @var string[]
     */
    public static $error_code = [
        412 => '缺少参数',
        40029 => '无效的Code',
        41008 => '缺少Code参数',
        20001 => '配置文件损坏或无法读取，请重新执行intall',
        30001 => 'The state does not match. You may be a victim of CSRF.',
        50001 => '<h2>可能是服务器无法请求https协议</h2>可能未开启curl支持,请尝试开启curl支持，重启web服务器，如果问题仍未解决，请联系我们'
    ];

    /**
     * OAuthException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, \Throwable $previous = null) {
        if (empty($message)) {
            $message = self::$error_code[$code] ?? '';
        }

        parent::__construct($message, $code, $previous);
    }

}