<?php
declare (strict_types=1);

use mark\auth\Authorize;

if (!function_exists('hasPermission')) {
    /**
     * 校验是否拥有权限
     *
     * @param string $identifier
     * @param string $permission
     * @param int    $roleid
     * @param bool   $cache
     *
     * @return bool
     */
    function hasPermission($identifier = '', $permission = 'button', $roleid = 0, $cache = true): bool {
        return Authorize::getInstance()->hasPermission($identifier, $permission, $roleid, $cache);
    }
}

if (!function_exists('is_allow')) {
    /**
     * 校验是否允许访问
     *
     * @param string $identifier
     * @param int    $roleid
     * @param bool   $cache
     *
     * @return bool
     */
    function is_allow($identifier = '', $roleid = 0, $cache = true): bool {
        return Authorize::getInstance()->isAllow($identifier, $roleid, $cache);
    }
}