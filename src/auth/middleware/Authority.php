<?php
declare (strict_types=1);

namespace mark\auth\middleware;

use mark\auth\Authorize;
use mark\auth\entity\AuthInfo;
use mark\auth\model\ChannelModel;
use mark\auth\sso\Client;
use mark\response\Response;

/**
 * Class Authority
 *
 * @package mark\auth\middleware
 */
abstract class Authority {

    /**
     * 初始化
     */
    protected function initialize(): void { }

    /**
     * 权限校验处理器
     *
     * @param $request
     *
     * @return \mark\response\Response
     */
    final protected function handler($request): Response {
        // 初始化
        $this->initialize();

        // 1、获取频道标识符
        $identifier = $this->getInternalIdentifier();
        Authorize::getInstance()->setIdentifier($identifier);
        if (empty($identifier)) {
            Authorize::getInstance()->getLogcat()->error('Authority::handler(无效的频道标识符)'
                . json_encode(array('identifier' => $identifier,
                                    'Param' => $request->param(),
                                    'Server' => $request->server(),
                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);
            $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）无效的频道标识符');
            return Response::create($identifier, 404, '', '无效的频道标识符');
        }

        // 2、排除自定义标识符：存在Bug，以下情况可能会排除
        if ($this->has_exclude($identifier)) {
            Authorize::getInstance()->getLogcat()->debug('Authority::handler(has_exclude)' . $identifier);
            return Response::create('', 200, 'exclude ignore identifier', '排除自定义标识符');
        }

        // 3、校验标识符，校验频道状态
        $channel_resp = ChannelModel::getIdentifier(Authorize::getInstance()->getAppId(), $identifier, Authorize::getInstance()->isCache());
        if ($channel_resp->getCode() != 200 || $channel_resp->isEmpty()) {
            Authorize::getInstance()->getLogcat()->warning('Authority::handler(ChannelInfo)' . json_encode($channel_resp->getOrigin(), JSON_UNESCAPED_UNICODE));
        }
        $channel = $channel_resp->toArray();

        // 4、管理员或测试员 不检查频道状态
        if (Authorize::isAdmin() || Authorize::isTesting()) {
            Authorize::getInstance()->getLogcat()->debug('Authority::handler(Check Super Manager has Channel privileges)' . $identifier);
            // return Response::create();
        }

        if (empty($channel) || empty($channel['channelid'] ?? '')) {
            Authorize::getInstance()->getLogcat()->error('Authority::handler(404 无效的频道信息)'
                . json_encode(array('identifier' => $identifier,
                                    'Param' => $request->param(),
                                    'Channel' => $channel,
                                    'Server' => $request->server(),
                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);
            $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）无效频道：' . $identifier);
            return Response::create('', 404, 'Invalid Channel information', '无效的频道信息');
        }

        switch ($channel['status'] ?? '') {
            case  0: // '初始状态', 'name' => 'invalid', 'describe' => '频道初始创建状态'),
                $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                    Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）频道未启用：' . $identifier);
                return Response::create('', 410, 'Channel unEnabled', '频道未启用');
            // not break;
            case  1: // '待提交', 'name' => 'initial', 'describe' => '创建频道时，在完善信息后保存，该频道便处于待提交的状态。'),
                $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                    Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）频道待提交：' . $identifier);
                break;
            case  2: // '审核中', 'name' => 'handle', 'describe' => '提交测试之后，频道就会处于该状态等待审核。'),
                $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                    Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）频道审核中：' . $identifier);
                break;
            case  3: // '测试中', 'name' => 'testing', 'describe' => '频道已被授受测试'),
                $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                    Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）频道测试中：' . $identifier);
                break;
            case  4: // '已删除', 'name' => 'disable', 'describe' => '频道已被删除'),
                $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                    Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）频道已删除：' . $identifier);
                return Response::create('', 410, 'Channel disabled', '频道已删除');
            // not break;
            case  5: // '已驳回', 'name' => 'failure', 'describe' => '频道未通过测试，请查看未通过原因并修改后进行重新提交'),
                $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                    Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）频道已驳回：' . $identifier);
                return Response::create('', 410, 'Channel failure', '频道已驳回');
            // not break;
            case  6: // '已发布', 'name' => 'release', 'describe' => '交易成功、发布、上线'),
                break;
            case 7: // '已过时', 'name' => 'deprecated', 'describe' => '频道已过时，该频道将逐渐被新的功能所替代'),
                $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                    Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）频道已过时：' . $identifier);
                break;
            case  71: // '已冻结', 'name' => 'freezing', 'describe' => '频道冻结后，该频道将无法展现，直至冻结期限结束'),
                $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                    Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）频道已冻结：' . $identifier);
                return Response::create('', 410, 'Channel freezing', '频道已冻结');
            // not break;
            case  8: // '已下架', 'name' => 'processed', 'describe' => '频道已下架'),
                $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                    Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）频道已下架：' . $identifier);
                return Response::create('', 410, 'Channel processed', '频道已下架');
            // not break;
            case  9: // '维护中', 'name' => 'maintain', 'describe' => '系统维护中')
                $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                    Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）频道维护中：' . $identifier);
                break;
            default:
                Authorize::getInstance()->getLogcat()->error('Authority::handler(410 频道状态异常)'
                    . json_encode(array('identifier' => $identifier,
                                        'Param' => $request->param(),
                                        'Channel' => $channel,
                                        'Server' => $request->server(),
                                        'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);
                $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                    Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）频道状态异常：' . $identifier);
                return Response::create('', 410, 'Channel information not available', '频道状态异常');
            // not break;
        }

        // 5、检查频道是否需要权限检查：公开页面，无需检查
        if (!isset($channel[AuthInfo::$modifier])) {
            Authorize::getInstance()->getLogcat()->error('Authority::handler(无效的频道标识符)'
                . json_encode(array('identifier' => $identifier,
                                    'Param' => $request->param(),
                                    'Channel' => $channel,
                                    'Server' => $request->server(),
                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

            $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）无效的频道标识符：' . $identifier);
            return Response::create('', 404, 'Invalid Channel identifier', '无效的频道标识符');
        }

        // 6、检查频道是否需要权限检查
        switch ($channel[AuthInfo::$modifier] ?? '') {
            case AuthInfo::$public:
                // 6.1、开放权限：仅登录即可
                Authorize::getInstance()->getLogcat()->debug('公开页面，无需检查：' . $identifier);
                return Response::create();
            case AuthInfo::$default:
                // 6.2、默认权限：仅登录即可
                // 6.2.1、校验异步请求
                if (!Authorize::isLogin()) {
                    Authorize::getInstance()->getLogcat()->debug('默认权限，仅登录即可：' . $identifier);
                    if (is_json($request->get('accept', $request->header('accept', ''))) || is_ajax() || is_pjax()) {
                        Authorize::getInstance()->getLogcat()->warning('Authority::handler(ajax checkChannel 401 身份认证)'
                            . json_encode(array('Param' => $request->param(),
                                                'Channel' => $channel,
                                                'Server' => $request->server(),
                                                'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                        return Response::create('', 401, 'Ajax Unauthorized', '请求用户的身份认证');
                    }

                    if (!is_get() && !$request->isGet()) {
                        Authorize::getInstance()->getLogcat()->warning('Authority::handler(401 用户未登录)');
                        return Response::create('', 401, 'Unauthorized', '用户未登录');
                    }

                    $client = Client::getInstance(true)
                                    ->setAppID(Authorize::getInstance()->getAppId())
                                    ->setSecret(Authorize::getInstance()->getSecret());

                    if ($request->has('errcode', 'get', true)) {
                        switch ((int)$request->get('errcode', '')) {
                            case 404: // 未找到相关授权
                                return $this->unAuthorized($request->get());
                            // not break;
                        }
                    }

                    if (!$request->has('code', 'get', true)) {
                        // @todo 默认权限，但未登录时，无法授权；本想舍弃auth_base作用域，此处暂时用auth_union用代替。
                        // @todo 随后将默认权限频道改为仅为异步接口使用，不作界面使用。
                        return $client->getCode($request->url(true), AuthInfo::$auth_union);
                    }

                    $token = $client->getAccessToken($request->get('code', ''), $request->get('state', ''))->toArray();
                    $userinfo = $client->getUserInfo($token['access_token'] ?? '', $token['openid'] ?? '', Authorize::getInstance()->getLang())->toArray();
                    if (!empty($userinfo) && is_array($userinfo) && !empty($userinfo['openid'] ?? '')) {
                        Authorize::getInstance()->getLogcat()->debug('Authority::handler(Default.UserInfo)' . json_encode($userinfo, JSON_UNESCAPED_UNICODE));
                        $this->onAuthorized($userinfo);
                        return Response::create();
                    }

                    Authorize::getInstance()->getLogcat()->warning('Authority::handler(Default.Result)'
                        . json_encode(array('userinfo' => $userinfo,
                                            'Param' => $request->param(),
                                            'Channel' => $channel,
                                            'Server' => $request->server(),
                                            'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                    $this->unAuthorized($userinfo);
                    return Response::create('', 407, 'Proxy Authentication Required Authorize', '权限不足，无法访问该页面');
                }

                if (Authorize::isLogin()) {
                    return Response::create();
                }
                break;
            case AuthInfo::$proteced:
                // 6.3、保护权限：需要授权
                if (!Authorize::isUnion(array(), AuthInfo::$auth_union) || $request->get('action', '') === 'choice') {
                    Authorize::getInstance()->getLogcat()->debug('保护权限：需要授权：' . $identifier);
                    // 获取联合授权
                    if (is_json($request->get('accept', $request->header('accept', ''))) || is_ajax() || is_pjax()) {
                        Authorize::getInstance()->getLogcat()->warning('Authority::handler(Proteced 异步请求需要授权认证)'
                            . json_encode(array('Param' => $request->param(),
                                                'Channel' => $channel,
                                                'Server' => $request->server(),
                                                'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                        return Response::create('', 407, 'Asyn Proxy Authentication Required Union', '需要授权认证');
                    }

                    if (is_get() || $request->isGet()) {
                        $client = Client::getInstance(true)
                                        ->setAppID(Authorize::getInstance()->getAppId())
                                        ->setSecret(Authorize::getInstance()->getSecret());

                        if ($request->has('errcode', 'get', true)) {
                            switch ((int)$request->get('errcode', '')) {
                                case 404: // 未找到相关授权
                                    return $this->unAuthorized($request->get());
                                // not break;
                            }
                        }

                        if (!$request->has('code', 'get', true)) {
                            return $client->getCode($request->url(true), AuthInfo::$auth_union);
                        }

                        $token = $client->getAccessToken($request->get('code', ''), $request->get('state', ''))->toArray();
                        $userinfo = $client->getUserInfo($token['access_token'] ?? '', $token['openid'] ?? '', Authorize::getInstance()->getLang())->toArray();
                        if (!empty($userinfo) && is_array($userinfo) && !empty($userinfo['openid'] ?? '')) {
                            Authorize::getInstance()->getLogcat()->debug('Authority::handler(Union.UserInfo)' . json_encode($userinfo, JSON_UNESCAPED_UNICODE));
                            $this->onAuthorized($userinfo);
                        } else {
                            Authorize::getInstance()->getLogcat()->warning('Authority::handler(Union.Result)'
                                . json_encode(array('userinfo' => $userinfo,
                                                    'Param' => $request->param(),
                                                    'Channel' => $channel,
                                                    'Server' => $request->server(),
                                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                            $this->unAuthorized($userinfo);
                            return Response::create('', 407, 'Proxy Authentication Required Union', '权限不足，无法访问该页面');
                        }
                    }
                }

                if (!Authorize::isUnion(array(), AuthInfo::$auth_union)) {
                    Authorize::getInstance()->getLogcat()->warning('Authority::handler(Proxy Authentication Required Union)'
                        . json_encode(array('Param' => $request->param(),
                                            'Channel' => $channel,
                                            'Server' => $request->server(),
                                            'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                    // return Response::create('', 407, 'Proxy Authentication Required Union');
                }
                break;
            case AuthInfo::$corporate:
                // 6.4、企业权限：员工授权
                if (!Authorize::isCorporate() || $request->get('action', '') === 'choice') {
                    Authorize::getInstance()->getLogcat()->debug('企业权限：员工授权：' . $identifier);
                    if (is_json($request->get('accept', $request->header('accept', ''))) || is_ajax() || is_pjax()) {
                        Authorize::getInstance()->getLogcat()->warning('Authority::handler(corporate 异步请求需要授权认证)'
                            . json_encode(array('Param' => $request->param(),
                                                'Channel' => $channel,
                                                'Server' => $request->server(),
                                                'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                        return Response::create('', 407, 'Asyn Proxy Authentication Required Corporate', '需要授权认证');
                    }

                    if (is_get() || $request->isGet()) {
                        $client = Client::getInstance(true)
                                        ->setAppID(Authorize::getInstance()->getAppId())
                                        ->setSecret(Authorize::getInstance()->getSecret())
                                        ->setCorpid(Authorize::getInstance()->getCorpid());

                        if ($request->has('errcode', 'get', true)) {
                            switch ((int)$request->get('errcode', '')) {
                                case 404: // 未找到相关授权
                                    return $this->unAuthorized($request->get());
                                // not break;
                            }
                        }

                        if (!$request->has('code', 'get', true)) {
                            return $client->getCode($request->url(true), AuthInfo::$auth_corp);
                        }

                        $token = $client->getAccessToken($request->get('code', ''), $request->get('state', ''))->toArray();
                        $userinfo = $client->getUserInfo($token['access_token'] ?? '', $token['openid'] ?? '', Authorize::getInstance()->getLang())->toArray();

                        if (!empty($userinfo) && is_array($userinfo) && isset($userinfo['openid']) && !empty($userinfo['openid'])) {
                            Authorize::getInstance()->getLogcat()->debug('Authority::handler(Corporate.UserInfo)' . json_encode($userinfo, JSON_UNESCAPED_UNICODE));
                            $this->onAuthorized($userinfo);
                        } else {
                            Authorize::getInstance()->getLogcat()->warning('Authority::handler(Corporate.Result)'
                                . json_encode(array('userinfo' => $userinfo,
                                                    'Param' => $request->param(),
                                                    'Channel' => $channel,
                                                    'Server' => $request->server(),
                                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                            $this->unAuthorized($userinfo);
                            return Response::create('', 407, 'Proxy Authentication Required Corporate', '权限不足，无法访问该页面');
                        }
                    }
                }

                if (!Authorize::isCorporate()) {
                    Authorize::getInstance()->getLogcat()->warning('Authority::handler(Proxy Authentication Required Corporate)'
                        . json_encode(array('Param' => $request->param(),
                                            'Channel' => $channel,
                                            'Server' => $request->server(),
                                            'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                    // return Response::create('', 407, 'Proxy Authentication Required Corporate');
                }
                break;
            case AuthInfo::$merchant:
                // 6.4、商户权限：成员授权
                if (!Authorize::isMerchant() || $request->get('action', '') === 'choice') {
                    Authorize::getInstance()->getLogcat()->debug('商户权限：成员授权：' . $identifier);
                    if (is_json($request->get('accept', $request->header('accept', ''))) || is_ajax() || is_pjax()) {
                        Authorize::getInstance()->getLogcat()->warning('Authority::handler(merchant 异步请求需要授权认证)'
                            . json_encode(array('Param' => $request->param(),
                                                'Channel' => $channel,
                                                'Server' => $request->server(),
                                                'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                        return Response::create('', 407, 'Asyn Proxy Authentication Required Merchant', '需要授权认证');
                    }

                    if (is_get() || $request->isGet()) {
                        $client = Client::getInstance(true)
                                        ->setAppID(Authorize::getInstance()->getAppId())
                                        ->setSecret(Authorize::getInstance()->getSecret())
                                        ->setCorpid(Authorize::getInstance()->getCorpid())
                                        ->setMchid(Authorize::getInstance()->getMchid());

                        if ($request->has('errcode', 'get', true)) {
                            switch ((int)$request->get('errcode', '')) {
                                case 404: // 未找到相关授权
                                    return $this->unAuthorized($request->get());
                                // not break;
                            }
                        }

                        if (!$request->has('code', 'get', true)) {
                            return $client->getCode($request->url(true), AuthInfo::$auth_merchant);
                        }

                        $token = $client->getAccessToken($request->get('code', ''), $request->get('state', ''))->toArray();
                        $userinfo = $client->getUserInfo($token['access_token'] ?? '', $token['openid'] ?? '', Authorize::getInstance()->getLang())->toArray();

                        if (!empty($userinfo) && is_array($userinfo) && isset($userinfo['openid']) && !empty($userinfo['openid'])) {
                            Authorize::getInstance()->getLogcat()->debug('Authority::handler(Merchant.UserInfo)' . json_encode($userinfo, JSON_UNESCAPED_UNICODE));
                            $this->onAuthorized($userinfo);
                        } else {
                            Authorize::getInstance()->getLogcat()->warning('Authority::handler(Merchant.Result)'
                                . json_encode(array('userinfo' => $userinfo,
                                                    'Param' => $request->param(),
                                                    'Channel' => $channel,
                                                    'Server' => $request->server(),
                                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                            $this->unAuthorized($userinfo);
                            return Response::create('', 407, 'Proxy Authentication Required Merchant', '权限不足，无法访问该页面');
                        }
                    }
                }

                if (!Authorize::isMerchant()) {
                    Authorize::getInstance()->getLogcat()->warning('Authority::handler(Proxy Authentication Required Merchant)'
                        . json_encode(array('Param' => $request->param(),
                                            'Channel' => $channel,
                                            'Server' => $request->server(),
                                            'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                    // return Response::create('', 407, 'Proxy Authentication Required Merchant');
                }
                break;
            case AuthInfo::$project:
                // 6.5、项目权限：成员授权
                if (!Authorize::isProject() || $request->get('action', '') === 'choice') {
                    Authorize::getInstance()->getLogcat()->debug('项目权限：成员授权：' . $identifier);
                    if (is_json($request->get('accept', $request->header('accept', ''))) || is_ajax() || is_pjax()) {
                        Authorize::getInstance()->getLogcat()->warning('Authority::handler(Project 异步请求需要授权认证)'
                            . json_encode(array('Param' => $request->param(),
                                                'Channel' => $channel,
                                                'Server' => $request->server(),
                                                'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                        return Response::create('', 407, 'Asyn Proxy Authentication Required Project', '需要授权认证');
                    }

                    if (is_get() || $request->isGet()) {
                        $client = Client::getInstance(true)
                                        ->setAppID(Authorize::getInstance()->getAppId())
                                        ->setSecret(Authorize::getInstance()->getSecret())
                                        ->setCorpid(Authorize::getInstance()->getCorpid())
                                        ->setMchid(Authorize::getInstance()->getMchid())
                                        ->setProjectid(Authorize::getInstance()->getProjectid());

                        if ($request->has('errcode', 'get', true)) {
                            switch ((int)$request->get('errcode', '')) {
                                case 404: // 未找到相关授权
                                    return $this->unAuthorized($request->get());
                                // not break;
                            }
                        }

                        if (!$request->has('code', 'get', true)) {
                            return $client->getCode($request->url(true), AuthInfo::$auth_project);
                        }

                        $token = $client->getAccessToken($request->get('code', ''), $request->get('state', ''))->toArray();
                        $userinfo = $client->getUserInfo($token['access_token'] ?? '', $token['openid'] ?? '', Authorize::getInstance()->getLang())->toArray();

                        if (!empty($userinfo) && is_array($userinfo) && !empty($userinfo['openid'] ?? '')) {
                            Authorize::getInstance()->getLogcat()->debug('Authority::handler(Project.UserInfo)' . json_encode($userinfo, JSON_UNESCAPED_UNICODE));
                            $this->onAuthorized($userinfo);
                        } else {
                            Authorize::getInstance()->getLogcat()->warning('Authority::handler(Project.Result)'
                                . json_encode(array('userinfo' => $userinfo,
                                                    'Param' => $request->param(),
                                                    'Channel' => $channel,
                                                    'Server' => $request->server(),
                                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                            $this->unAuthorized($userinfo);
                            return Response::create('', 407, 'Proxy Authentication Required Project', '权限不足，无法访问该页面');
                        }
                    }
                }

                if (!Authorize::isProject()) {
                    Authorize::getInstance()->getLogcat()->warning('Authority::handler(Proxy Authentication Required Project)'
                        . json_encode(array('Param' => $request->param(),
                                            'Channel' => $channel,
                                            'Server' => $request->server(),
                                            'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                    // return Response::create('', 407, 'Proxy Authentication Required Project');
                }
                break;
            case AuthInfo::$property:
                // 6.5、物业权限：物业授权
                if (!Authorize::isProperty() || $request->get('action', '') === 'choice') {
                    Authorize::getInstance()->getLogcat()->debug('物业权限：物业授权：' . $identifier);
                    if (is_json($request->get('accept', $request->header('accept', ''))) || is_ajax() || is_pjax()) {
                        Authorize::getInstance()->getLogcat()->warning('Authority::handler(Property 异步请求需要授权认证)'
                            . json_encode(array('Param' => $request->param(),
                                                'Channel' => $channel,
                                                'Server' => $request->server(),
                                                'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                        return Response::create('', 407, 'Asyn Proxy Authentication Required Property', '需要授权认证');
                    }

                    if (is_get() || $request->isGet()) {
                        $client = Client::getInstance(true)
                                        ->setAppID(Authorize::getInstance()->getAppId())
                                        ->setSecret(Authorize::getInstance()->getSecret())
                                        ->setCorpid(Authorize::getInstance()->getCorpid())
                                        ->setProjectid(Authorize::getInstance()->getProjectid());

                        if ($request->has('errcode', 'get', true)) {
                            switch ((int)$request->get('errcode', '')) {
                                case 404: // 未找到相关授权
                                    return $this->unAuthorized($request->get());
                                // not break;
                            }
                        }

                        if (!$request->has('code', 'get', true)) {
                            return $client->getCode($request->url(true), AuthInfo::$auth_property);
                        }

                        $token = $client->getAccessToken($request->get('code', ''), $request->get('state', ''))->toArray();
                        $userinfo = $client->getUserInfo($token['access_token'] ?? '', $token['openid'] ?? '', Authorize::getInstance()->getLang())->toArray();

                        if (!empty($userinfo) && is_array($userinfo) && !empty($userinfo['openid'] ?? '')) {
                            Authorize::getInstance()->getLogcat()->debug('Authority::handler(Property.UserInfo)' . json_encode($userinfo, JSON_UNESCAPED_UNICODE));
                            $this->onAuthorized($userinfo);
                        } else {
                            Authorize::getInstance()->getLogcat()->warning('Authority::handler(Property.Result)'
                                . json_encode(array('userinfo' => $userinfo,
                                                    'Param' => $request->param(),
                                                    'Channel' => $channel,
                                                    'Server' => $request->server(),
                                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                            $this->unAuthorized($userinfo);
                            return Response::create('', 407, 'Proxy Authentication Required Property', '权限不足，无法访问该页面');
                        }
                    }
                }

                if (!Authorize::isProperty()) {
                    Authorize::getInstance()->getLogcat()->warning('Authority::handler(Proxy Authentication Required Property)'
                        . json_encode(array('Param' => $request->param(),
                                            'Channel' => $channel,
                                            'Server' => $request->server(),
                                            'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                    // return Response::create('', 407, 'Proxy Authentication Required Property');
                }
                break;
            case AuthInfo::$community:
                // 6.6、社区权限：业主授权
                if (!Authorize::isCommunity() || $request->get('action', '') === 'choice') {
                    Authorize::getInstance()->getLogcat()->debug('社区权限：业主授权：' . $identifier);
                    // 获取联合授权
                    if (is_json($request->get('accept', $request->header('accept', ''))) || is_ajax() || is_pjax()) {
                        Authorize::getInstance()->getLogcat()->warning('Authority::handler(community 异步请求需要授权认证)'
                            . json_encode(array('Param' => $request->param(),
                                                'Channel' => $channel,
                                                'Server' => $request->server(),
                                                'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                        return Response::create('', 407, 'Asyn Proxy Authentication Required', '需要授权认证');
                    }

                    if (is_get() || $request->isGet()) {
                        $client = Client::getInstance(true)
                                        ->setAppID(Authorize::getInstance()->getAppId())
                                        ->setSecret(Authorize::getInstance()->getSecret())
                                        ->setCorpid(Authorize::getInstance()->getCorpid())
                                        ->setProjectid(Authorize::getInstance()->getProjectid());

                        if ($request->has('errcode', 'get', true)) {
                            switch ((int)$request->get('errcode', '')) {
                                case 404: // 未找到相关授权
                                    return $this->unAuthorized($request->get());
                                // not break;
                            }
                        }

                        if (!$request->has('code', 'get', true)) {
                            return $client->getCode($request->url(true), AuthInfo::$auth_community);
                        }

                        $token = $client->getAccessToken($request->get('code', ''), $request->get('state', ''))->toArray();
                        $userinfo = $client->getUserInfo($token['access_token'] ?? '', $token['openid'] ?? '', Authorize::getInstance()->getLang())->toArray();
                        if (!empty($userinfo) && is_array($userinfo) && !empty($userinfo['openid'] ?? '')) {
                            Authorize::getInstance()->getLogcat()->debug('Authority::handler(Community.UserInfo)' . json_encode($userinfo, JSON_UNESCAPED_UNICODE) . PHP_EOL);
                            $this->onAuthorized($userinfo);
                        } else {
                            Authorize::getInstance()->getLogcat()->warning('Authority::handler(Community.Result)'
                                . json_encode(array('userinfo' => $userinfo,
                                                    'Param' => $request->param(),
                                                    'Channel' => $channel,
                                                    'Server' => $request->server(),
                                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                            $this->unAuthorized($userinfo);

                            return Response::create('', 407, 'Proxy Authentication Required Community', '权限不足，无法访问该页面');
                        }
                    }
                }

                if (!Authorize::isCommunity()) {
                    Authorize::getInstance()->getLogcat()->warning('Authority::handler(Proxy Authentication Required Community)'
                        . json_encode(array('Param' => $request->param(),
                                            'Channel' => $channel,
                                            'Server' => $request->server(),
                                            'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);
                    // return Response::create('', 407, 'Proxy Authentication Required Community');
                }
                break;
            default:
                // 6.9、其它权限：暂未授权
                Authorize::getInstance()->getLogcat()->warning('Authority::handler(Invalid Channel identifier)' . $identifier . PHP_EOL
                    . json_encode(array('Param' => $request->param(),
                                        'Channel' => $channel,
                                        'Server' => $request->server(),
                                        'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

                $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                    Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）频道标识符异常：' . $identifier);
                return Response::create('', 400, 'Invalid Channel identifier', '频道标识符异常');
        }

        // 7、校验授权信息
        $access_resp = ChannelModel::getAccess(Authorize::getInstance()->getAppId(),
            $channel['channelid'] ?? 404,
            $this->session->get('union.roleid', 404),
            Authorize::getInstance()->isCache());
        if ($access_resp->getCode() != 200 || $access_resp->isEmpty()) {
            Authorize::getInstance()->getLogcat()->warning('Authority::handler(AccessInfo)' . json_encode($access_resp->getOrigin(), JSON_UNESCAPED_UNICODE));
        }
        $access = $access_resp->toArray();

        // 7.1、无效的授权信息
        if (empty($access)) {
            Authorize::getInstance()->getLogcat()->error('Authority::handler(407 无效的授权信息)'
                . json_encode(array('Access' => $access,
                                    'Channel' => $channel,
                                    'Server' => $request->server(),
                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

            $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）无效的授权信息：' . $identifier);
            return Response::create('', 407, 'Invalid authorization information', '无效的授权信息');
        }

        // 7.2、授权信息已被禁用
        if (!isset($access['status']) || $access['status'] != 1) {
            Authorize::getInstance()->getLogcat()->warning('Authority::handler(407 授权信息已被禁用)'
                . json_encode(array('Access' => $access,
                                    'Channel' => $channel,
                                    'Server' => $request->server(),
                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

            $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）授权信息已被禁用：' . $identifier);
            return Response::create('', 407, 'Authorization information has been disabled', '授权已被禁用');
        }

        // 7.3 权限不足
        if (!isset($access['allow']) || $access['allow'] != 1) {
            Authorize::getInstance()->getLogcat()->warning('Authority::handler(402 权限不足，无法访问该页面)'
                . json_encode(array('Access' => $access,
                                    'Channel' => $channel,
                                    'Server' => $request->server(),
                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

            $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）402 权限不足，无法访问该页面：' .
                $identifier);
            return Response::create('', 402, 'Insufficient authority not allow access', '权限不足，无法访问该页面');
        }

        // 7.4、禁止访问
        if (!isset($access['method']) || stripos($access['method'], $request->method()) === false) {
            Authorize::getInstance()->getLogcat()->warning('Authority::handler(405 该页面禁止 ' . $request->method() . ' 方法请求)'
                . json_encode(array('Access' => $access,
                                    'Channel' => $channel,
                                    'Server' => $request->server(),
                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

            $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）405 该页面禁止 ' .
                $request->method() . ' 方法请求：' . $identifier);
            return Response::create('', 405, $request->method() . ' Method Not Allowed', '该页面禁止' . $request->method() . '请求');
        }

        // 7.5、禁止Ajax请求
        if (!isset($access['method']) || (stripos($access['method'], 'ajax') === false && is_ajax())) {
            Authorize::getInstance()->getLogcat()->warning('Authority::handler(405 该页面禁止Ajax请求)'
                . json_encode(array('Access' => $access,
                                    'Channel' => $channel,
                                    'Server' => $request->server(),
                                    'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

            $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
                Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）405 该页面禁止Ajax请求：' .
                $identifier);
            return Response::create('', 405, 'Ajax Method Not Allowed', '该页面禁止Ajax请求');
        }

        // 7.6、许可访问
        if (!empty($access['method'] ?? '') && stripos($access['method'], $request->method()) !== false) {
            return Response::create();
        }

        // 7.9、异常授权
        Authorize::getInstance()->getLogcat()->error('Authority::handler(Not Acceptable：406 授权信息异常)'
            . json_encode(array('Access' => $access,
                                'Channel' => $channel,
                                'Server' => $request->server(),
                                'Session' => $request->session()), JSON_UNESCAPED_UNICODE) . PHP_EOL);

        $this->runevent('ogg465nBPci7n9oNCXzMwg5e2Gbo', $request->url(true),
            Authorize::getInstance()->getAppId() . '、' . $request->session('nickname', '佚名') . '（' . $request->session('union.roleid', 404) . '）Not Acceptable：授权信息异常：' .
            $identifier);
        return Response::create('', 406, 'Not Acceptable', '授权信息异常');
    }

    /**
     * 获取内部排除项
     *
     * @return string
     */
    private function getInternalIdentifier(): string {
        $identifier = $this->getIdentifier();
        if (!empty($identifier)) {
            return strtolower($identifier);
        }

        return $identifier;
    }

    /**
     * 获取访问标识符 * 权限标识符（Identifier）推荐使用 resource:action 形式命名，如 email:login
     *
     * @return string
     */
    abstract protected function getIdentifier(): string;

    /**
     * 获取内部排除项
     *
     * @return array
     */
    private function internalIgnore(): array {
        return array_merge($this->getIgnore(),
            array('/',
                  'index:*', 'index:index:index', 'index.htm', 'index.html', 'index.php',
                  'portal:*', 'portal:index:index', 'portal.htm', 'portal.html', 'portal.php',
                  'api:*', 'api:index:index',
                  'captcha:*', '*:captcha:*', 'captcha:index:index',
                  'logo.png', 'favicon.ico', 'favicon.svg', 'rotobs.txt',
                  '404.htm', '404.html',
                  '500.htm', '500.html',
                  '502.htm', '502.html'));
    }

    /**
     * 获取排除项
     *
     * @return array
     */
    abstract protected function getIgnore(): array;

    /**
     * 自定义排除算法，排除当前标识符
     *
     * @param string $identifier
     *
     * @return bool
     */
    protected function has_exclude(string $identifier): bool {
        Authorize::getInstance()->getLogcat()->debug('Authority::has_exclude(' . $identifier . ')');
        $ignore = $this->internalIgnore();

        return !empty($ignore) && is_array($ignore) && !empty($identifier) && in_array($identifier, $ignore, true);
    }

    /**
     * 验证频道
     *
     * @param string $identifier
     *
     * @return bool
     */
    abstract protected function has_channel(string $identifier): bool;

    /**
     * 验证角色
     *
     * @param array $role
     *
     * @return bool
     */
    abstract protected function has_role(array $role): bool;

    /**
     * 验证联合授权
     *
     * @param array $union
     *
     * @return bool
     */
    abstract protected function has_union(array $union): bool;

    /**
     * 验证权限
     *
     * @param array $permission
     *
     * @return bool
     */
    abstract protected function has_permission(array $permission): bool;

    /**
     * OAuth2 获取到用户Openid后，由实现类存储
     *
     * @param string $openid
     *
     * @return bool
     */
    abstract protected function onOpenid(string $openid): bool;

    /**
     * OAuth2 获取到用户信息后，由实现类处理数据的存储
     *
     * @param array $userinfo
     */
    abstract protected function onAuthorized(array $userinfo): void;

    /**
     * 未授权的
     *
     * @param array $response
     *
     * @return \mark\response\Response
     */
    protected function unAuthorized(array $response): Response {
        return Response::create('', (int)($response['errcode'] ?? 404), '', $response['reason'] ?? '');
    }

    /**
     * 用户已注销
     *
     * @return bool
     */
    protected function onCancelled(): bool { return true; }

    /**
     * 系统运行事件
     *
     * @param string $openid
     * @param string $first
     * @param string $source
     * @param int    $time
     * @param string $content
     * @param string $url
     * @param string $remark
     */
    protected function runevent(string $openid, string $first = '', string $source = '', $time = 0, $content = '', $url = '', $remark = '') { }
}