<?php
declare (strict_types=1);

namespace mark\auth;

use mark\auth\entity\AppInfo;
use mark\auth\entity\AuthInfo;
use mark\auth\model\ChannelModel;
use mark\auth\tool\Logcat;
use mark\cache\CacheHandle;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use think\facade\Config;
use think\facade\Session;

/**
 * Class Authorize
 *
 * @package mark\auth
 */
final class Authorize {
    public static $login = 'login';
    public static $isLogin = 'isLogin';
    public static $isAdmin = 'isAdmin';
    public static $isTesting = 'isTesting';
    public static $isManager = 'isManager';
    public static $expiretime = 'expiretime';

    public static $host = 'https://auth.tianfu.ink';
    /**
     * @deprecated
     * @var string
     */
    private $_endpoint = '';
    public static $_type = 'OAuth2';
    public static $_version = '1.2.1';

    /**
     * @var int
     */
    public $expire = 7200;

    /**
     * 权限目录
     *
     * @var string
     */
    private static $authPath;

    private function __construct() {
        self::$authPath = __DIR__ . DIRECTORY_SEPARATOR;
    }

    /**
     * 获取权限目录
     *
     * @return string
     */
    public static function getAuthPath(): string {
        return self::$authPath ?: __DIR__ . DIRECTORY_SEPARATOR;
    }

    private static $instance;

    /**
     * @param bool $origin
     *
     * @return \mark\auth\Authorize
     */
    public static function getInstance(bool $origin = false): self {
        if (self::$instance == null || $origin) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @var string
     */
    private $appid = '';
    /**
     * @var string
     */
    private $poolid = '';
    /**
     * @var string
     */
    private $secret = '';
    /**
     * @var string
     */
    private $corpid = '';
    /**
     * @var string
     */
    private $mchid = '';
    /**
     * @var string
     */
    private $projectid = '';
    /**
     * @var string
     */
    private $lang = 'zh-cn';

    /**
     * @var string
     */
    private $identifier = '';

    /**
     * 设置授权接入域名
     *
     * @param string $endpoint
     *
     * @return static
     * @deprecated
     */
    final public function setEndpoint(string $endpoint): self {
        if (!empty($endpoint)) {
            $this->_endpoint = $endpoint;
        }

        return $this;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getEndpoint(): string {
        if (!empty($this->_endpoint)) {
            return $this->_endpoint;
        }

        return self::$host;
    }

    /**
     * @return string
     */
    public function getHost(): string {
        return self::$host;
    }

    /**
     * 设置AppID
     *
     * @param $appid
     *
     * @return $this
     */
    final public function setAppId(string $appid): self {
        if (!empty($appid)) {
            $this->appid = $appid;
        }

        return $this;
    }

    /**
     * 获取AppID
     *
     * @return string
     */
    final public function getAppId(): string {
        return $this->appid;
    }

    /**
     * @param $poolid
     *
     * @return $this
     */
    final public function setPoolId(string $poolid): self {
        if (!empty($poolid)) {
            $this->poolid = $poolid;
        }

        return $this;
    }

    /**
     * 获取PoolID
     *
     * @return string
     */
    final public function getPoolId(): string {
        return $this->poolid;
    }

    /**
     * @param string $secret
     *
     * @return $this
     */
    final public function setSecret(string $secret): self {
        if (!empty($secret)) {
            $this->secret = $secret;
        }

        return $this;
    }

    /**
     * @return string
     */
    final public function getSecret(): string {
        return $this->secret;
    }

    /**
     * 设置企业ID
     *
     * @param string $corpid
     *
     * @return $this
     */
    final public function setCorpid(string $corpid): self {
        if (!empty($corpid)) {
            $this->corpid = $corpid;
        }

        return $this;
    }

    /**
     * 获取企业ID
     *
     * @return string
     */
    final public function getCorpid(): string {
        return $this->corpid;
    }

    /**
     * 设置商户ID
     *
     * @param string $mchid
     *
     * @return $this
     */
    final public function setMchid(string $mchid): self {
        if (!empty($mchid)) {
            $this->mchid = $mchid;
        }

        return $this;
    }

    /**
     * 获取商户ID
     *
     * @return string
     */
    final public function getMchid(): string {
        return $this->mchid;
    }

    /**
     * 设置项目ID
     *
     * @param string $projectid
     *
     * @return $this
     */
    final public function setProjectid(string $projectid): self {
        if (!empty($projectid)) {
            $this->projectid = $projectid;
        }

        return $this;
    }

    /**
     * 获取项目ID
     *
     * @return string
     */
    final public function getProjectid(): string {
        return $this->projectid;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return $this
     */
    public function setIdentifier(string $identifier): self {
        if (!empty($identifier)) {
            $this->identifier = $identifier;
        }

        return $this;
    }

    /**
     * 设置语言
     *
     * @param string $lang
     *
     * @return $this
     */
    final public function setLang(string $lang = 'zh-cn'): self {
        if (!empty($lang)) {
            $this->lang = str_ireplace('_', '-', $lang);
        }

        return $this;
    }

    /**
     * 获取语言
     *
     * @return string
     */
    final public function getLang(): string {
        return $this->lang;
    }

    /**
     * @var \Psr\SimpleCache\CacheInterface
     */
    private $cache_handle;

    /**
     * 设置缓存实例
     *
     * @param \Psr\SimpleCache\CacheInterface $cache_handle
     *
     * @return $this
     */
    final public function setCacheHandle(CacheInterface $cache_handle): self {
        if ($cache_handle instanceof CacheInterface) {
            $this->cache_handle = new CacheHandle($cache_handle);
        }

        return $this;
    }

    /**
     * 获取缓存实例
     *
     * @return \Psr\SimpleCache\CacheInterface
     */
    final public function getCacheHandle(): CacheInterface {
        if ($this->cache_handle instanceof CacheInterface) {
            return $this->cache_handle;
        }

        return new CacheHandle();
    }

    /**
     * @var bool
     */
    private $cache = true;

    /**
     * 设置是否开启缓存
     *
     * @param bool $cache
     *
     * @return $this
     */
    public function setCache($cache = true): self {
        $this->cache = is_true($cache);

        return $this;
    }

    /**
     * @return bool
     */
    public function isCache(): bool {
        return $this->cache;
    }

    /**
     * @var bool
     */
    private $debug = false;

    /**
     * 设置是否开启调试模式
     *
     * @param bool $debug
     *
     * @return $this
     */
    final public function setDebug(bool $debug = false): self {
        $this->debug = $debug;

        return $this;
    }

    /**
     * 是否开启调试模式
     *
     * @return bool
     */
    final public function isDebug(): bool {
        return $this->debug;
    }

    /**
     * @var LoggerInterface
     */
    public $logcat;

    /**
     * 设置日志实例
     *
     * @param \Psr\Log\LoggerInterface $logcat
     *
     * @return $this
     */
    public function setLogcat(LoggerInterface $logcat): self {
        if ($logcat instanceof LoggerInterface) {
            $this->logcat = new Logcat($logcat);
        }

        return $this;
    }

    /**
     * 获取日志实例
     *
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogcat(): LoggerInterface {
        if ($this->logcat instanceof LoggerInterface) {
            return $this->logcat;
        }

        return new Logcat();
    }

    private $strict = true;

    /**
     * 设置是否严格模式
     *
     * @access public
     *
     * @param bool $strict 是否严格模式
     *
     * @return $this
     */
    final public function setStrict(bool $strict = true): self {
        $this->strict = $strict;

        return $this;
    }

    final public function isStrict(): bool {
        return $this->strict;
    }

    /**
     * @var string 运行模式
     */
    private $pattern = 'release';

    /**
     * 设置运行模式
     *
     * @param string $pattern
     *
     * @return $this
     */
    final public function setPattern(string $pattern = 'release'): self {
        if (isset(AppInfo::$patterns[$pattern]) || in_array($pattern, AppInfo::$patterns, true)) {
            $this->pattern = $pattern;
        }

        return $this;
    }

    final public function getPattern(): string {
        return $this->pattern;
    }

    /**
     * 设置缓存有效期
     *
     * @param int $expire
     *
     * @return $this
     */
    final public function setExpire(int $expire): self {
        if (!empty($expire)) {
            $this->expire = $expire;
        }

        return $this;
    }

    /**
     * @return int
     */
    final public function getExpire(): int {
        return $this->expire;
    }

    /**
     * 获取UserID
     *
     * @return mixed
     */
    public static function getUserID() {
        return Session::get('userid', Session::get('uuid', Session::get('uid', Session::get('union.userid', ''))));
    }

    /**
     * 检测用户是否已经登录
     * 已登录为True
     * 未登录为False
     *
     * @param array $value
     *
     * @return bool
     */
    public static function isLogin($value = array()): bool {
        return Session::has(self::$login) && Session::get(self::$login, 0) == 1
            && Session::has(self::$isLogin) && Session::get(self::$isLogin, 0) == 1
            && !self::isExpire($value);
    }

    /**
     * 验证是否经验联合授权
     *
     * @param array  $value
     * @param string $scope
     *
     * @return bool
     * @todo Union.Status 只有6才可使用，其余均为缓存，过期不候；有待完善，具体有效数据
     * @todo Union.scope 作用域，不应作为限制性条件，应为所包含的响应内容范围
     * @todo Union.corporate.corpid 除企业内管理员外，其余均需要管理员邀请同意才可在其它App上使用。
     */
    public static function isUnion($value = array(), string $scope = ''): bool {
        return self::isLogin($value)
            && Session::has('openid') && !empty(Session::get('openid'))
            && Session::has('union') && !empty(Session::get('union'))
            && Session::has('union.unionid') && Session::get('union.unionid', 0) !== 0
            && (Session::get('union.appid', 1) == Config::get('auth.appid', 0) || Session::get('corporate.corpid', 0) !== 0)
            // && Session::has('union.poolid') && Session::get('union.poolid', 1) == Config::get('auth.poolid', 0)
            && (Session::get('union.scope', '') == ($scope ?: Session::get('union.scope', '')))
            && Session::has('union.roleid') && Session::get('union.roleid', 0) !== 0
            && Session::has('union.status') && Session::get('union.status', 0) !== 0;
    }

    /**
     * 校验是否为企业
     *
     * @param array $value
     *
     * @return bool
     * @todo corporate.status eq 6；应添加检测企业状态的方法；
     */
    public static function isCorporate($value = array()): bool {
        return self::isLogin($value)
            && self::isUnion($value, AuthInfo::$auth_corp)
            && Session::has('corporate') && !empty(Session::get('corporate'))
            && Session::has('corporate.corpid') && Session::get('corporate.corpid', 0) !== 0
            && Session::has('corporate.status') && Session::get('corporate.status', 0) !== 0;
    }

    /**
     * 校验是否为商户
     *
     * @param array $value
     *
     * @return bool
     */
    public static function isMerchant($value = array()): bool {
        return self::isLogin($value)
            && self::isUnion($value, AuthInfo::$auth_merchant)
            && Session::has('merchant') && !empty(Session::get('merchant'))
            && Session::has('merchant.mchid') && !empty(Session::get('merchant.mchid')) && Session::get('merchant.mchid', 0) != 0
            && Session::has('merchant.status') && !empty(Session::get('merchant.status')) && Session::get('merchant.status', 0) != 0;
    }

    /**
     * 校验是否为项目
     *
     * @param array $value
     *
     * @return bool
     */
    public static function isProject($value = array()): bool {
        return self::isLogin($value)
            && self::isUnion($value, AuthInfo::$auth_project)
            && Session::has('project') && !empty(Session::get('project'))
            && Session::has('project.projectid') && !empty(Session::get('project.projectid')) && Session::get('project.projectid', 0) != 0
            && Session::has('project.status') && !empty(Session::get('project.status')) && Session::get('project.status', 0) != 0;
    }

    /**
     * 校验是否为物业
     *
     * @param array $value
     *
     * @return bool
     */
    public static function isProperty($value = array()): bool {
        return self::isLogin($value)
            && self::isUnion($value, AuthInfo::$auth_property)
            && Session::has('property') && !empty(Session::get('property'))
            && Session::has('property.propertyid') && !empty(Session::get('property.propertyid')) && Session::get('property.propertyid', 0) != 0
            && Session::has('property.status') && !empty(Session::get('property.status')) && Session::get('property.status', 0) != 0;
    }

    /**
     * 校验是否为小区
     *
     * @param array $value
     *
     * @return bool
     */
    public static function isCommunity($value = array()): bool {
        return self::isLogin($value)
            && self::isUnion($value, AuthInfo::$auth_community)
            && Session::has('community') && !empty(Session::get('community'))
            && Session::has('community.propertyid') && !empty(Session::get('community.propertyid')) && Session::get('community.propertyid', 0) != 0
            && Session::has('community.status') && !empty(Session::get('community.status')) && Session::get('community.status', 0) != 0;
    }

    /**
     * 校验是否为组织
     *
     * @param array $value
     *
     * @return bool
     */
    public static function isOrganize($value = array()): bool {
        return self::isLogin($value)
            && self::isUnion($value, AuthInfo::$auth_organize)
            && Session::has('organize') && !empty(Session::get('organize'))
            && Session::has('organize.orgid') && !empty(Session::get('organize.orgid')) && Session::get('organize.orgid', 0) != 0
            && Session::has('organize.status') && !empty(Session::get('organize.status')) && Session::get('organize.status', 0) != 0;
    }

    /**
     * 校验是否为门店
     *
     * @param array $value
     *
     * @return bool
     */
    public static function isStore($value = array()): bool {
        return self::isLogin($value)
            && self::isUnion($value, AuthInfo::$auth_store)
            && Session::has('store') && !empty(Session::get('store'))
            && Session::has('store.storeid') && !empty(Session::get('store.storeid')) && Session::get('store.storeid', 0) != 0
            && Session::has('store.status') && !empty(Session::get('store.status')) && Session::get('store.status', 0) != 0;
    }

    /**
     * 校验是否为员工
     *
     * @param array $value
     *
     * @return bool
     */
    public static function isEmployee($value = array()): bool {
        return self::isCorporate($value)
            && self::isUnion($value, AuthInfo::$auth_employee)
            && Session::has('employee') && !empty(Session::get('employee'))
            && Session::has('employee.empid') && !empty(Session::get('employee.empid')) && Session::get('employee.empid', 0) != 0
            && Session::has('employee.corpid') && !empty(Session::get('employee.corpid')) && Session::get('employee.corpid', 0) != 0
            && Session::has('employee.status') && !empty(Session::get('employee.status')) && Session::get('employee.status', 0) != 0;
    }

    /**
     * 验证是否为系统管理员
     * 管理员为True
     * 其它人为False
     *
     * @param array $value
     *
     * @return bool
     */
    public static function isAdmin($value = array()): bool {
        return self::isLogin($value) && (Session::get('union.gid', Session::get('union.guid', 200)) <= 10
                || Session::get('union.roleid', 200) <= 10
                || Session::get('union.roleid', 200) === 1);
    }

    /**
     * 校验是否为管理者
     * 管理者为True
     * 其它人为False
     *
     * @param array $value
     *
     * @return bool
     */
    public static function isManager($value = array()): bool {
        return self::isLogin($value) && (Session::get('union.gid', Session::get('union.guid', 200)) <= 10
                || Session::get('union.roleid', 200) <= 10
                || Session::get('union.roleid', 200) === 100
                || Session::get('employee.roleid', 200) === 100);
    }

    /**
     * 验证是否为测试员
     * 测试员为True
     * 其它人为False
     *
     * @param array $value
     *
     * @return bool
     */
    public static function isTesting($value = array()): bool {
        return self::isLogin($value) && Session::get('union.roleid', 200) == 340;
    }

    /**
     * 验证是否过期
     * 已过期为True
     * 未过期为False
     *
     * @param array $value
     *
     * @return bool
     */
    public static function isExpire($value = array()): bool {
        return !Session::has(self::$expiretime) || empty(Session::get(self::$expiretime)) || Session::get(self::$expiretime, time()) <= time();
    }

    /**
     * 校验是否拥有权限
     *
     * @param string $identifier 频道标识符
     * @param string $permission 权限标识符
     * @param int    $roleid     角色ID
     * @param bool   $cache      是否开启缓存
     *
     * @return bool
     */
    final public function hasPermission($identifier = '', $permission = 'button', $roleid = 0, $cache = true): bool {
        if (empty($roleid)) {
            $roleid = Session::get('roleid', Session::get('union.roleid', 404));
        }

        return ChannelModel::hasPermission(self::getAppId(), $identifier, $permission, $roleid, $cache);
    }

    /**
     * 校验是否允许访问
     *
     * @param string $identifier
     * @param int    $roleid
     * @param bool   $cache
     *
     * @return bool
     * @todo 有待优化；其目的是优化开放接口，可将部分接口转有私有接口，即可直接使用现有权限校验机制；避免非法访问。待实现
     */
    final public function isAllow($identifier = '', $roleid = 0, $cache = true): bool {
        if (empty($roleid)) {
            $roleid = Session::get('roleid', Session::get('union.roleid', 404));
        }

        return ChannelModel::hasPermission(self::getAppId(), $identifier, 'allow', $roleid, $cache);
    }
}